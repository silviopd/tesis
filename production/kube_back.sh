#!/bin/bash

cd backend && npm run build && cd .. &&
sudo docker build --no-cache --tag silviopd/tesis-backend:v16 --file production/backend.dockerfile . &&
docker push silviopd/tesis-backend:v16
