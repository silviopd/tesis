FROM node:12-alpine

WORKDIR /usr/src/app

COPY backend/dist ./
COPY production/package.json ./
RUN npm install --silent

EXPOSE 4000