#!/bin/bash

sudo docker build --no-cache --tag silviopd/tesis-fronted:v39 --file production/fronted.dockerfile . &&
docker push silviopd/tesis-fronted:v39
