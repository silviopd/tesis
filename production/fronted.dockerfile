FROM romeoz/docker-nginx-php
COPY fronted/ /var/www/app/
EXPOSE 80 443

CMD ["/usr/bin/supervisord"]