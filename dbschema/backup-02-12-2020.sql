PGDMP          	    
            x            tesis    9.5.22    13.1 (Debian 13.1-1.pgdg90+1) W    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16384    tesis    DATABASE     Y   CREATE DATABASE tesis WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';
    DROP DATABASE tesis;
                postgres    false                        2615    24716    audit    SCHEMA        CREATE SCHEMA audit;
    DROP SCHEMA audit;
                postgres    false            �           0    0    SCHEMA public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                   postgres    false    7            �            1255    24765 O   f_contribuyente_delete(character varying, character varying, character varying)    FUNCTION     W  CREATE FUNCTION public.f_contribuyente_delete(p_dni character varying, p_dni_usuario character varying, p_tipo character varying) RETURNS TABLE(v_result integer)
    LANGUAGE plpgsql
    AS $$
	declare v_contribuyente record;
	
	begin
		select * into v_contribuyente from contribuyente where dni like p_dni;

		--guardar el audit
		INSERT INTO audit.audit_contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado, 
				dni_usuario, 
				tipo)
		    VALUES (
				v_contribuyente.dni,
				v_contribuyente.nombres,
				v_contribuyente.apellido_paterno,
				v_contribuyente.apellido_materno,
				v_contribuyente.codigo,
				v_contribuyente.celular,
				v_contribuyente.direccion, 
				v_contribuyente.referencia, 
				v_contribuyente.correo, 
				v_contribuyente.estado, 
				p_dni_usuario, 
				upper(p_tipo)
		    );

		--eliminar el registro (solo se cambia el estado a 'I' inactivo)
		UPDATE public.contribuyente
		   SET  estado='I'
		 WHERE dni=p_dni;


		 return query
			select 200;
	end;
	
$$;
 �   DROP FUNCTION public.f_contribuyente_delete(p_dni character varying, p_dni_usuario character varying, p_tipo character varying);
       public          postgres    false            �            1255    24764 �   f_contribuyente_edit(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)    FUNCTION     )  CREATE FUNCTION public.f_contribuyente_edit(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying) RETURNS TABLE(v_result integer)
    LANGUAGE plpgsql
    AS $$
	declare v_contribuyente record;
	
	begin
		select * into v_contribuyente from contribuyente where dni like p_dni;
		
		--editar el registro
		UPDATE public.contribuyente
		   SET 	nombres=p_nombres, 
			apellido_paterno=p_apellido_paterno, 
			apellido_materno=p_apellido_materno, 
			codigo=p_codigo, 
			celular=p_celular, 
			direccion=p_direccion, 
			referencia=p_referencia, 
			correo=p_correo, 
			estado=p_estado
		 WHERE dni = p_dni;



		--guardar el audit
		INSERT INTO audit.audit_contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado, 
				dni_usuario, 
				tipo)
		    VALUES (
				v_contribuyente.dni,
				v_contribuyente.nombres,
				v_contribuyente.apellido_paterno,
				v_contribuyente.apellido_materno,
				v_contribuyente.codigo,
				v_contribuyente.celular,
				v_contribuyente.direccion,
				v_contribuyente.referencia,
				v_contribuyente.correo,
				v_contribuyente.estado,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		 return query
			select 200;
	end;
	
$$;
 �  DROP FUNCTION public.f_contribuyente_edit(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying);
       public          postgres    false            �            1255    24767 �   f_contribuyente_new(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)    FUNCTION       CREATE FUNCTION public.f_contribuyente_new(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying) RETURNS TABLE(v_result integer)
    LANGUAGE plpgsql
    AS $$
	declare v_contribuyente char(8);
	
	begin
		select dni into v_contribuyente from public.contribuyente where dni like p_dni;

		if v_contribuyente is null then 
			--se agrego el registro
			INSERT INTO public.contribuyente(
					dni, 
					nombres, 
					apellido_paterno, 
					apellido_materno, 
					codigo, 
					celular, 
					direccion, 
					referencia, 
					correo, 
					estado)
			    VALUES (
					p_dni,
					p_nombres,
					p_apellido_paterno,
					p_apellido_materno,
					p_codigo,
					p_celular,
					p_direccion,
					p_referencia,
					p_correo,
					p_estado
			    );


			--guardar el audit
			INSERT INTO audit.audit_contribuyente(
					dni, 
					nombres, 
					apellido_paterno, 
					apellido_materno, 
					codigo, 
					celular, 
					direccion, 
					referencia, 
					correo, 
					estado, 
					dni_usuario, 
					tipo)
			    VALUES (
					p_dni,
					p_nombres,
					p_apellido_paterno,
					p_apellido_materno,
					p_codigo,
					p_celular,
					p_direccion,
					p_referencia,
					p_correo,
					p_estado,
					p_dni_usuario, 
					upper(p_tipo)
			    );

			  --registar usuario
			  INSERT INTO public.usuario(
				    dni, password, estado)
			  VALUES (p_dni, MD5(p_dni), 'A');


			 return query
				select 200;
		else
			return query
				select 500;
		end if;
	
		
	end;
	
$$;
 �  DROP FUNCTION public.f_contribuyente_new(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying);
       public          postgres    false            �            1255    32914 3   f_deuda_cuotas(integer, integer, character varying)    FUNCTION     X  CREATE FUNCTION public.f_deuda_cuotas(p_id_deuda integer, p_nro_cuotas integer, p_fecha character varying) RETURNS TABLE(v_result integer)
    LANGUAGE plpgsql
    AS $$
	declare v_pago record;
	declare v_deuda_total numeric;
	declare v_deuda_mensual numeric;
	declare v_cuota int;
	
	begin
		
		select total into v_deuda_total from public.deuda where id_deuda = p_id_deuda;
		v_deuda_mensual = v_deuda_total / p_nro_cuotas;
		v_cuota = 1;


		 FOR v_pago IN (select (DATE (p_fecha) + (interval '1' month * generate_series(1,p_nro_cuotas)))::DATE as fecha)
		  LOOP
			INSERT INTO public.deuda_detalle(id_deuda, cuotas, fecha, total)
			VALUES (p_id_deuda,v_cuota,v_pago.fecha,v_deuda_mensual);

			v_cuota = v_cuota + 1;
		  END LOOP;


		UPDATE public.deuda
		   SET cuotas='S'
		 WHERE id_deuda=p_id_deuda;



		 return query
			select 200;
	end;
	
$$;
 j   DROP FUNCTION public.f_deuda_cuotas(p_id_deuda integer, p_nro_cuotas integer, p_fecha character varying);
       public          postgres    false            �            1255    24781 =   f_deuda_delete(integer, character varying, character varying)    FUNCTION     v  CREATE FUNCTION public.f_deuda_delete(p_id_deuda integer, p_dni_usuario character varying, p_tipo character varying) RETURNS TABLE(v_result integer)
    LANGUAGE plpgsql
    AS $$
	declare v_deuda record;
	
	begin
		select * into v_deuda from public.deuda where id_deuda = p_id_deuda;

		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda,
				dni,
				id_categoria,
				subtotal,
				mora,
				estado,
				total,
				descuento,
				dni_usuario,
				tipo)
		    VALUES (
				v_deuda.id_deuda,
				v_deuda.dni,
				v_deuda.id_categoria,
				v_deuda.subtotal,
				v_deuda.mora,
				v_deuda.estado,
				v_deuda.total, 
				v_deuda.descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );

		--eliminar el registro (solo se cambia el estado a 'I' inactivo)
		UPDATE public.deuda
		   SET  estado='I'
		 WHERE id_deuda=p_id_deuda;


		 return query
			select 200;
	end;
	
$$;
 t   DROP FUNCTION public.f_deuda_delete(p_id_deuda integer, p_dni_usuario character varying, p_tipo character varying);
       public          postgres    false            �            1255    24782 �   f_deuda_edit(integer, character varying, integer, numeric, numeric, numeric, numeric, character varying, character varying, character varying)    FUNCTION     s  CREATE FUNCTION public.f_deuda_edit(p_id_deuda integer, p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying) RETURNS TABLE(v_result integer)
    LANGUAGE plpgsql
    AS $$
	declare v_deuda record;
	
	begin
		select * into v_deuda from public.deuda where id_deuda = p_id_deuda;
		
		--editar el registro
		UPDATE public.deuda
		   SET 	dni=p_dni, 
			id_categoria=p_id_categoria, 
			subtotal=p_subtotal, 
			mora=p_mora, 
			estado=p_estado, 
			total=p_total, 
			descuento=p_descuento
		 WHERE id_deuda=p_id_deuda;


		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento,
				dni_usuario,
				tipo)
		    VALUES (
				v_deuda.id_deuda,
				v_deuda.dni,
				v_deuda.id_categoria,
				v_deuda.subtotal,
				v_deuda.mora,
				v_deuda.estado,
				v_deuda.total,
				v_deuda.descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		 return query
			select 200;
	end;
	
$$;
 	  DROP FUNCTION public.f_deuda_edit(p_id_deuda integer, p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying);
       public          postgres    false            �            1255    24779 �   f_deuda_new(character varying, integer, numeric, numeric, numeric, numeric, character varying, character varying, character varying)    FUNCTION       CREATE FUNCTION public.f_deuda_new(p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying) RETURNS TABLE(v_result integer)
    LANGUAGE plpgsql
    AS $$
	declare v_deuda record;
	declare v_id_deuda int;
	
	begin

		select * into v_id_deuda from public.f_generar_correlativo('deuda');
		
		--se agrego el registro
		INSERT INTO public.deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento)
		VALUES (
				v_id_deuda, 
				p_dni, 
				p_id_categoria, 
				p_subtotal, 
				p_mora, 
				p_estado, 
				p_total, 
				p_descuento
		);


		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento,
				dni_usuario, 
				tipo)
		    VALUES (
				v_id_deuda, 
				p_dni, 
				p_id_categoria, 
				p_subtotal, 
				p_mora, 
				p_estado, 
				p_total, 
				p_descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );

		--actualizando el correlativo
		UPDATE public.configuracion
		   SET  numero=numero+1
		 WHERE tabla='deuda';

		 return query
			select 200;
	end;
	
$$;
 �   DROP FUNCTION public.f_deuda_new(p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying);
       public          postgres    false            �            1255    16385 (   f_generar_correlativo(character varying)    FUNCTION     �   CREATE FUNCTION public.f_generar_correlativo(p_tabla character varying) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
	
	begin
		return query
		select 
			c.numero+1 
		from 
			configuracion c 
		where 
			c.tabla = p_tabla;
	end
	
$$;
 G   DROP FUNCTION public.f_generar_correlativo(p_tabla character varying);
       public          postgres    false            �            1255    41177 -   f_login(character varying, character varying)    FUNCTION     ?  CREATE FUNCTION public.f_login(p_user character varying, p_password character varying) RETURNS TABLE(r_mensaje character varying, r_dni character varying, r_nombres character varying, r_apellido_paterno character varying, r_apellido_materno character varying, r_id_perfiles integer, r_perfiles character varying, r_id_menu integer, r_menu character varying, r_icono character varying)
    LANGUAGE plpgsql
    AS $$
	declare v_usuario_estado varchar;
	
	begin

		select estado into v_usuario_estado from usuario where dni like p_user and password like MD5(p_password);
		if v_usuario_estado != 'A' || v_usuario_estado is null then
			return query 
			select 'Hubo un error en los datos, ingrese bien o contactese con soporte'::varchar,
				''::varchar,
				''::varchar,
				''::varchar,
				''::varchar,
				0,
				''::varchar,
				0,		
				''::varchar,
				''::varchar;
		else
			return query 
			SELECT 	'ok'::varchar,
				c1.dni::varchar, 
				c1.nombres, 
				c1.apellido_paterno, 
				c1.apellido_materno, 
				upm.id_perfiles, 
				p.nombre as nombre_perfiles,
				upm.id_menu, 
				m1.nombre as nombre_menu, 
				m1.icono				
			FROM "public".usuario_perfil_menu upm 
				INNER JOIN "public".menu m1 ON ( upm.id_menu = m1.id_menu  )  
				INNER JOIN "public".usuario u ON ( upm.dni = u.dni  )  
					INNER JOIN "public".contribuyente c1 ON ( u.dni = c1.dni  )  
				INNER JOIN "public".perfiles p ON ( upm.id_perfiles = p.id_perfiles  )  
			WHERE 	u.dni like p_user AND 
				u.password like MD5(p_password) AND 
				u.estado like 'A' AND
				upm.estado like 'A';
		end if;
			  
	end;
	
$$;
 V   DROP FUNCTION public.f_login(p_user character varying, p_password character varying);
       public          postgres    false            �            1255    49382 ;   f_pagar_deuda(integer, integer, integer, character varying)    FUNCTION     �  CREATE FUNCTION public.f_pagar_deuda(p_id_deuda integer, p_cuotas integer, p_id_tipo_transaccion integer, p_numero_transaccion character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE v_pagado int;
BEGIN
	UPDATE public.deuda_detalle
	SET id_tipo_transaccion=p_id_tipo_transaccion, fecha_transaccion=now(), numero_transaccion=p_numero_transaccion, estado='P'
	WHERE id_deuda=p_id_deuda and cuotas=p_cuotas;

	select count(*) into v_pagado from deuda_detalle where id_deuda=p_id_deuda and estado = 'A';
	if v_pagado = 0 then 
		UPDATE public.deuda SET estado = 'P' where id_deuda=p_id_deuda ;
	end if;
	
	RETURN true;
END;
$$;
 �   DROP FUNCTION public.f_pagar_deuda(p_id_deuda integer, p_cuotas integer, p_id_tipo_transaccion integer, p_numero_transaccion character varying);
       public          postgres    false            �            1255    16564 %   f_usuario_perfiles(character varying)    FUNCTION     L  CREATE FUNCTION public.f_usuario_perfiles(p_dni character varying) RETURNS TABLE(perfiles integer)
    LANGUAGE plpgsql
    AS $$
	declare v_usuario_estado varchar;
	
	begin
		return query
		SELECT p.id_perfiles
		FROM "public".usuario_menu_perfiles ump 
			INNER JOIN "public".usuario u ON ( ump.dni = u.dni  )  
			INNER JOIN "public".menu_perfiles mp ON ( ump.id_perfiles = mp.id_perfiles AND ump.id_menu = mp.id_menu  )  
				INNER JOIN "public".perfiles p ON ( mp.id_perfiles = p.id_perfiles  )  
		WHERE u.dni like p_dni
		GROUP BY u.dni, p.id_perfiles, p.nombre;
			  
	end;
	
$$;
 B   DROP FUNCTION public.f_usuario_perfiles(p_dni character varying);
       public          postgres    false            �            1259    24734    audit_contribuyente    TABLE     j  CREATE TABLE audit.audit_contribuyente (
    dni character(8) NOT NULL,
    nombres character varying(100) NOT NULL,
    apellido_paterno character varying(100) NOT NULL,
    apellido_materno character varying(100) NOT NULL,
    codigo character varying(11),
    celular character varying(10),
    direccion character varying(200),
    referencia character varying(200),
    correo character varying(20),
    estado character(1),
    dni_usuario character(8) NOT NULL,
    tipo character varying(9) NOT NULL,
    id_audit_contribuyente integer NOT NULL,
    fecha timestamp without time zone DEFAULT now() NOT NULL
);
 &   DROP TABLE audit.audit_contribuyente;
       audit            postgres    false    8            �           0    0    COLUMN audit_contribuyente.tipo    COMMENT     ]   COMMENT ON COLUMN audit.audit_contribuyente.tipo IS '1.- Agregar\n2.- Editar\n3.- Eliminar';
          audit          postgres    false    193            �            1259    24743 .   audit_contribuyente_id_audit_contribuyente_seq    SEQUENCE     �   CREATE SEQUENCE audit.audit_contribuyente_id_audit_contribuyente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE audit.audit_contribuyente_id_audit_contribuyente_seq;
       audit          postgres    false    8    193            �           0    0 .   audit_contribuyente_id_audit_contribuyente_seq    SEQUENCE OWNED BY        ALTER SEQUENCE audit.audit_contribuyente_id_audit_contribuyente_seq OWNED BY audit.audit_contribuyente.id_audit_contribuyente;
          audit          postgres    false    194            �            1259    24772    audit_deuda    TABLE     �  CREATE TABLE audit.audit_deuda (
    id_audit_contribuyente integer NOT NULL,
    id_deuda integer NOT NULL,
    dni character(8),
    id_categoria integer,
    subtotal numeric(15,2),
    mora numeric(12,2),
    estado character(1),
    total numeric(15,2),
    descuento numeric(15,2),
    dni_usuario character(8) NOT NULL,
    tipo character varying(9) NOT NULL,
    fecha timestamp without time zone DEFAULT now() NOT NULL
);
    DROP TABLE audit.audit_deuda;
       audit            postgres    false    8            �            1259    24770 &   audit_deuda_id_audit_contribuyente_seq    SEQUENCE     �   CREATE SEQUENCE audit.audit_deuda_id_audit_contribuyente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE audit.audit_deuda_id_audit_contribuyente_seq;
       audit          postgres    false    196    8            �           0    0 &   audit_deuda_id_audit_contribuyente_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE audit.audit_deuda_id_audit_contribuyente_seq OWNED BY audit.audit_deuda.id_audit_contribuyente;
          audit          postgres    false    195            �            1259    16387 	   categoria    TABLE     p   CREATE TABLE public.categoria (
    id_categoria integer NOT NULL,
    nombre character varying(50) NOT NULL
);
    DROP TABLE public.categoria;
       public            postgres    false            �            1259    16390    configuracion    TABLE     n   CREATE TABLE public.configuracion (
    tabla character varying(100) NOT NULL,
    numero integer NOT NULL
);
 !   DROP TABLE public.configuracion;
       public            postgres    false            �            1259    16393    contribuyente    TABLE     �  CREATE TABLE public.contribuyente (
    dni character(8) NOT NULL,
    nombres character varying(100) NOT NULL,
    apellido_paterno character varying(100) NOT NULL,
    apellido_materno character varying(100) NOT NULL,
    codigo character varying(11),
    celular character varying(10),
    direccion character varying(200),
    referencia character varying(200),
    correo character varying(20),
    estado character(1)
);
 !   DROP TABLE public.contribuyente;
       public            postgres    false            �            1259    16399    deuda    TABLE     Q  CREATE TABLE public.deuda (
    id_deuda integer NOT NULL,
    dni character(8),
    id_categoria integer,
    subtotal numeric(15,2),
    mora numeric(12,2) DEFAULT 0,
    estado character(1),
    total numeric(15,2),
    descuento numeric(15,2) DEFAULT 0,
    fecha timestamp without time zone DEFAULT now() NOT NULL,
    cuotas character(1) DEFAULT 'N'::bpchar NOT NULL,
    reclamo character(1) DEFAULT 'N'::bpchar,
    reclamo_descripcion character varying,
    reclamo_fecha timestamp without time zone,
    reclamo_ad timestamp without time zone,
    reclamo_usuario_ad character(8)
);
    DROP TABLE public.deuda;
       public            postgres    false            �            1259    16402    deuda_detalle    TABLE     �  CREATE TABLE public.deuda_detalle (
    id_deuda integer NOT NULL,
    cuotas integer NOT NULL,
    mora numeric(12,2) DEFAULT 0,
    fecha date,
    id_tipo_transaccion integer,
    fecha_transaccion timestamp without time zone,
    numero_transaccion character varying(20),
    estado character(1) DEFAULT 'A'::bpchar NOT NULL,
    subtotal numeric(15,2) DEFAULT 0,
    descuento numeric(15,2) DEFAULT 0,
    total numeric(15,2) NOT NULL
);
 !   DROP TABLE public.deuda_detalle;
       public            postgres    false            �            1259    16493    menu    TABLE        CREATE TABLE public.menu (
    id_menu integer NOT NULL,
    nombre character varying(100),
    icono character varying(30)
);
    DROP TABLE public.menu;
       public            postgres    false            �           0    0 
   TABLE menu    COMMENT     G   COMMENT ON TABLE public.menu IS 'row usuario: N: No, S: Si, A: Ambos';
          public          postgres    false    192            �            1259    32917    notifcacion_usuario    TABLE     �   CREATE TABLE public.notifcacion_usuario (
    dni character(8) NOT NULL,
    data character varying NOT NULL,
    estado character(1) DEFAULT 'A'::bpchar NOT NULL
);
 '   DROP TABLE public.notifcacion_usuario;
       public            postgres    false            �            1259    16405    notificacion    TABLE     ,  CREATE TABLE public.notificacion (
    id_notificacion integer NOT NULL,
    app character(1) NOT NULL,
    correo character(1) NOT NULL,
    web character(1) NOT NULL,
    mensaje character varying(300) NOT NULL,
    fecha timestamp without time zone DEFAULT now() NOT NULL,
    dni character(8)
);
     DROP TABLE public.notificacion;
       public            postgres    false            �            1259    41090     notificacion_id_notificacion_seq    SEQUENCE     �   CREATE SEQUENCE public.notificacion_id_notificacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.notificacion_id_notificacion_seq;
       public          postgres    false    187            �           0    0     notificacion_id_notificacion_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.notificacion_id_notificacion_seq OWNED BY public.notificacion.id_notificacion;
          public          postgres    false    198            �            1259    16409    perfiles    TABLE     f   CREATE TABLE public.perfiles (
    id_perfiles integer NOT NULL,
    nombre character varying(100)
);
    DROP TABLE public.perfiles;
       public            postgres    false            �           0    0    TABLE perfiles    COMMENT     J   COMMENT ON TABLE public.perfiles IS '1= administrador\n2= contribuyente';
          public          postgres    false    188            �            1259    16412    tipo_transaccion    TABLE     z   CREATE TABLE public.tipo_transaccion (
    id_tipo_transaccion integer NOT NULL,
    nombre character varying NOT NULL
);
 $   DROP TABLE public.tipo_transaccion;
       public            postgres    false            �            1259    16415    usuario    TABLE     �   CREATE TABLE public.usuario (
    dni character(8) NOT NULL,
    password character(32) NOT NULL,
    estado character(1) DEFAULT 'A'::bpchar
);
    DROP TABLE public.usuario;
       public            postgres    false            �            1259    41126    usuario_perfil_menu    TABLE     �   CREATE TABLE public.usuario_perfil_menu (
    dni character(8) NOT NULL,
    id_perfiles integer NOT NULL,
    id_menu integer NOT NULL,
    estado character(1) DEFAULT 'A'::bpchar NOT NULL
);
 '   DROP TABLE public.usuario_perfil_menu;
       public            postgres    false            �            1259    16487    v_deudas    VIEW     �  CREATE VIEW public.v_deudas AS
 SELECT c1.nombres,
    c1.apellido_paterno,
    c1.apellido_materno,
    d.id_deuda,
    d.dni,
    d.id_categoria,
    d.subtotal,
    d.mora,
    d.total,
    d.estado,
    c2.nombre AS categoria,
    d.descuento,
    d.cuotas,
    d.fecha,
    d.reclamo,
    d.reclamo_descripcion,
    d.reclamo_fecha,
    d.reclamo_ad
   FROM ((public.contribuyente c1
     JOIN public.deuda d ON ((c1.dni = d.dni)))
     JOIN public.categoria c2 ON ((d.id_categoria = c2.id_categoria)));
    DROP VIEW public.v_deudas;
       public          postgres    false    185    182    182    184    184    184    184    185    185    185    185    185    185    185    185    185    185    185    185    185            �            1259    41195    v_deudas_reporte    VIEW     ?  CREATE VIEW public.v_deudas_reporte AS
 SELECT dd.id_deuda,
    dd.cuotas,
    dd.mora,
    dd.fecha,
    dd.id_tipo_transaccion,
    dd.fecha_transaccion,
    dd.numero_transaccion,
    dd.estado,
    dd.subtotal,
    dd.descuento,
    dd.total,
    d.subtotal AS deuda_subtotal,
    d.mora AS deuda_mora,
    d.total AS deuda_total,
    d.descuento AS deuda_descuento,
    d.fecha AS deuda_fecha,
    d.cuotas AS deuda_cuotas,
    c1.dni,
    c1.nombres,
    c1.apellido_paterno,
    c1.apellido_materno,
    c1.celular,
    c1.direccion,
    c1.referencia,
    c1.correo,
    c2.nombre AS nombre_categoria
   FROM (((public.deuda_detalle dd
     JOIN public.deuda d ON ((dd.id_deuda = d.id_deuda)))
     JOIN public.contribuyente c1 ON ((d.dni = c1.dni)))
     JOIN public.categoria c2 ON ((d.id_categoria = c2.id_categoria)));
 #   DROP VIEW public.v_deudas_reporte;
       public          postgres    false    185    185    185    184    184    184    184    184    184    185    185    185    185    185    186    186    186    186    186    186    186    186    186    186    186    185    182    182    184    184                       2604    24745 *   audit_contribuyente id_audit_contribuyente    DEFAULT     �   ALTER TABLE ONLY audit.audit_contribuyente ALTER COLUMN id_audit_contribuyente SET DEFAULT nextval('audit.audit_contribuyente_id_audit_contribuyente_seq'::regclass);
 X   ALTER TABLE audit.audit_contribuyente ALTER COLUMN id_audit_contribuyente DROP DEFAULT;
       audit          postgres    false    194    193            !           2604    24775 "   audit_deuda id_audit_contribuyente    DEFAULT     �   ALTER TABLE ONLY audit.audit_deuda ALTER COLUMN id_audit_contribuyente SET DEFAULT nextval('audit.audit_deuda_id_audit_contribuyente_seq'::regclass);
 P   ALTER TABLE audit.audit_deuda ALTER COLUMN id_audit_contribuyente DROP DEFAULT;
       audit          postgres    false    195    196    196                       2604    41092    notificacion id_notificacion    DEFAULT     �   ALTER TABLE ONLY public.notificacion ALTER COLUMN id_notificacion SET DEFAULT nextval('public.notificacion_id_notificacion_seq'::regclass);
 K   ALTER TABLE public.notificacion ALTER COLUMN id_notificacion DROP DEFAULT;
       public          postgres    false    198    187            �          0    24734    audit_contribuyente 
   TABLE DATA           �   COPY audit.audit_contribuyente (dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha) FROM stdin;
    audit          postgres    false    193   ��       �          0    24772    audit_deuda 
   TABLE DATA           �   COPY audit.audit_deuda (id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha) FROM stdin;
    audit          postgres    false    196   �       �          0    16387 	   categoria 
   TABLE DATA           9   COPY public.categoria (id_categoria, nombre) FROM stdin;
    public          postgres    false    182   -�       �          0    16390    configuracion 
   TABLE DATA           6   COPY public.configuracion (tabla, numero) FROM stdin;
    public          postgres    false    183   !�       �          0    16393    contribuyente 
   TABLE DATA           �   COPY public.contribuyente (dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado) FROM stdin;
    public          postgres    false    184   Y�       �          0    16399    deuda 
   TABLE DATA           �   COPY public.deuda (id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad) FROM stdin;
    public          postgres    false    185   ��       �          0    16402    deuda_detalle 
   TABLE DATA           �   COPY public.deuda_detalle (id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total) FROM stdin;
    public          postgres    false    186   b�       �          0    16493    menu 
   TABLE DATA           6   COPY public.menu (id_menu, nombre, icono) FROM stdin;
    public          postgres    false    192   �       �          0    32917    notifcacion_usuario 
   TABLE DATA           @   COPY public.notifcacion_usuario (dni, data, estado) FROM stdin;
    public          postgres    false    197   ޸       �          0    16405    notificacion 
   TABLE DATA           ^   COPY public.notificacion (id_notificacion, app, correo, web, mensaje, fecha, dni) FROM stdin;
    public          postgres    false    187   ��       �          0    16409    perfiles 
   TABLE DATA           7   COPY public.perfiles (id_perfiles, nombre) FROM stdin;
    public          postgres    false    188   _�       �          0    16412    tipo_transaccion 
   TABLE DATA           G   COPY public.tipo_transaccion (id_tipo_transaccion, nombre) FROM stdin;
    public          postgres    false    189   Ŀ       �          0    16415    usuario 
   TABLE DATA           8   COPY public.usuario (dni, password, estado) FROM stdin;
    public          postgres    false    190   ��       �          0    41126    usuario_perfil_menu 
   TABLE DATA           P   COPY public.usuario_perfil_menu (dni, id_perfiles, id_menu, estado) FROM stdin;
    public          postgres    false    199   a�       �           0    0 .   audit_contribuyente_id_audit_contribuyente_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('audit.audit_contribuyente_id_audit_contribuyente_seq', 50, true);
          audit          postgres    false    194            �           0    0 &   audit_deuda_id_audit_contribuyente_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('audit.audit_deuda_id_audit_contribuyente_seq', 104, true);
          audit          postgres    false    195            �           0    0     notificacion_id_notificacion_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.notificacion_id_notificacion_seq', 101, true);
          public          postgres    false    198            >           2606    24777    audit_deuda audit_deuda_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY audit.audit_deuda
    ADD CONSTRAINT audit_deuda_pkey PRIMARY KEY (id_audit_contribuyente);
 E   ALTER TABLE ONLY audit.audit_deuda DROP CONSTRAINT audit_deuda_pkey;
       audit            postgres    false    196            <           2606    24753 A   audit_contribuyente pk_audit_contribuyente_id_audit_contribuyente 
   CONSTRAINT     �   ALTER TABLE ONLY audit.audit_contribuyente
    ADD CONSTRAINT pk_audit_contribuyente_id_audit_contribuyente PRIMARY KEY (id_audit_contribuyente);
 j   ALTER TABLE ONLY audit.audit_contribuyente DROP CONSTRAINT pk_audit_contribuyente_id_audit_contribuyente;
       audit            postgres    false    193            &           2606    16428 #   categoria pk_categoria_id_categoria 
   CONSTRAINT     k   ALTER TABLE ONLY public.categoria
    ADD CONSTRAINT pk_categoria_id_categoria PRIMARY KEY (id_categoria);
 M   ALTER TABLE ONLY public.categoria DROP CONSTRAINT pk_categoria_id_categoria;
       public            postgres    false    182            (           2606    16430 $   configuracion pk_configuracion_tabla 
   CONSTRAINT     e   ALTER TABLE ONLY public.configuracion
    ADD CONSTRAINT pk_configuracion_tabla PRIMARY KEY (tabla);
 N   ALTER TABLE ONLY public.configuracion DROP CONSTRAINT pk_configuracion_tabla;
       public            postgres    false    183            *           2606    16432 "   contribuyente pk_contribuyente_dni 
   CONSTRAINT     a   ALTER TABLE ONLY public.contribuyente
    ADD CONSTRAINT pk_contribuyente_dni PRIMARY KEY (dni);
 L   ALTER TABLE ONLY public.contribuyente DROP CONSTRAINT pk_contribuyente_dni;
       public            postgres    false    184            .           2606    16434 '   deuda_detalle pk_deuda_detalle_id_deuda 
   CONSTRAINT     s   ALTER TABLE ONLY public.deuda_detalle
    ADD CONSTRAINT pk_deuda_detalle_id_deuda PRIMARY KEY (id_deuda, cuotas);
 Q   ALTER TABLE ONLY public.deuda_detalle DROP CONSTRAINT pk_deuda_detalle_id_deuda;
       public            postgres    false    186    186            ,           2606    16436    deuda pk_deuda_id_deuda 
   CONSTRAINT     [   ALTER TABLE ONLY public.deuda
    ADD CONSTRAINT pk_deuda_id_deuda PRIMARY KEY (id_deuda);
 A   ALTER TABLE ONLY public.deuda DROP CONSTRAINT pk_deuda_id_deuda;
       public            postgres    false    185            :           2606    16497    menu pk_menu_id_menu 
   CONSTRAINT     W   ALTER TABLE ONLY public.menu
    ADD CONSTRAINT pk_menu_id_menu PRIMARY KEY (id_menu);
 >   ALTER TABLE ONLY public.menu DROP CONSTRAINT pk_menu_id_menu;
       public            postgres    false    192            @           2606    32946 .   notifcacion_usuario pk_notifcacion_usuario_dni 
   CONSTRAINT     s   ALTER TABLE ONLY public.notifcacion_usuario
    ADD CONSTRAINT pk_notifcacion_usuario_dni PRIMARY KEY (dni, data);
 X   ALTER TABLE ONLY public.notifcacion_usuario DROP CONSTRAINT pk_notifcacion_usuario_dni;
       public            postgres    false    197    197            0           2606    41097 ,   notificacion pk_notificacion_id_notificacion 
   CONSTRAINT     w   ALTER TABLE ONLY public.notificacion
    ADD CONSTRAINT pk_notificacion_id_notificacion PRIMARY KEY (id_notificacion);
 V   ALTER TABLE ONLY public.notificacion DROP CONSTRAINT pk_notificacion_id_notificacion;
       public            postgres    false    187            2           2606    16440     perfiles pk_perfiles_id_perfiles 
   CONSTRAINT     g   ALTER TABLE ONLY public.perfiles
    ADD CONSTRAINT pk_perfiles_id_perfiles PRIMARY KEY (id_perfiles);
 J   ALTER TABLE ONLY public.perfiles DROP CONSTRAINT pk_perfiles_id_perfiles;
       public            postgres    false    188            4           2606    57575 8   tipo_transaccion pk_tipo_transaccion_id_tipo_transaccion 
   CONSTRAINT     �   ALTER TABLE ONLY public.tipo_transaccion
    ADD CONSTRAINT pk_tipo_transaccion_id_tipo_transaccion PRIMARY KEY (id_tipo_transaccion);
 b   ALTER TABLE ONLY public.tipo_transaccion DROP CONSTRAINT pk_tipo_transaccion_id_tipo_transaccion;
       public            postgres    false    189            8           2606    16442    usuario pk_usuario_dni 
   CONSTRAINT     U   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT pk_usuario_dni PRIMARY KEY (dni);
 @   ALTER TABLE ONLY public.usuario DROP CONSTRAINT pk_usuario_dni;
       public            postgres    false    190            B           2606    41156 )   usuario_perfil_menu pk_usuario_perfil_dni 
   CONSTRAINT     ~   ALTER TABLE ONLY public.usuario_perfil_menu
    ADD CONSTRAINT pk_usuario_perfil_dni PRIMARY KEY (dni, id_perfiles, id_menu);
 S   ALTER TABLE ONLY public.usuario_perfil_menu DROP CONSTRAINT pk_usuario_perfil_dni;
       public            postgres    false    199    199    199            6           2606    16446 9   tipo_transaccion unq_tipo_transaccion_id_tipo_transaccion 
   CONSTRAINT     �   ALTER TABLE ONLY public.tipo_transaccion
    ADD CONSTRAINT unq_tipo_transaccion_id_tipo_transaccion UNIQUE (id_tipo_transaccion);
 c   ALTER TABLE ONLY public.tipo_transaccion DROP CONSTRAINT unq_tipo_transaccion_id_tipo_transaccion;
       public            postgres    false    189            C           2606    16447    deuda fk_deuda_categoria    FK CONSTRAINT     �   ALTER TABLE ONLY public.deuda
    ADD CONSTRAINT fk_deuda_categoria FOREIGN KEY (id_categoria) REFERENCES public.categoria(id_categoria);
 B   ALTER TABLE ONLY public.deuda DROP CONSTRAINT fk_deuda_categoria;
       public          postgres    false    182    2086    185            D           2606    16557    deuda fk_deuda_contribuyente    FK CONSTRAINT     �   ALTER TABLE ONLY public.deuda
    ADD CONSTRAINT fk_deuda_contribuyente FOREIGN KEY (dni) REFERENCES public.contribuyente(dni);
 F   ALTER TABLE ONLY public.deuda DROP CONSTRAINT fk_deuda_contribuyente;
       public          postgres    false    184    185    2090            E           2606    16457 $   deuda_detalle fk_deuda_detalle_deuda    FK CONSTRAINT     �   ALTER TABLE ONLY public.deuda_detalle
    ADD CONSTRAINT fk_deuda_detalle_deuda FOREIGN KEY (id_deuda) REFERENCES public.deuda(id_deuda);
 N   ALTER TABLE ONLY public.deuda_detalle DROP CONSTRAINT fk_deuda_detalle_deuda;
       public          postgres    false    185    186    2092            F           2606    16462 /   deuda_detalle fk_deuda_detalle_tipo_transaccion    FK CONSTRAINT     �   ALTER TABLE ONLY public.deuda_detalle
    ADD CONSTRAINT fk_deuda_detalle_tipo_transaccion FOREIGN KEY (id_tipo_transaccion) REFERENCES public.tipo_transaccion(id_tipo_transaccion);
 Y   ALTER TABLE ONLY public.deuda_detalle DROP CONSTRAINT fk_deuda_detalle_tipo_transaccion;
       public          postgres    false    186    189    2102            I           2606    32928 8   notifcacion_usuario fk_notifcacion_usuario_contribuyente    FK CONSTRAINT     �   ALTER TABLE ONLY public.notifcacion_usuario
    ADD CONSTRAINT fk_notifcacion_usuario_contribuyente FOREIGN KEY (dni) REFERENCES public.contribuyente(dni);
 b   ALTER TABLE ONLY public.notifcacion_usuario DROP CONSTRAINT fk_notifcacion_usuario_contribuyente;
       public          postgres    false    2090    184    197            G           2606    41103 *   notificacion fk_notificacion_contribuyente    FK CONSTRAINT     �   ALTER TABLE ONLY public.notificacion
    ADD CONSTRAINT fk_notificacion_contribuyente FOREIGN KEY (dni) REFERENCES public.contribuyente(dni);
 T   ALTER TABLE ONLY public.notificacion DROP CONSTRAINT fk_notificacion_contribuyente;
       public          postgres    false    187    2090    184            H           2606    16472     usuario fk_usuario_contribuyente    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk_usuario_contribuyente FOREIGN KEY (dni) REFERENCES public.contribuyente(dni);
 J   ALTER TABLE ONLY public.usuario DROP CONSTRAINT fk_usuario_contribuyente;
       public          postgres    false    2090    184    190            L           2606    41150 *   usuario_perfil_menu fk_usuario_perfil_menu    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_perfil_menu
    ADD CONSTRAINT fk_usuario_perfil_menu FOREIGN KEY (id_menu) REFERENCES public.menu(id_menu);
 T   ALTER TABLE ONLY public.usuario_perfil_menu DROP CONSTRAINT fk_usuario_perfil_menu;
       public          postgres    false    199    192    2106            K           2606    41137 .   usuario_perfil_menu fk_usuario_perfil_perfiles    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_perfil_menu
    ADD CONSTRAINT fk_usuario_perfil_perfiles FOREIGN KEY (id_perfiles) REFERENCES public.perfiles(id_perfiles);
 X   ALTER TABLE ONLY public.usuario_perfil_menu DROP CONSTRAINT fk_usuario_perfil_perfiles;
       public          postgres    false    199    2098    188            J           2606    41132 -   usuario_perfil_menu fk_usuario_perfil_usuario    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_perfil_menu
    ADD CONSTRAINT fk_usuario_perfil_usuario FOREIGN KEY (dni) REFERENCES public.usuario(dni);
 W   ALTER TABLE ONLY public.usuario_perfil_menu DROP CONSTRAINT fk_usuario_perfil_usuario;
       public          postgres    false    2104    190    199            �   G  x��W[n�8��W���}�qx�I�N[`������X��d���,e0K�ƆJh�e�4B�ִ��xp��չ�!�q�]�m��E%��u��__�
�3y�X����o�-;!�zyS7녆_.��z��͕��N,��M�_��5�vc���B�2>���LCl���fw=踸 T>��������� ���
ւ�Q؄�^�\5W��j3[�_�v�l�Wi�]���Ģn�����o����>Wm����sC�2��0ʢv�߇�]ʨ#�>�β�v��e�y,=����Y0������7p|���ٞ"�C����>� ��8��B��@�)��X�U������y���v�S��E�)� �v?y�Ñjw9e��R�NӰ���'9$x�cJ{�t������!�)��d���(�S�uD=eK�Ҟ{]2^��Ҟ{]��F1jЃ�wʽ.1��Q�ud
�C ��S�F{�u	���Q�u&Z�'�a�;]���X>7:c�E)vƻ0D:�F���C?(��c?Aj(8����S-�ݷFw�t�{�!	�Xv�J�]�h��G����X��Y_��?���>;{?��o��?�'g�������8�M>��0;)�|�ݺie���fY]�����=�&c���-T��5��p�c��p�����g���=#�a��Ky6�鰉�Y�?�����-�U��p�9�rڶ`R��ji��?ۺ�RG�_��'M�hnJqZ�oy��ɍ���es���ZB�)8�Ӝ|�	C_t�A�᜶2V��Gb�&��by��U.�x]���28��)33:�a�M����F����      �   -  x���MnC!��p�w� ��f�Yt�JU�U���E�鏒����x�	�T�R#Ԅ�T��q�χ������7L;�h�.N�*�-���6*Aq��_�%���ޣ�~a_�/'���#��[�&`ψr�*�u%K*�Vg�
��]d�Q6� #<��A<6i�O?�XY��`-�H`9���4�h-b��2���Z5�rӅ��ك�+}4+d\}m�\���ޔjK4��ͪ��i�_�ugX[*���

tF^�-$���8
-����o�YUr�Bt_\�\�U��5����?�J�{      �   �   x���MJA�םS����Y
�4*�ܤ������*��͜��۾�%
-��.||����.�T9���8\�=?������F
�����2���1�S����Z���nm����(Nθ�W�S�_I���/{ {n���Od^b;���ϊ[6���(�w7�zr2�E�V�3k!jb_C��fc�y���E�O%��.eXt=�c�N�����> | 8���      �   (   x�+H-J��I-�4�
�4�JI-MI�442����� ��/      �   V  x���KN�@���)|���Ȏ�f�Ѫ�bc��1̈́�&n�Q8C/�$MK+�,kd��������9n �Iz�Lq�wQ:��,�|D0N�{p�p!8�0"i����7�5�5ϕ�R*d3)؂˶S������,s�=]��;�+�����&aRQA�N�l��1�wC9S��[�X�3�dE�I4ȉ̞�z��w<@Ъ̍�����IH+�qNO�DB������9'%��i��P�+�b�JF;��]���Hq�֮4���������FK�����+#�rY�vy�a�ZT�x&�\f*t�C�o�wL1�jO3կ�f���j�{ ���eY_�Ծ�      �   �  x��W�nG<��b~`�W�9� >r�E���c����I=='�,�gQ[$��\"��x�L:єS�o�4��gη�\�l��(i.Ue�e�4�_��=��:���¸��L��E$1	��)�fΝ�1�l(��	��q#ĞH����e�~ �#&��|.��L�H^�S� m�(�������~��򸽽����%i8��)��_����o�Y�%k2&W?���C�xu^���JŘ�-CW�D��F��iRS�.Ŋ���E��ߌї=�6�IvZ2;���p�h�W=����$�&�e��y�������d�qke-g̲A�TJJ�i�l��z�zcЌ��9U*���z������}~n�8;�,����B��s���n�l
��V��B�D�ad~�^2Nr�=!��|��R����JA�E�=98}�+��M��NQ��#] ��T���T'ʖrל�(�7�qm�Pm�J�Pq���?�!�j�ŋ��&�5W��C:A�������8�M��$��Q��K% �4"Y�s���up��~��˚K)�c7�2ß���k+�L�;Y�ɠ�Q)yK���YpoFRJB'��H)m?0ZՒx�rè�G@[}��"�<�<p�S�in�x�����^Ҵ���Q�T6�k¢��ǣJ S�a<�*Ѧp� j&M��2�9V����֊W�h�N�e��5dz*��k�R���sֶi��,"l�βB4������E�K�1�,}���f�uSL��«h�vHWk+I��q9�)@�*j%7�b�&�&%$̕�����Y�j�@�����k�7���X��0�3Ѧ����0o�	e��j�7��}5��&����]��ܴ"�⧩e��pk(�F`Az�8>��h��=�q��1��%x ��9�q�ԪC2�Y\��C�bITo��;�Ej�^|�QnLm��ѯ0m�D�D�:r�,�iQ9N�-[������l�X�bn�-#����!#�
�	��Lל�K;O��kL�jG?a�+D�0w�cSl����{����)�ǲ*�Z_��r��I&�CA����26��={��᮫�qA���/!i���}��֓v�����S"�Rm�
�c�������e��Y��u~�}~z��#�����y�?��b8��~|b�K��/����4���      �   �  x��T�n�0���D ����:t-:t�R ����RiȮ�(��)�tЀ@�(�@G�#K�|����O�W�H1��G`�wi*�9�!n*�9��p5D$Kf����}�����E�Q���-�#Pf�0�D���L�`N���5�YQU=�H�&ޘv���i��~㱓ȳx�	�x�uMk�����"GʀI�x=�u�����y� �����)��@V��Y�D��Qkσ���<G�h7D����t]�d���V�R�)�ÍlW��G{x$o�)�� {yO@�V4�{�Ķ�x�����C�/E��I��ǃ�ll]�ꋛEW�3�p�l.x�Or	���|9�Lڶ=���̚����O��wCn1ɋ���F�.����+�$��g\����*1      �   �   x�e�]N�0���S�Y���s^��-ٸ�����#p1�+���q��F�yrJ�h!��nB�&�����	^\$U�����+o�$��Rx∑eg�n�Y��I2�~����	z7Rq�5�Ը�\�\���R.�;�qxv#�niB2&���G�!��'&<���󢱴}K�<'�лj��_<�st���߱����z �wtt�      �     x���Ɏ�Z�u�1X�n��;3clfc����y�Q��v�*�:�"�R/ΦTRU}�u�FH%�|�@��MQ����c;��޲�z͛&/A��k�T����-=�1a��绽�g�X����s-�`�.��	nDX�Q_Cm��".F��7<�,��L�����.#�Ʃ���c��%���6�
��`>�gQ����yzYM�Jbq��`�R����	�
��-�h,��-* ��,�B����b$��ޯ�L�:?���\�X/An���2�|$W�0���;�J�3�#	�ʭ���"���0�l��xS�ҦД���D��1 U�I����<�`�Ԍ���߿�_P'>�'�_�'p����$>y��n�V|~�Z�5ۙܖK���4q�[d�w&��6x�y�B4�z��<8�5�֢�C�H+8�\n���]ѱep��7�#��m�B�sV��3���t��ah���S�E� /�)&WCL�
�,�[cB��6xjuH2.�͏�s�H@N�}��|S��l���7��@MƇ�^����xDd/ub81�h�X��n������g�u�4��ގa�(�SQ�`V����
�Ѥ\Gg|^��	�%�f��/����9��gS����Ô���2�Xsr^i-�o}�t����q��lĔJ�|�n�&�+M��bR���v/� �{�����2�0E�����,D�톉��v���}BX���]�WU8��=�3�u�ړ�c��?�5펭����/��B:e�c����Q��y}yy��-�r      �   S  x����n"I���)�&%�U���5v/��zv�2Ai���E�C8$CNRZ�sٮ����菷�!�ۇ���~��}���}��C�w:��}�b�#�'�O�a��c��T�2ʢ��y\p�s6̇����0y�,��0�ڳ��[X{�����l�/�i5L��|2k1�j���3qs�)b��ť��vm��f��tX�/��ߖ�1�;�HR��c��� ]�)���?��t����s�G���q�U1��A>��;�JAL�WA}��4y]�ϳ��S;��ޞ�S"(�,.N�G	�z"ꐺ�$dG��m���]�(�b��-0wh-�\�����m�&N�� ��Q����^���i���٪_���4	We"g�����k��@B���Ok��B�����G��-�y��\+$ ���G��j92�ʨn��U�c1��Me[Y݄7�cpT�D�~ǵ<�R��ټ�ƊU�s�Z?n��[�YԮM.�X����p搥�9�x��<���%��p�JLB�~�G��~�_�S����'� ��N	@>{>6�[��#�cƜJ��x�|\\�6M�]��a돋��;r��ra�9��G�� "�ʶ�;.����P��?ɣk���ZLХ+@Q� ȱC�vk���6�M҉�-M�	��!Y�)1I�D�4�m��%;֔%������}*S[�HZ�4e<�o�b�����@lK6pr�Q���8�xj{hR6S�O������)�Yu�6m�݀T��:��kNmK�T��H�|˶r{� [4�U�|����W��sX寃���1j���u�f���Uo�����y519��}�g��4���1��Z��\6!`q��+����6�4�      �   U   x�ʻ�0E�ڞ" �ۆ���Db䘂�	�=w�l-��ϫ-�g���J���F�7<P[M��B5�	��K�V�Jab����?���      �       x�3�LN/I��2�LI-�/�,������ X��      �   ]   x����� �0L信�i��!�	(N�����P�hF=���T	pTz�%����N+L����[���;a�[���ָR��>K)x(      �   z   x�e�A� �sy��e8z���a5�ÄM�{��+D�9�Z ��)̼��!���1'�?�x���q�>�Yt0�����(��f�ͨ	F����|~��=Yx0#O3ɹ����9w�AGN     