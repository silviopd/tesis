CREATE SCHEMA IF NOT EXISTS "audit";

CREATE SEQUENCE "audit".audit_contribuyente_id_audit_contribuyente_seq START WITH 1;

CREATE SEQUENCE "audit".audit_deuda_id_audit_contribuyente_seq START WITH 1;

CREATE SEQUENCE "public".notificacion_id_notificacion_seq START WITH 1;

CREATE  TABLE "audit".audit_contribuyente ( 
	id_audit_contribuyente integer DEFAULT nextval('audit.audit_contribuyente_id_audit_contribuyente_seq'::regclass) NOT NULL ,
	dni                  char(8)  NOT NULL ,
	nombres              varchar(100)  NOT NULL ,
	apellido_paterno     varchar(100)  NOT NULL ,
	apellido_materno     varchar(100)  NOT NULL ,
	codigo               varchar(11)   ,
	celular              varchar(10)   ,
	direccion            varchar(200)   ,
	referencia           varchar(200)   ,
	correo               varchar(20)   ,
	estado               char(1)   ,
	dni_usuario          char(8)  NOT NULL ,
	tipo                 varchar(9)  NOT NULL ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	CONSTRAINT pk_audit_contribuyente_id_audit_contribuyente PRIMARY KEY ( id_audit_contribuyente )
 );

COMMENT ON COLUMN "audit".audit_contribuyente.tipo IS '1.- Agregar\n2.- Editar\n3.- Eliminar';

CREATE  TABLE "audit".audit_deuda ( 
	id_audit_contribuyente integer DEFAULT nextval('audit.audit_deuda_id_audit_contribuyente_seq'::regclass) NOT NULL ,
	id_deuda             integer  NOT NULL ,
	dni                  char(8)   ,
	id_categoria         integer   ,
	subtotal             numeric(15,2)   ,
	mora                 numeric(12,2)   ,
	estado               char(1)   ,
	total                numeric(15,2)   ,
	descuento            numeric(15,2)   ,
	dni_usuario          char(8)  NOT NULL ,
	tipo                 varchar(9)  NOT NULL ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	CONSTRAINT audit_deuda_pkey PRIMARY KEY ( id_audit_contribuyente )
 );

CREATE  TABLE "public".categoria ( 
	id_categoria         integer  NOT NULL ,
	nombre               varchar(50)  NOT NULL ,
	CONSTRAINT pk_categoria_id_categoria PRIMARY KEY ( id_categoria )
 );

CREATE  TABLE "public".configuracion ( 
	tabla                varchar(100)  NOT NULL ,
	numero               integer  NOT NULL ,
	CONSTRAINT pk_configuracion_tabla PRIMARY KEY ( tabla )
 );

CREATE  TABLE "public".contribuyente ( 
	dni                  char(8)  NOT NULL ,
	nombres              varchar(100)  NOT NULL ,
	apellido_paterno     varchar(100)  NOT NULL ,
	apellido_materno     varchar(100)  NOT NULL ,
	codigo               varchar(11)   ,
	celular              varchar(10)   ,
	direccion            varchar(200)   ,
	referencia           varchar(200)   ,
	correo               varchar(20)   ,
	estado               char(1)   ,
	CONSTRAINT pk_contribuyente_dni PRIMARY KEY ( dni )
 );

CREATE  TABLE "public".deuda ( 
	id_deuda             integer  NOT NULL ,
	dni                  char(8)   ,
	id_categoria         integer   ,
	subtotal             numeric(15,2)   ,
	mora                 numeric(12,2) DEFAULT 0  ,
	estado               char(1)   ,
	total                numeric(15,2)   ,
	descuento            numeric(15,2) DEFAULT 0  ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	cuotas               char(1) DEFAULT 'N'::bpchar NOT NULL ,
	CONSTRAINT pk_deuda_id_deuda PRIMARY KEY ( id_deuda )
 );

CREATE  TABLE "public".menu ( 
	id_menu              integer  NOT NULL ,
	nombre               varchar(100)   ,
	CONSTRAINT pk_menu_id_menu PRIMARY KEY ( id_menu )
 );

COMMENT ON TABLE "public".menu IS 'row usuario: N: No, S: Si, A: Ambos';

CREATE  TABLE "public".notifcacion_usuario ( 
	dni                  char(8)  NOT NULL ,
	"data"               varchar  NOT NULL ,
	estado               char(1) DEFAULT 'A'::bpchar NOT NULL ,
	CONSTRAINT pk_notifcacion_usuario_dni PRIMARY KEY ( dni, "data" )
 );

CREATE  TABLE "public".notificacion ( 
	id_notificacion      integer DEFAULT nextval('notificacion_id_notificacion_seq'::regclass) NOT NULL ,
	app                  char(1)  NOT NULL ,
	correo               char(1)  NOT NULL ,
	web                  char(1)  NOT NULL ,
	mensaje              varchar(300)  NOT NULL ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	dni                  char(8)   ,
	CONSTRAINT pk_notificacion_id_notificacion PRIMARY KEY ( id_notificacion )
 );

CREATE  TABLE "public".perfiles ( 
	id_perfiles          integer  NOT NULL ,
	nombre               varchar(100)   ,
	CONSTRAINT pk_perfiles_id_perfiles PRIMARY KEY ( id_perfiles )
 );

COMMENT ON TABLE "public".perfiles IS '1= administrador\n2= contribuyente';

CREATE  TABLE "public".tipo_transaccion ( 
	id_tipo_transaccion  integer  NOT NULL ,
	nombre               varchar  NOT NULL ,
	CONSTRAINT pk_tipo_transaccion_id_tipo_transaccion PRIMARY KEY ( id_tipo_transaccion ),
	CONSTRAINT pk_tipo_transaccion_id_tipo_transaccion PRIMARY KEY ( id_tipo_transaccion )
 );

CREATE  TABLE "public".usuario ( 
	dni                  char(8)  NOT NULL ,
	"password"           char(32)  NOT NULL ,
	estado               char(1) DEFAULT 'A'::bpchar  ,
	CONSTRAINT pk_usuario_dni PRIMARY KEY ( dni )
 );

CREATE  TABLE "public".deuda_detalle ( 
	id_deuda             integer  NOT NULL ,
	cuotas               integer  NOT NULL ,
	mora                 numeric(12,2) DEFAULT 0  ,
	fecha                date   ,
	id_tipo_transaccion  integer   ,
	fecha_transaccion    timestamp   ,
	numero_transaccion   varchar(20)   ,
	estado               char(1) DEFAULT 'A'::bpchar NOT NULL ,
	subtotal             numeric(15,2) DEFAULT 0  ,
	descuento            numeric(15,2) DEFAULT 0  ,
	total                numeric(15,2)  NOT NULL ,
	CONSTRAINT pk_deuda_detalle_id_deuda PRIMARY KEY ( id_deuda, cuotas )
 );

CREATE  TABLE "public".menu_perfiles ( 
	id_menu              integer  NOT NULL ,
	id_perfiles          integer  NOT NULL ,
	estado               char(1) DEFAULT 'A'::bpchar NOT NULL ,
	CONSTRAINT pk_menu_perfiles_id_menu PRIMARY KEY ( id_menu, id_perfiles ),
	CONSTRAINT unq_menu_perfiles_id_perfiles UNIQUE ( id_perfiles, id_menu ) 
 );

CREATE  TABLE "public".usuario_menu_perfiles ( 
	dni                  char(8)  NOT NULL ,
	id_menu              integer  NOT NULL ,
	id_perfiles          integer  NOT NULL ,
	estado               char(1)  NOT NULL ,
	CONSTRAINT pk_usuario_menu_perfiles_dni PRIMARY KEY ( dni, id_menu, id_perfiles )
 );

CREATE OR REPLACE FUNCTION public.f_contribuyente_delete(p_dni character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_contribuyente record;
	
	begin
		select * into v_contribuyente from contribuyente where dni like p_dni;
		--guardar el audit
		INSERT INTO audit.audit_contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado, 
				dni_usuario, 
				tipo)
		    VALUES (
				v_contribuyente.dni,
				v_contribuyente.nombres,
				v_contribuyente.apellido_paterno,
				v_contribuyente.apellido_materno,
				v_contribuyente.codigo,
				v_contribuyente.celular,
				v_contribuyente.direccion, 
				v_contribuyente.referencia, 
				v_contribuyente.correo, 
				v_contribuyente.estado, 
				p_dni_usuario, 
				upper(p_tipo)
		    );
		--eliminar el registro (solo se cambia el estado a 'I' inactivo)
		UPDATE public.contribuyente
		   SET  estado='I'
		 WHERE dni=p_dni;

		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_contribuyente_edit(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_contribuyente record;
	
	begin
		select * into v_contribuyente from contribuyente where dni like p_dni;
		
		--editar el registro
		UPDATE public.contribuyente
		   SET 	nombres=p_nombres, 
			apellido_paterno=p_apellido_paterno, 
			apellido_materno=p_apellido_materno, 
			codigo=p_codigo, 
			celular=p_celular, 
			direccion=p_direccion, 
			referencia=p_referencia, 
			correo=p_correo, 
			estado=p_estado
		 WHERE dni = p_dni;

		--guardar el audit
		INSERT INTO audit.audit_contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado, 
				dni_usuario, 
				tipo)
		    VALUES (
				v_contribuyente.dni,
				v_contribuyente.nombres,
				v_contribuyente.apellido_paterno,
				v_contribuyente.apellido_materno,
				v_contribuyente.codigo,
				v_contribuyente.celular,
				v_contribuyente.direccion,
				v_contribuyente.referencia,
				v_contribuyente.correo,
				v_contribuyente.estado,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_contribuyente_new(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_contribuyente record;
	
	begin
		--se agrego el registro
		INSERT INTO public.contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado)
		    VALUES (
				p_dni,
				p_nombres,
				p_apellido_paterno,
				p_apellido_materno,
				p_codigo,
				p_celular,
				p_direccion,
				p_referencia,
				p_correo,
				p_estado
		    );

		--guardar el audit
		INSERT INTO audit.audit_contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado, 
				dni_usuario, 
				tipo)
		    VALUES (
				p_dni,
				p_nombres,
				p_apellido_paterno,
				p_apellido_materno,
				p_codigo,
				p_celular,
				p_direccion,
				p_referencia,
				p_correo,
				p_estado,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_cuotas(p_id_deuda integer, p_nro_cuotas integer, p_fecha character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_pago record;
	declare v_deuda_total numeric;
	declare v_deuda_mensual numeric;
	declare v_cuota int;
	
	begin
		
		select total into v_deuda_total from public.deuda where id_deuda = p_id_deuda;
		v_deuda_mensual = v_deuda_total / p_nro_cuotas;
		v_cuota = 1;

		 FOR v_pago IN (select (DATE (p_fecha) + (interval '1' month * generate_series(1,p_nro_cuotas)))::DATE as fecha)
		  LOOP
			INSERT INTO public.deuda_detalle(id_deuda, cuotas, fecha, total)
			VALUES (p_id_deuda,v_cuota,v_pago.fecha,v_deuda_mensual);
			v_cuota = v_cuota + 1;
		  END LOOP;

		UPDATE public.deuda
		   SET cuotas='S'
		 WHERE id_deuda=p_id_deuda;

		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_delete(p_id_deuda integer, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_deuda record;
	
	begin
		select * into v_deuda from public.deuda where id_deuda = p_id_deuda;
		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda,
				dni,
				id_categoria,
				subtotal,
				mora,
				estado,
				total,
				descuento,
				dni_usuario,
				tipo)
		    VALUES (
				v_deuda.id_deuda,
				v_deuda.dni,
				v_deuda.id_categoria,
				v_deuda.subtotal,
				v_deuda.mora,
				v_deuda.estado,
				v_deuda.total, 
				v_deuda.descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		--eliminar el registro (solo se cambia el estado a 'I' inactivo)
		UPDATE public.deuda
		   SET  estado='I'
		 WHERE id_deuda=p_id_deuda;

		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_edit(p_id_deuda integer, p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_deuda record;
	
	begin
		select * into v_deuda from public.deuda where id_deuda = p_id_deuda;
		
		--editar el registro
		UPDATE public.deuda
		   SET 	dni=p_dni, 
			id_categoria=p_id_categoria, 
			subtotal=p_subtotal, 
			mora=p_mora, 
			estado=p_estado, 
			total=p_total, 
			descuento=p_descuento
		 WHERE id_deuda=p_id_deuda;

		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento,
				dni_usuario,
				tipo)
		    VALUES (
				v_deuda.id_deuda,
				v_deuda.dni,
				v_deuda.id_categoria,
				v_deuda.subtotal,
				v_deuda.mora,
				v_deuda.estado,
				v_deuda.total,
				v_deuda.descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_new(p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_deuda record;
	declare v_id_deuda int;
	
	begin
		select * into v_id_deuda from public.f_generar_correlativo('deuda');
		
		--se agrego el registro
		INSERT INTO public.deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento)
		VALUES (
				v_id_deuda, 
				p_dni, 
				p_id_categoria, 
				p_subtotal, 
				p_mora, 
				p_estado, 
				p_total, 
				p_descuento
		);

		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento,
				dni_usuario, 
				tipo)
		    VALUES (
				v_id_deuda, 
				p_dni, 
				p_id_categoria, 
				p_subtotal, 
				p_mora, 
				p_estado, 
				p_total, 
				p_descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		--actualizando el correlativo
		UPDATE public.configuracion
		   SET  numero=numero+1
		 WHERE tabla='deuda';
		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_generar_correlativo(p_tabla character varying)
 RETURNS SETOF integer
 LANGUAGE plpgsql
AS $function$
	
	begin
		return query
		select 
			c.numero+1 
		from 
			configuracion c 
		where 
			c.tabla = p_tabla;
	end
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_login(p_user character varying, p_password character varying)
 RETURNS TABLE(r_mensaje character varying, r_dni character varying, r_nombres character varying, r_apellido_paterno character varying, r_apellido_materno character varying, r_id_menu integer, r_id_perfiles integer, r_menu character varying, r_perfil character varying)
 LANGUAGE plpgsql
AS $function$
	declare v_usuario_estado varchar;
	
	begin
		select estado into v_usuario_estado from usuario where dni like p_user and password like MD5(p_password);
		if v_usuario_estado != 'A' || v_usuario_estado is null then
			return query select 'Hubo un error en los datos, ingrese bien o contactese con soporte'::varchar,''::varchar,''::varchar,''::varchar,''::varchar,0,0,''::varchar,''::varchar;
		else
			return query select 'ok'::varchar,c1.dni::varchar, c1.nombres, c1.apellido_paterno, c1.apellido_materno, mp.id_menu, mp.id_perfiles, m1.nombre as menu, p.nombre as perfil
				FROM "public".usuario_menu_perfiles ump 
					INNER JOIN "public".usuario u ON ( ump.dni = u.dni  )  
						INNER JOIN "public".contribuyente c1 ON ( u.dni = c1.dni  )  
					INNER JOIN "public".menu_perfiles mp ON ( ump.id_perfiles = mp.id_perfiles AND ump.id_menu = mp.id_menu  )  
						INNER JOIN "public".menu m1 ON ( mp.id_menu = m1.id_menu  )  
						INNER JOIN "public".perfiles p ON ( mp.id_perfiles = p.id_perfiles  )  
				WHERE mp.estado like 'A' AND
				u.dni like p_user AND
				u.password like MD5(p_password) AND
				u.estado like 'A';
		end if;
			  
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_usuario_perfiles(p_dni character varying)
 RETURNS TABLE(perfiles integer)
 LANGUAGE plpgsql
AS $function$
	declare v_usuario_estado varchar;
	
	begin
		return query
		SELECT p.id_perfiles
		FROM "public".usuario_menu_perfiles ump 
			INNER JOIN "public".usuario u ON ( ump.dni = u.dni  )  
			INNER JOIN "public".menu_perfiles mp ON ( ump.id_perfiles = mp.id_perfiles AND ump.id_menu = mp.id_menu  )  
				INNER JOIN "public".perfiles p ON ( mp.id_perfiles = p.id_perfiles  )  
		WHERE u.dni like p_dni
		GROUP BY u.dni, p.id_perfiles, p.nombre;
			  
	end;
	
$function$
;

CREATE VIEW public.v_deudas AS  SELECT c1.nombres,
    c1.apellido_paterno,
    c1.apellido_materno,
    d.id_deuda,
    d.dni,
    d.id_categoria,
    d.subtotal,
    d.mora,
    d.total,
    d.estado,
    c2.nombre AS categoria,
    d.descuento,
    d.cuotas
   FROM ((contribuyente c1
     JOIN deuda d ON ((c1.dni = d.dni)))
     JOIN categoria c2 ON ((d.id_categoria = c2.id_categoria)));;

ALTER TABLE "public".deuda ADD CONSTRAINT fk_deuda_categoria FOREIGN KEY ( id_categoria ) REFERENCES "public".categoria( id_categoria );

ALTER TABLE "public".deuda ADD CONSTRAINT fk_deuda_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni );

ALTER TABLE "public".deuda_detalle ADD CONSTRAINT fk_deuda_detalle_deuda FOREIGN KEY ( id_deuda ) REFERENCES "public".deuda( id_deuda );

ALTER TABLE "public".deuda_detalle ADD CONSTRAINT fk_deuda_detalle_tipo_transaccion FOREIGN KEY ( id_tipo_transaccion ) REFERENCES "public".tipo_transaccion( id_tipo_transaccion );

ALTER TABLE "public".menu_perfiles ADD CONSTRAINT fk_menu_perfiles_menu FOREIGN KEY ( id_menu ) REFERENCES "public".menu( id_menu );

ALTER TABLE "public".menu_perfiles ADD CONSTRAINT fk_menu_perfiles_perfiles FOREIGN KEY ( id_perfiles ) REFERENCES "public".perfiles( id_perfiles );

ALTER TABLE "public".notifcacion_usuario ADD CONSTRAINT fk_notifcacion_usuario_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni );

ALTER TABLE "public".notificacion ADD CONSTRAINT fk_notificacion_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni );

ALTER TABLE "public".usuario ADD CONSTRAINT fk_usuario_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni );

ALTER TABLE "public".usuario_menu_perfiles ADD CONSTRAINT fk_usuario_menu_perfiles_usuario FOREIGN KEY ( dni ) REFERENCES "public".usuario( dni );

ALTER TABLE "public".usuario_menu_perfiles ADD CONSTRAINT fk_usuario_menu_perfiles_menu_perfiles FOREIGN KEY ( id_perfiles, id_menu ) REFERENCES "public".menu_perfiles( id_perfiles, id_menu );

INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 12, 103, '46705014', 1, 234.00, 234.00, 'A', 234.00, 234.00, '46705014', 'NUEVO', '2020-06-19 10:59:45.677029' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 13, 104, '46705014', 1, 123.00, 123.00, 'A', 12.00, 12.00, '46705014', 'NUEVO', '2020-06-19 11:00:55.070878' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 14, 105, '46705014', 1, 123.00, 123.00, 'A', 12.00, 12.00, '46705014', 'NUEVO', '2020-06-19 11:02:09.85815' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 15, 106, '46705014', 1, 123.00, 123.00, 'A', 12.00, 123.00, '46705014', 'NUEVO', '2020-06-19 11:02:49.924315' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 16, 107, '46705014', 1, 123.00, 12.00, 'A', 12.00, 12.00, '46705014', 'NUEVO', '2020-06-19 11:03:44.940229' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 17, 108, '46705014', 1, 123.00, 12.00, 'A', 12.00, 1.00, '46705014', 'NUEVO', '2020-06-19 11:05:16.044759' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 18, 0, '46705014', 0, 56.00, 90.00, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-19 11:09:48.240901' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 19, 109, '46705014', 1, 12.00, 12.00, 'A', 12.00, 12.00, '46705014', 'NUEVO', '2020-06-19 11:13:05.318653' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 20, 110, '12341234', 1, 12.00, 12.00, 'A', 12.00, 12.00, '46705014', 'NUEVO', '2020-06-19 11:13:14.832668' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 21, 111, '46705014', 1, 123.00, 12.00, 'A', 12.00, 12.00, '46705014', 'NUEVO', '2020-06-19 13:08:53.421083' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 22, 0, '46705014', 0, 56.00, 91.00, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-19 13:18:24.549444' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 23, 0, '46705014', 0, 56.00, 90.00, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-19 13:18:36.588024' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 24, 112, '12341234', 1, 0.00, 0.00, 'A', 10.00, 0.00, '46705014', 'NUEVO', '2020-06-24 00:09:16.029022' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 25, 0, '46705014', 0, 56.00, 91.00, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:35:56.86209' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 26, 0, '46705014', 0, 56.00, 90.00, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:38:39.542365' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 27, 0, '46705014', 0, 56.00, 89.00, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:43:56.788594' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 28, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:44:43.025962' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 29, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:46:25.366305' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 30, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:48:35.615672' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 31, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:50:07.077087' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 32, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:51:36.714418' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 33, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:52:56.15672' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 34, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:54:57.930869' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 35, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:55:30.895644' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 36, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:55:46.247805' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 38, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:58:04.641238' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 39, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 10:58:54.127462' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 40, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:01:13.144658' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 41, 0, '46705014', 0, 56.00, 89.01, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:02:27.760776' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 42, 0, '46705014', 0, 56.00, 89.02, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:03:07.069788' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 43, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:06:39.96266' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 44, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:08:26.413731' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 45, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:10:16.012885' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 46, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:11:09.887378' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 47, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:12:39.298855' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 48, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:14:05.426108' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 49, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:14:34.146767' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 50, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:21:46.780438' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 51, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:22:30.878017' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 52, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:23:04.580571' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 53, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:23:58.072629' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 54, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:24:20.617747' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 55, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:24:45.123674' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 56, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:25:22.820878' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 57, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:26:53.498211' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 58, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:27:16.207651' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 59, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:28:33.200604' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 60, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:32:00.930211' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 61, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:33:19.053863' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 62, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:33:49.476179' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 63, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:34:21.150258' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 64, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:34:43.222989' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 65, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:44:40.831972' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 66, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:47:35.314027' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 67, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:47:44.022693' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 68, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:51:51.021952' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 69, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:52:16.18681' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 70, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:53:40.776944' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 71, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:55:16.104345' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 72, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 11:56:59.445752' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 74, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:25:22.316705' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 75, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:26:28.222377' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 76, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:28:38.325566' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 77, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:29:31.461614' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 78, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:31:07.637188' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 79, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:32:40.850894' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 81, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:36:47.6176' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 82, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:37:15.181759' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 83, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:39:13.676434' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 84, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:40:20.916806' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 85, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:42:17.861793' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 86, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:49:58.796502' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 87, 1, 'sntxtbyn', 0, 671200093904.93, 9186071188.99, 'I', 4889045498515.87, null, '46705014', 'EDITAR', '2020-06-26 12:50:18.098359' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 88, 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '46705014', 'EDITAR', '2020-06-26 12:56:29.575915' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 0, 'Mike like swimming. Mike watches football. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 1, 'I watches football. I watches football. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 2, 'Rudi like swimming. I loves flowers. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 3, 'Mike watches football. Mike like sports. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 4, 'Mike watches football. Mike like swimming. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 5, 'Rudi watches football. Mike watches football. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 6, 'I like sports. Rudi loves flowers. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 7, 'Mike loves flowers. Mike watches football. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 8, 'I watches football. I loves flowers. ' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 9, 'Mike watches football. Mike watches football. ' ); 
INSERT INTO "public".configuracion( tabla, numero ) VALUES ( 'perfiles', 0 ); 
INSERT INTO "public".configuracion( tabla, numero ) VALUES ( 'SSS', 1 ); 
INSERT INTO "public".configuracion( tabla, numero ) VALUES ( 'deuda', 112 ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'opnbmrbf', 'Mike watches football. Mike watches football. ', 'Mike watches football. Mike watches football. ', 'I watches football. Rudi like swimming. ', 'mfn', 'dpq', 'Anne is walking. John is shopping. Tony is shopping. John has free time. Anne has free time. ', 'John bought new car. Anne has free time. Tony has free time. John has free time. Anne has free time. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'btqvmysa', 'Mike watches football. I like swimming. ', 'Rudi loves flowers. Mike watches football. ', 'Rudi watches football. Rudi watches football. ', 'ljo', 'wtu', 'Tony bought new car. Tony has free time. Anne is shopping. ', 'Anne bought new car. Tony bought new car. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'tmwrhimw', 'I loves flowers. Rudi watches football. ', 'I watches football. Mike watches football. ', 'I watches football. I loves flowers. ', 'wdn', 'zjy', 'Anne is shopping. Anne has free time. Anne has free time. Tony is shopping. Tony has free time. ', 'Tony is shopping. Anne is shopping. John has free time. ', 'hj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'drmqglgc', 'Mike like sports. Mike watches football. ', 'I loves flowers. Mike watches football. ', 'Mike loves flowers. Mike loves flowers. ', 'azi', 'aoq', 'Tony bought new car. Anne has free time. John has free time. John has free time. ', 'John bought new car. John bought new car. ', 'hj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'kluzmgbx', 'Rudi watches football. Mike watches football. ', 'Mike like swimming. I watches football. ', 'I like swimming. Rudi watches football. ', 'qkf', 'rfr', 'Tony bought new car. Anne bought new car. John bought new car. John bought new car. ', 'John bought new car. John is shopping. Tony bought new car. Anne has free time. ', 'fg', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'qhdweggb', 'I watches football. I loves flowers. ', 'I like sports. Rudi watches football. ', 'Mike watches football. I watches football. ', 'uwd', 'msm', 'Anne is shopping. Anne is walking. John bought new car. John is walking. Anne bought new car. ', 'Anne bought new car. Anne bought new car. Tony bought new car. John has free time. ', 'hj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'zadlflzo', 'Mike like swimming. Mike watches football. ', 'Mike watches football. ', 'Mike watches football. I like sports. ', 'xuf', 'epy', 'Anne bought new car. Anne is shopping. Anne bought new car. Tony has free time. Tony is walking. ', 'Anne bought new car. Tony bought new car. John bought new car. ', null, 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'gdgpqssh', 'I watches football. Rudi like sports. ', 'I loves flowers. ', 'Mike watches football. ', 'wmi', 'mcr', 'Anne is shopping. Anne bought new car. ', 'Tony bought new car. Tony bought new car. John has free time. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'bwycsxpx', 'Mike watches football. Rudi loves flowers. ', 'Mike like sports. ', 'Rudi like swimming. Rudi loves flowers. ', 'vzo', 'sie', 'Anne bought new car. John has free time. Tony bought new car. Tony bought new car. John bought new car. ', 'John bought new car. John is walking. ', 'hj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'ocwfuyyf', 'Rudi like swimming. I loves flowers. ', 'Mike watches football. I watches football. ', 'I watches football. Rudi like sports. ', 'uho', 'jvm', 'Anne bought new car. Anne has free time. Anne is walking. Anne has free time. John bought new car. ', 'Anne bought new car. Tony has free time. Tony has free time. Tony bought new car. Tony is shopping. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'oinzuwji', 'Mike watches football. Mike like sports. ', 'I like sports. Mike watches football. ', 'Mike watches football. Rudi watches football. ', 'lsn', 'sii', 'Anne is walking. John is walking. John has free time. Anne has free time. Tony has free time. ', 'Anne has free time. Anne is shopping. John bought new car. Anne bought new car. John has free time. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'juekbcrn', 'I watches football. Rudi loves flowers. ', 'Rudi loves flowers. Rudi like sports. ', 'Rudi like swimming. Mike watches football. ', 'rsp', 'uix', 'Anne bought new car. Tony bought new car. Tony bought new car. John bought new car. Anne has free time. ', 'John has free time. Anne bought new car. Tony has free time. Tony is shopping. Tony is shopping. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'drdraxgs', 'I watches football. I watches football. ', 'Mike watches football. Rudi watches football. ', 'Mike like swimming. Rudi like sports. ', 'hmv', 'vlt', 'John bought new car. Anne is walking. Tony bought new car. John bought new car. Tony is shopping. ', 'Anne has free time. Tony has free time. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'chohycfx', 'I like sports. Rudi loves flowers. ', 'Rudi watches football. ', 'Mike loves flowers. I loves flowers. ', 'lps', 'cmm', 'Tony is shopping. John is shopping. Tony bought new car. John is walking. ', 'Anne has free time. Tony is shopping. ', 'ghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'eifeuarx', 'Mike like sports. ', 'I like swimming. ', 'Rudi like sports. I watches football. ', 'kev', 'tdt', 'Anne bought new car. Anne is shopping. Tony bought new car. Anne is walking. John has free time. ', 'John has free time. Anne has free time. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'upygimkv', 'I watches football. Rudi loves flowers. ', 'Mike like sports. I like swimming. ', 'Rudi like swimming. Mike loves flowers. ', 'vom', 'itm', 'Anne bought new car. Tony is shopping. ', 'Tony has free time. Tony bought new car. Tony has free time. Tony has free time. Tony is shopping. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'wmdbsrzi', 'Mike watches football. I loves flowers. ', 'Rudi loves flowers. Rudi watches football. ', 'Mike watches football. Mike watches football. ', 'ivj', 'uku', 'Anne bought new car. Anne has free time. John has free time. Anne has free time. John has free time. ', 'Anne has free time. Tony is shopping. Anne bought new car. Tony is walking. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'sntxtbyn', 'Mike loves flowers. Mike watches football. ', 'Mike watches football. Rudi watches football. ', 'Mike like sports. Mike loves flowers. ', 'kmk', 'oiv', 'Anne bought new car. Anne is shopping. John is walking. Anne is shopping. ', 'Anne bought new car. John is shopping. Tony bought new car. Anne bought new car. John bought new car. ', 'hj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '46705014', 'SILVIO REYNALDO', 'PEÑA', 'DIAZ2', '000000001', '978438896', 'aqui4', 'gfhjfghj', 'silviopd01@gmail.com', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '12341234', 'aaa', 'bb', 'ccc', 'ddd', 'eeee', 'ffff', 'gggg', 'fghj', 'I' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'bhubevik', 'Mike like sports. Mike loves flowers. ', 'Rudi loves flowers. ', 'I like swimming. Rudi watches football. ', 'kqb', 'lka', 'Anne has free time. John is walking. Tony bought new car. Tony is shopping. Tony is walking. ', 'Anne bought new car. Anne bought new car. John bought new car. Anne has free time. Tony bought new car. ', 'fghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( 'ajzjfxns', 'Mike watches football. Mike like swimming. ', 'Mike watches football. Rudi watches football. ', 'Rudi watches football. ', 'jld', 'biq', 'Anne has free time. Anne has free time. John bought new car. Tony is walking. ', 'Anne is walking. Anne is shopping. John bought new car. Anne is shopping. John has free time. ', 'ghj', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '12345671', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '87654328', 'z', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '43214321', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '88888888', 'sdfgdsfg1', 'sdfg2', 'dsfg3', 'dsgfdsfg', 'sdfg4', 'sdfg6', 'sdfg7', 'dsfg5', 'I' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 2, '46705014', 8, 5751459632416.19, 2844491165.35, 'i', 8263097621290.81, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 3, 'btqvmysa', 0, 6603196166874.72, 248135604.33, 'b', 6977123432244.60, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 4, 'upygimkv', 9, 664934520396.18, 7992049574.79, 'w', 3457472586625.59, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 5, null, 4, 8111701180816.85, 1512321021.19, 'u', 2192445019248.66, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 6, 'bhubevik', 5, 8648662291873.66, 4204191082.87, 'n', 7315296811767.05, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 7, 'gdgpqssh', null, 9102762576431.91, 33807088.94, 'y', 2220016021250.67, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 8, 'gdgpqssh', 7, 413469603656.58, 3604859702.86, 'z', 243137544313.88, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 9, 'gdgpqssh', 9, 9775124263641.76, 8236490953.54, 'c', 3928764411717.09, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 10, 'sntxtbyn', 0, 5425455205006.15, 6445663470.29, 'f', 6649705153926.66, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 11, 'chohycfx', 6, 935458661635.85, 7515484122.03, 'g', 2831489393823.21, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 12, 'kluzmgbx', 8, 4058816196450.47, 4511089209.78, 'n', 601543723577.84, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 13, 'opnbmrbf', 0, null, 8076235760.86, 'f', 5118487849555.99, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 14, 'drdraxgs', 4, 9262506241981.94, 4163741585.40, 'h', null, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 15, 'juekbcrn', 3, 4874576197509.99, 8930844720.84, 'm', 401602735740.98, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 16, 'btqvmysa', 9, 1166505499274.86, 7816198922.12, 'l', 8369530804563.83, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 17, null, 5, 5357579968780.57, 3468149471.57, 'l', 3545521456938.44, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 18, 'eifeuarx', 4, 3761619218306.64, 4883437440.29, 'b', 4738395232771.46, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 19, 'btqvmysa', 0, 6907794197994.39, 5704670181.05, 'j', 3497419296226.17, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 20, 'wmdbsrzi', 5, 979102303651.60, 8401954109.39, 'q', 6263868093865.74, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 21, 'qhdweggb', 8, 2600071331985.62, 8976446022.88, 'i', 341070342389.90, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 22, '46705014', 2, 4450206924288.64, 8675423013.80, 'w', 1605381106188.00, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 23, null, 1, 7478608051087.58, 1481314823.74, 'p', 384429147926.94, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 24, 'opnbmrbf', 9, 2071236993247.45, 4523228396.98, 'e', 9654775497789.73, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 25, null, 2, 4514138761996.66, 5617746496.12, 'c', 8334519232455.43, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 26, 'upygimkv', 6, 3672603443430.14, 8290590913.27, 'w', 8024463303473.45, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 27, 'ajzjfxns', null, 5110870978098.35, 3353855085.27, 'n', 2738463993573.34, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 28, 'zadlflzo', 7, 5469092329628.73, 4141690077.42, 'w', 4685189872863.32, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 29, 'tmwrhimw', 7, 3196490906394.88, 7168057834.55, 'b', 4756824986787.38, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 30, 'bhubevik', 0, 2939867087274.60, null, 'g', null, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 31, 'ajzjfxns', 5, 8995079522976.69, 4503356772.69, 'x', 8579193921813.17, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 32, 'ocwfuyyf', 0, 9172625177844.90, 5897271874.46, 's', 3003087415279.37, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 33, '46705014', 8, 2186373340296.88, null, 'r', 8363621386636.73, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 34, null, 1, null, 9908046552.28, 's', 5660211001083.58, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 35, 'ocwfuyyf', null, 1028352068660.34, 5041216809.26, 'm', 1843789787786.43, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 36, 'ocwfuyyf', 9, 2453103003689.44, 8588650334.53, 'e', 3815759593116.39, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 37, 'bwycsxpx', 2, 2842715239488.99, 8339316382.05, 'q', 9438636522157.73, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 38, 'bhubevik', 5, 4123379132401.53, 4736684435.65, 'd', 859345496871.17, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 39, 'ajzjfxns', null, 773598893237.57, 6579202772.73, null, 5740323579218.08, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 40, 'sntxtbyn', 1, 6918341643978.24, 6267226852.58, 'w', null, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 41, 'oinzuwji', 9, 4953536576688.27, null, null, 7760899217183.31, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 42, 'qhdweggb', null, 2625500803796.08, 9754713781.62, 't', 9216850346815.33, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 43, 'oinzuwji', 1, null, 8372270928.53, 'j', 8771953333092.27, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 44, 'wmdbsrzi', 0, 2981183033191.98, 912834929.39, 'w', 4499165308736.79, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 45, '46705014', 4, 7515271894602.31, 3043312978.66, 'd', 148921285248.86, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 46, 'tmwrhimw', 4, 1418184406787.21, 7901650177.10, 'f', 1198343302309.77, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 47, 'btqvmysa', 2, 3960701319190.61, 3754343445.35, 'm', 9713071930633.80, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 48, 'drdraxgs', 7, 6042412818551.28, 4104530324.63, null, null, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 49, 'btqvmysa', 7, 8898961712853.23, 6767446309.69, 'c', 826088085547.61, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 50, 'bhubevik', 3, 3066708338304.85, 7889786365.49, 'n', 617818830617.11, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 51, 'oinzuwji', 7, null, 4490789587.72, 'k', 7731517765133.41, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 52, 'chohycfx', 8, 3176430606960.49, null, 'p', 4634886090116.54, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 53, 'qhdweggb', 4, null, 2471229866.84, 'm', 616244270715.07, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 54, 'chohycfx', 1, 9607282832050.74, null, 'a', 5741708101320.92, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 55, 'qhdweggb', 1, 8396911895372.22, 1482220565.02, 'o', 1653858759089.19, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 56, null, 1, 3542298467390.77, 2706163541.34, 'j', 3020293975297.42, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 57, 'ocwfuyyf', 5, 653711122331.54, 9799180225.29, 'k', 8315516531101.29, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 58, 'btqvmysa', 0, 4670750966666.71, 758028699.52, null, 7427397984237.11, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 59, 'ajzjfxns', 1, null, 7666179801.12, 'x', 2699068551195.59, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 60, 'chohycfx', 6, null, 9315041077.25, 'y', 9335838395475.87, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 61, 'ajzjfxns', 5, 4501040343132.85, 9373778601.26, 'm', 5346584786422.00, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 62, 'drdraxgs', 0, 6158906820453.33, 2463821060.33, 's', 5261531389451.62, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 63, 'drdraxgs', 5, 4625401024526.38, 5394808515.39, 'j', 5095160680420.47, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 64, 'bwycsxpx', 5, 6578136684543.36, 8006178712.79, 'j', 4579036976364.65, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 65, 'opnbmrbf', 4, 4307488087332.94, 978048655.93, 'y', 8641945529403.90, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 67, 'chohycfx', 0, 7175281529688.45, 1723097588.68, 'v', 9699804901836.30, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 68, 'ajzjfxns', 4, 3638862261969.60, 4802426661.37, 'q', 3294287664813.12, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 69, 'juekbcrn', 3, 4021684212906.34, 7341252344.40, 'x', 9342341079978.87, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 70, 'drdraxgs', 8, 7237737918991.58, 4339439076.81, null, 5567949891691.79, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 71, 'opnbmrbf', 3, 527860688369.62, 3040256495.77, 'c', 9571328420984.01, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 72, 'bwycsxpx', 6, 7516112894408.51, 6394339259.87, 'r', 9900152562409.65, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 73, 'chohycfx', 7, 7958947745709.26, 2914131683.25, 's', 4790556748196.64, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 74, 'qhdweggb', 7, 3502083689487.43, 8519289445.61, 'e', 2024118420618.75, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 75, 'upygimkv', 5, 1986122945256.67, 2717532880.23, 'n', 5599909399191.99, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 76, 'ocwfuyyf', 0, 1374219317125.22, 2816642786.11, 'v', 5122128231362.53, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 77, 'upygimkv', 9, 3548536382409.32, 5830060348.87, 'e', 6334655222065.51, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 78, 'gdgpqssh', 3, 6998258987587.71, 3815523199.70, 'n', 9836743868476.01, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 79, 'eifeuarx', 6, 2780031322993.14, 4963599216.69, 'd', 982278408232.52, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 80, 'zadlflzo', 1, 2736756018091.64, 6927802735.33, 'h', 63105636747.65, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 81, 'qhdweggb', null, null, null, 'u', 6784565756608.73, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 82, 'kluzmgbx', 3, 1025719492935.91, 1974340382.69, 'e', 6266553171196.87, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 83, null, 9, 4731793355265.40, 9961244691.71, 'v', 342484332754.34, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 84, 'btqvmysa', 4, 8038160941344.37, 3452636416.73, 'g', 7995334954823.05, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 85, 'oinzuwji', 7, null, 5643015057.37, 'd', 7934433751207.62, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 86, 'wmdbsrzi', 9, 976356488712.17, 2018762638.50, 'a', null, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 87, 'upygimkv', 8, 7269686467339.87, null, 'k', 3754883488064.09, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 88, 'eifeuarx', 9, 4534290202590.06, 8628306389.25, 'w', 4648648319824.08, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 89, 'opnbmrbf', 3, 9619134827597.87, 666195418.75, 'v', 4256154262778.05, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 90, null, null, 2292817488272.21, 2640969715.14, 'p', 1806666778850.12, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 91, 'drdraxgs', 6, 9787864528682.68, 2838311505.02, 'u', 3057639079471.76, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 92, 'tmwrhimw', 1, 9018836176441.32, 9632164508.36, 'h', 8894147534566.55, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 93, 'gdgpqssh', null, 6532434729515.87, 8745334799.50, 'j', 5861022132730.78, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 94, 'opnbmrbf', 9, 610456087852.14, 8194213438.98, 'e', 5632416624625.16, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 95, 'btqvmysa', 5, 8118516737493.88, 6042961247.44, 's', 8906104626077.45, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 96, 'upygimkv', 8, 47445153722.99, 9360987444.33, 'o', 9765927193264.51, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 97, 'ajzjfxns', 1, 2031967458031.13, 7224228921.82, 'w', 4010779748863.71, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 98, null, 3, 303777562231.62, 6875268336.90, 'd', 1300744489453.56, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 99, null, 7, 1227166563489.61, 7709757462.64, 'u', 969211238332.81, null, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 101, '46705014', 0, 12.00, 3.00, 'A', 14.00, 11.00, '2020-06-19 00:54:59.57504', 'S' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 102, '46705014', 4, 12.00, 0.00, 'A', 12.00, 0.00, '2020-06-19 00:54:59.57504', 'S' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 66, '46705014', 9, 8327004274412.26, 8079797525.66, 'k', 448369913355.40, null, '2020-06-19 00:54:59.57504', 'S' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 104, '46705014', 1, 123.00, 123.00, 'A', 12.00, 12.00, '2020-06-19 11:00:55.070878', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 105, '46705014', 1, 123.00, 123.00, 'A', 12.00, 12.00, '2020-06-19 11:02:09.85815', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 106, '46705014', 1, 123.00, 123.00, 'A', 12.00, 123.00, '2020-06-19 11:02:49.924315', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 107, '46705014', 1, 123.00, 12.00, 'A', 12.00, 12.00, '2020-06-19 11:03:44.940229', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 108, '46705014', 1, 123.00, 12.00, 'A', 12.00, 1.00, '2020-06-19 11:05:16.044759', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 109, '46705014', 1, 12.00, 12.00, 'A', 12.00, 12.00, '2020-06-19 11:13:05.318653', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 110, '12341234', 1, 12.00, 12.00, 'A', 12.00, 12.00, '2020-06-19 11:13:14.832668', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 111, '46705014', 1, 123.00, 12.00, 'A', 12.00, 12.00, '2020-06-19 13:08:53.421083', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 1, 'sntxtbyn', 0, 671200093904.93, 9186071188.99, 'I', 4889045498515.87, 0.00, '2020-06-19 00:54:59.57504', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 0, '46705014', 0, 56.00, 89.03, 'I', 300.00, 12.00, '2020-06-19 00:54:59.57504', 'S' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 100, '46705014', 3, 12.00, 12.00, 'A', 12.00, 12.00, '2020-06-19 00:54:59.57504', 'S' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 112, '12341234', 1, 0.00, 0.00, 'A', 10.00, 0.00, '2020-06-24 00:09:16.029022', 'N' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas ) VALUES ( 103, '46705014', 1, 234.00, 234.00, 'A', 234.00, 234.00, '2020-06-19 10:59:45.677029', 'S' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 1, 'contribuyente' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 2, 'deuda' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 3, 'descuento' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 5, 'reportes' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 6, 'reportes-1' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 7, 'reportes-2' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 8, 'datos personales' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 9, 'cerrar sesión' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 10, 'notificacion' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 11, 'reclamos' ); 
INSERT INTO "public".menu( id_menu, nombre ) VALUES ( 4, 'pago' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '46705014', '{"endpoint":"https://fcm.googleapis.com/fcm/send/d8S6kJMg7KU:APA91bGOm5O1lHHuZFA-ojDg6r6E86nK4znDnJRdsrhlKrAfYzAKr25NdSU2lwa4wO-WLkKllyS49LYhXynXXH6WGeA6_K5LQLA5m4x74CI7oG4kD0sGRR7GzopAMkV7GDIGYp71yYV7","expirationTime":null,"keys":{"p256dh":"BDRy2pcm3HNshNYUnloLOkFxCH-5ltOwPYKVlvaNcRPPt9U7IhZmJgE_FHDSeltmpTA-K-opEtUKldDTnxvgA2c","auth":"ZhJqtu0L3psZRHgNGBjxOg"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '12341234', '{"endpoint":"https://fcm.googleapis.com/fcm/send/ckp5NRPQbUs:APA91bGDKpfoyU5ywWp0-ljeobcYQ5qCqPBpsnysVzvyq_1fKr-M8kBSq_KFnKacV0naKiOYsMRBgQs2GI1qiq9lZW7Ri3O2L6ZZwOyyGw5OwSUIZP7RfE6--ieqS5d3__QdTRT_upVa","expirationTime":null,"keys":{"p256dh":"BEYO5EcXOEcrm2qQNBxDOvs_KotnpZzKn_58T3VvkbBSJaXeFeGd9a1ze-rFYPD-8BlpwmqtLh4QKen5bJyUdXc","auth":"eOka5rFqE_u8hawV99zdyw"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '12341234', '{"endpoint":"https://fcm.googleapis.com/fcm/send/d8S6kJMg7KU:APA91bGOm5O1lHHuZFA-ojDg6r6E86nK4znDnJRdsrhlKrAfYzAKr25NdSU2lwa4wO-WLkKllyS49LYhXynXXH6WGeA6_K5LQLA5m4x74CI7oG4kD0sGRR7GzopAMkV7GDIGYp71yYV7","expirationTime":null,"keys":{"p256dh":"BDRy2pcm3HNshNYUnloLOkFxCH-5ltOwPYKVlvaNcRPPt9U7IhZmJgE_FHDSeltmpTA-K-opEtUKldDTnxvgA2c","auth":"ZhJqtu0L3psZRHgNGBjxOg"}}', 'A' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 5, 'S', 'S', 'S', 'Se ha modificado la deuda 0, por favor revisar...', '2020-06-26 12:56:29.576506', '46705014' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 2, 'contribuyente' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 1, 'administrador' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 3, 'todos' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 4, 'departamento de reclamo' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 5, 'area de sistema' ); 
INSERT INTO "public".tipo_transaccion( id_tipo_transaccion, nombre ) VALUES ( 0, 'cgtch' ); 
INSERT INTO "public".tipo_transaccion( id_tipo_transaccion, nombre ) VALUES ( 1, 'deposito' ); 
INSERT INTO "public".usuario( dni, password, estado ) VALUES ( '46705014', '202cb962ac59075b964b07152d234b70', 'A' ); 
INSERT INTO "public".usuario( dni, password, estado ) VALUES ( '12341234', '202cb962ac59075b964b07152d234b70', 'A' ); 
INSERT INTO "public".usuario( dni, password, estado ) VALUES ( '43214321', '202cb962ac59075b964b07152d234b70', 'A' ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 6, null, '2020-12-19', null, null, null, 'A', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 7, null, '2021-01-19', null, null, null, 'A', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 8, null, '2021-02-19', null, null, null, 'A', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 9, null, '2021-03-19', null, null, null, 'A', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 10, null, '2021-04-19', null, null, null, 'A', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 101, 1, null, '2020-07-19', null, null, null, 'A', null, null, 7.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 101, 2, null, '2020-08-19', null, null, null, 'A', null, null, 7.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 102, 1, null, '2020-07-19', null, null, null, 'A', null, null, 6.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 102, 2, null, '2020-08-19', null, null, null, 'A', null, null, 6.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 1, null, '2020-07-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 2, null, '2020-08-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 3, null, '2020-09-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 4, null, '2020-10-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 5, null, '2020-11-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 6, null, '2020-12-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 7, null, '2021-01-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 8, null, '2021-02-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 9, null, '2021-03-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 66, 10, null, '2021-04-19', null, null, null, 'A', null, null, 44836991335.54 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 100, 1, 0.00, '2020-07-23', null, null, null, 'A', 0.00, 0.00, 6.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 100, 2, 0.00, '2020-08-23', null, null, null, 'A', 0.00, 0.00, 6.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 1, 0.00, '2020-07-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 2, 0.00, '2020-08-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 3, 0.00, '2020-09-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 4, 0.00, '2020-10-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 5, 0.00, '2020-11-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 6, 0.00, '2020-12-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 7, 0.00, '2021-01-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 8, 0.00, '2021-02-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 9, 0.00, '2021-03-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 103, 10, 0.00, '2021-04-26', null, null, null, 'A', 0.00, 0.00, 23.40 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 1, null, '2020-07-19', null, '2020-06-27 01:23:49.972837', null, 'A', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 2, null, '2020-08-19', 0, '2020-06-27 01:34:26.631254', '123', 'P', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 3, null, '2020-09-19', 0, '2020-06-27 01:36:05.636201', '5345345', 'P', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 4, null, '2020-10-19', 0, '2020-06-27 01:36:47.123879', '1234123', 'P', null, null, 30.00 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 0, 5, null, '2020-11-19', 0, '2020-06-27 01:37:25.22693', '123123', 'P', null, null, 30.00 ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 1, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 2, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 4, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 5, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 6, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 7, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 2, 2, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 5, 2, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 6, 2, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 7, 2, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 8, 3, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 9, 3, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 10, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 3, 1, 'I' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 3, 2, 'I' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 11, 1, 'A' ); 
INSERT INTO "public".menu_perfiles( id_menu, id_perfiles, estado ) VALUES ( 10, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 1, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 2, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 3, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 4, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 5, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 6, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 7, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 2, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 3, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 5, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 6, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 7, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 8, 3, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 9, 3, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 2, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 3, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 5, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 6, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 7, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 8, 3, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 9, 3, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 10, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 11, 1, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '46705014', 10, 2, 'A' ); 
INSERT INTO "public".usuario_menu_perfiles( dni, id_menu, id_perfiles, estado ) VALUES ( '12341234', 10, 2, 'A' ); 
