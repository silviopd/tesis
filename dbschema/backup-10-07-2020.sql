CREATE SCHEMA IF NOT EXISTS "audit";

CREATE SEQUENCE "audit".audit_contribuyente_id_audit_contribuyente_seq START WITH 1;

CREATE SEQUENCE "audit".audit_deuda_id_audit_contribuyente_seq START WITH 1;

CREATE SEQUENCE "public".notificacion_id_notificacion_seq START WITH 1;

CREATE  TABLE "audit".audit_contribuyente ( 
	id_audit_contribuyente integer DEFAULT nextval('audit.audit_contribuyente_id_audit_contribuyente_seq'::regclass) NOT NULL ,
	dni                  char(8)  NOT NULL ,
	nombres              varchar(100)  NOT NULL ,
	apellido_paterno     varchar(100)  NOT NULL ,
	apellido_materno     varchar(100)  NOT NULL ,
	codigo               varchar(11)   ,
	celular              varchar(10)   ,
	direccion            varchar(200)   ,
	referencia           varchar(200)   ,
	correo               varchar(20)   ,
	estado               char(1)   ,
	dni_usuario          char(8)  NOT NULL ,
	tipo                 varchar(9)  NOT NULL ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	CONSTRAINT pk_audit_contribuyente_id_audit_contribuyente PRIMARY KEY ( id_audit_contribuyente )
 );

COMMENT ON COLUMN "audit".audit_contribuyente.tipo IS '1.- Agregar\n2.- Editar\n3.- Eliminar';

CREATE  TABLE "audit".audit_deuda ( 
	id_audit_contribuyente integer DEFAULT nextval('audit.audit_deuda_id_audit_contribuyente_seq'::regclass) NOT NULL ,
	id_deuda             integer  NOT NULL ,
	dni                  char(8)   ,
	id_categoria         integer   ,
	subtotal             numeric(15,2)   ,
	mora                 numeric(12,2)   ,
	estado               char(1)   ,
	total                numeric(15,2)   ,
	descuento            numeric(15,2)   ,
	dni_usuario          char(8)  NOT NULL ,
	tipo                 varchar(9)  NOT NULL ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	CONSTRAINT audit_deuda_pkey PRIMARY KEY ( id_audit_contribuyente )
 );

CREATE  TABLE "public".categoria ( 
	id_categoria         integer  NOT NULL ,
	nombre               varchar(50)  NOT NULL ,
	CONSTRAINT pk_categoria_id_categoria PRIMARY KEY ( id_categoria )
 );

CREATE  TABLE "public".configuracion ( 
	tabla                varchar(100)  NOT NULL ,
	numero               integer  NOT NULL ,
	CONSTRAINT pk_configuracion_tabla PRIMARY KEY ( tabla )
 );

CREATE  TABLE "public".contribuyente ( 
	dni                  char(8)  NOT NULL ,
	nombres              varchar(100)  NOT NULL ,
	apellido_paterno     varchar(100)  NOT NULL ,
	apellido_materno     varchar(100)  NOT NULL ,
	codigo               varchar(11)   ,
	celular              varchar(10)   ,
	direccion            varchar(200)   ,
	referencia           varchar(200)   ,
	correo               varchar(20)   ,
	estado               char(1)   ,
	CONSTRAINT pk_contribuyente_dni PRIMARY KEY ( dni )
 );

CREATE  TABLE "public".deuda ( 
	id_deuda             integer  NOT NULL ,
	dni                  char(8)   ,
	id_categoria         integer   ,
	subtotal             numeric(15,2)   ,
	mora                 numeric(12,2) DEFAULT 0  ,
	estado               char(1)   ,
	total                numeric(15,2)   ,
	descuento            numeric(15,2) DEFAULT 0  ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	cuotas               char(1) DEFAULT 'N'::bpchar NOT NULL ,
	reclamo              char(1) DEFAULT 'N'::bpchar  ,
	reclamo_descripcion  varchar   ,
	reclamo_fecha        timestamp   ,
	reclamo_ad           timestamp   ,
	reclamo_usuario_ad   char(8)   ,
	CONSTRAINT pk_deuda_id_deuda PRIMARY KEY ( id_deuda ),
	CONSTRAINT fk_deuda_categoria FOREIGN KEY ( id_categoria ) REFERENCES "public".categoria( id_categoria )  ,
	CONSTRAINT fk_deuda_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni )  
 );

CREATE  TABLE "public".menu ( 
	id_menu              integer  NOT NULL ,
	nombre               varchar(100)   ,
	icono                varchar(30)   ,
	CONSTRAINT pk_menu_id_menu PRIMARY KEY ( id_menu )
 );

COMMENT ON TABLE "public".menu IS 'row usuario: N: No, S: Si, A: Ambos';

CREATE  TABLE "public".notifcacion_usuario ( 
	dni                  char(8)  NOT NULL ,
	"data"               varchar  NOT NULL ,
	estado               char(1) DEFAULT 'A'::bpchar NOT NULL ,
	CONSTRAINT pk_notifcacion_usuario_dni PRIMARY KEY ( dni, "data" ),
	CONSTRAINT fk_notifcacion_usuario_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni )  
 );

CREATE  TABLE "public".notificacion ( 
	id_notificacion      integer DEFAULT nextval('notificacion_id_notificacion_seq'::regclass) NOT NULL ,
	app                  char(1)  NOT NULL ,
	correo               char(1)  NOT NULL ,
	web                  char(1)  NOT NULL ,
	mensaje              varchar(300)  NOT NULL ,
	fecha                timestamp DEFAULT current_timestamp NOT NULL ,
	dni                  char(8)   ,
	CONSTRAINT pk_notificacion_id_notificacion PRIMARY KEY ( id_notificacion ),
	CONSTRAINT fk_notificacion_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni )  
 );

CREATE  TABLE "public".perfiles ( 
	id_perfiles          integer  NOT NULL ,
	nombre               varchar(100)   ,
	CONSTRAINT pk_perfiles_id_perfiles PRIMARY KEY ( id_perfiles )
 );

COMMENT ON TABLE "public".perfiles IS '1= administrador\n2= contribuyente';

CREATE  TABLE "public".tipo_transaccion ( 
	id_tipo_transaccion  integer  NOT NULL ,
	nombre               varchar  NOT NULL ,
	CONSTRAINT unq_tipo_transaccion_id_tipo_transaccion UNIQUE ( id_tipo_transaccion ) 
 );

CREATE  TABLE "public".usuario ( 
	dni                  char(8)  NOT NULL ,
	"password"           char(32)  NOT NULL ,
	estado               char(1) DEFAULT 'A'::bpchar  ,
	CONSTRAINT pk_usuario_dni PRIMARY KEY ( dni ),
	CONSTRAINT fk_usuario_contribuyente FOREIGN KEY ( dni ) REFERENCES "public".contribuyente( dni )  
 );

CREATE  TABLE "public".usuario_perfil_menu ( 
	dni                  char(8)  NOT NULL ,
	id_perfiles          integer  NOT NULL ,
	id_menu              integer  NOT NULL ,
	estado               char(1) DEFAULT 'A'::bpchar NOT NULL ,
	CONSTRAINT pk_usuario_perfil_dni PRIMARY KEY ( dni, id_perfiles, id_menu ),
	CONSTRAINT fk_usuario_perfil_usuario FOREIGN KEY ( dni ) REFERENCES "public".usuario( dni )  ,
	CONSTRAINT fk_usuario_perfil_perfiles FOREIGN KEY ( id_perfiles ) REFERENCES "public".perfiles( id_perfiles )  ,
	CONSTRAINT fk_usuario_perfil_menu FOREIGN KEY ( id_menu ) REFERENCES "public".menu( id_menu )  
 );

CREATE  TABLE "public".deuda_detalle ( 
	id_deuda             integer  NOT NULL ,
	cuotas               integer  NOT NULL ,
	mora                 numeric(12,2) DEFAULT 0  ,
	fecha                date   ,
	id_tipo_transaccion  integer   ,
	fecha_transaccion    timestamp   ,
	numero_transaccion   varchar(20)   ,
	estado               char(1) DEFAULT 'A'::bpchar NOT NULL ,
	subtotal             numeric(15,2) DEFAULT 0  ,
	descuento            numeric(15,2) DEFAULT 0  ,
	total                numeric(15,2)  NOT NULL ,
	CONSTRAINT pk_deuda_detalle_id_deuda PRIMARY KEY ( id_deuda, cuotas ),
	CONSTRAINT fk_deuda_detalle_deuda FOREIGN KEY ( id_deuda ) REFERENCES "public".deuda( id_deuda )  ,
	CONSTRAINT fk_deuda_detalle_tipo_transaccion FOREIGN KEY ( id_tipo_transaccion ) REFERENCES "public".tipo_transaccion( id_tipo_transaccion )  
 );

CREATE OR REPLACE FUNCTION public.f_contribuyente_delete(p_dni character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_contribuyente record;
	
	begin
		select * into v_contribuyente from contribuyente where dni like p_dni;
		--guardar el audit
		INSERT INTO audit.audit_contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado, 
				dni_usuario, 
				tipo)
		    VALUES (
				v_contribuyente.dni,
				v_contribuyente.nombres,
				v_contribuyente.apellido_paterno,
				v_contribuyente.apellido_materno,
				v_contribuyente.codigo,
				v_contribuyente.celular,
				v_contribuyente.direccion, 
				v_contribuyente.referencia, 
				v_contribuyente.correo, 
				v_contribuyente.estado, 
				p_dni_usuario, 
				upper(p_tipo)
		    );
		--eliminar el registro (solo se cambia el estado a 'I' inactivo)
		UPDATE public.contribuyente
		   SET  estado='I'
		 WHERE dni=p_dni;

		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_contribuyente_edit(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_contribuyente record;
	
	begin
		select * into v_contribuyente from contribuyente where dni like p_dni;
		
		--editar el registro
		UPDATE public.contribuyente
		   SET 	nombres=p_nombres, 
			apellido_paterno=p_apellido_paterno, 
			apellido_materno=p_apellido_materno, 
			codigo=p_codigo, 
			celular=p_celular, 
			direccion=p_direccion, 
			referencia=p_referencia, 
			correo=p_correo, 
			estado=p_estado
		 WHERE dni = p_dni;

		--guardar el audit
		INSERT INTO audit.audit_contribuyente(
				dni, 
				nombres, 
				apellido_paterno, 
				apellido_materno, 
				codigo, 
				celular, 
				direccion, 
				referencia, 
				correo, 
				estado, 
				dni_usuario, 
				tipo)
		    VALUES (
				v_contribuyente.dni,
				v_contribuyente.nombres,
				v_contribuyente.apellido_paterno,
				v_contribuyente.apellido_materno,
				v_contribuyente.codigo,
				v_contribuyente.celular,
				v_contribuyente.direccion,
				v_contribuyente.referencia,
				v_contribuyente.correo,
				v_contribuyente.estado,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_contribuyente_new(p_dni character varying, p_nombres character varying, p_apellido_paterno character varying, p_apellido_materno character varying, p_codigo character varying, p_celular character varying, p_direccion character varying, p_referencia character varying, p_correo character varying, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_contribuyente char(8);
	
	begin
		select dni into v_contribuyente from public.contribuyente where dni like p_dni;
		if v_contribuyente is null then 
			--se agrego el registro
			INSERT INTO public.contribuyente(
					dni, 
					nombres, 
					apellido_paterno, 
					apellido_materno, 
					codigo, 
					celular, 
					direccion, 
					referencia, 
					correo, 
					estado)
			    VALUES (
					p_dni,
					p_nombres,
					p_apellido_paterno,
					p_apellido_materno,
					p_codigo,
					p_celular,
					p_direccion,
					p_referencia,
					p_correo,
					p_estado
			    );

			--guardar el audit
			INSERT INTO audit.audit_contribuyente(
					dni, 
					nombres, 
					apellido_paterno, 
					apellido_materno, 
					codigo, 
					celular, 
					direccion, 
					referencia, 
					correo, 
					estado, 
					dni_usuario, 
					tipo)
			    VALUES (
					p_dni,
					p_nombres,
					p_apellido_paterno,
					p_apellido_materno,
					p_codigo,
					p_celular,
					p_direccion,
					p_referencia,
					p_correo,
					p_estado,
					p_dni_usuario, 
					upper(p_tipo)
			    );
			 return query
				select 200;
		else
			return query
				select 500;
		end if;
	
		
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_cuotas(p_id_deuda integer, p_nro_cuotas integer, p_fecha character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_pago record;
	declare v_deuda_total numeric;
	declare v_deuda_mensual numeric;
	declare v_cuota int;
	
	begin
		
		select total into v_deuda_total from public.deuda where id_deuda = p_id_deuda;
		v_deuda_mensual = v_deuda_total / p_nro_cuotas;
		v_cuota = 1;

		 FOR v_pago IN (select (DATE (p_fecha) + (interval '1' month * generate_series(1,p_nro_cuotas)))::DATE as fecha)
		  LOOP
			INSERT INTO public.deuda_detalle(id_deuda, cuotas, fecha, total)
			VALUES (p_id_deuda,v_cuota,v_pago.fecha,v_deuda_mensual);
			v_cuota = v_cuota + 1;
		  END LOOP;

		UPDATE public.deuda
		   SET cuotas='S'
		 WHERE id_deuda=p_id_deuda;

		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_delete(p_id_deuda integer, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_deuda record;
	
	begin
		select * into v_deuda from public.deuda where id_deuda = p_id_deuda;
		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda,
				dni,
				id_categoria,
				subtotal,
				mora,
				estado,
				total,
				descuento,
				dni_usuario,
				tipo)
		    VALUES (
				v_deuda.id_deuda,
				v_deuda.dni,
				v_deuda.id_categoria,
				v_deuda.subtotal,
				v_deuda.mora,
				v_deuda.estado,
				v_deuda.total, 
				v_deuda.descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		--eliminar el registro (solo se cambia el estado a 'I' inactivo)
		UPDATE public.deuda
		   SET  estado='I'
		 WHERE id_deuda=p_id_deuda;

		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_edit(p_id_deuda integer, p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_deuda record;
	
	begin
		select * into v_deuda from public.deuda where id_deuda = p_id_deuda;
		
		--editar el registro
		UPDATE public.deuda
		   SET 	dni=p_dni, 
			id_categoria=p_id_categoria, 
			subtotal=p_subtotal, 
			mora=p_mora, 
			estado=p_estado, 
			total=p_total, 
			descuento=p_descuento
		 WHERE id_deuda=p_id_deuda;

		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento,
				dni_usuario,
				tipo)
		    VALUES (
				v_deuda.id_deuda,
				v_deuda.dni,
				v_deuda.id_categoria,
				v_deuda.subtotal,
				v_deuda.mora,
				v_deuda.estado,
				v_deuda.total,
				v_deuda.descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_deuda_new(p_dni character varying, p_id_categoria integer, p_subtotal numeric, p_mora numeric, p_total numeric, p_descuento numeric, p_estado character varying, p_dni_usuario character varying, p_tipo character varying)
 RETURNS TABLE(v_result integer)
 LANGUAGE plpgsql
AS $function$
	declare v_deuda record;
	declare v_id_deuda int;
	
	begin
		select * into v_id_deuda from public.f_generar_correlativo('deuda');
		
		--se agrego el registro
		INSERT INTO public.deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento)
		VALUES (
				v_id_deuda, 
				p_dni, 
				p_id_categoria, 
				p_subtotal, 
				p_mora, 
				p_estado, 
				p_total, 
				p_descuento
		);

		--guardar el audit
		INSERT INTO audit.audit_deuda(
				id_deuda, 
				dni, 
				id_categoria, 
				subtotal, 
				mora, 
				estado, 
				total, 
				descuento,
				dni_usuario, 
				tipo)
		    VALUES (
				v_id_deuda, 
				p_dni, 
				p_id_categoria, 
				p_subtotal, 
				p_mora, 
				p_estado, 
				p_total, 
				p_descuento,
				p_dni_usuario, 
				upper(p_tipo)
		    );
		--actualizando el correlativo
		UPDATE public.configuracion
		   SET  numero=numero+1
		 WHERE tabla='deuda';
		 return query
			select 200;
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_generar_correlativo(p_tabla character varying)
 RETURNS SETOF integer
 LANGUAGE plpgsql
AS $function$
	
	begin
		return query
		select 
			c.numero+1 
		from 
			configuracion c 
		where 
			c.tabla = p_tabla;
	end
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_login(p_user character varying, p_password character varying)
 RETURNS TABLE(r_mensaje character varying, r_dni character varying, r_nombres character varying, r_apellido_paterno character varying, r_apellido_materno character varying, r_id_perfiles integer, r_perfiles character varying, r_id_menu integer, r_menu character varying, r_icono character varying)
 LANGUAGE plpgsql
AS $function$
	declare v_usuario_estado varchar;
	
	begin
		select estado into v_usuario_estado from usuario where dni like p_user and password like MD5(p_password);
		if v_usuario_estado != 'A' || v_usuario_estado is null then
			return query 
			select 'Hubo un error en los datos, ingrese bien o contactese con soporte'::varchar,
				''::varchar,
				''::varchar,
				''::varchar,
				''::varchar,
				0,
				''::varchar,
				0,		
				''::varchar,
				''::varchar;
		else
			return query 
			SELECT 	'ok'::varchar,
				c1.dni::varchar, 
				c1.nombres, 
				c1.apellido_paterno, 
				c1.apellido_materno, 
				upm.id_perfiles, 
				p.nombre as nombre_perfiles,
				upm.id_menu, 
				m1.nombre as nombre_menu, 
				m1.icono				
			FROM "public".usuario_perfil_menu upm 
				INNER JOIN "public".menu m1 ON ( upm.id_menu = m1.id_menu  )  
				INNER JOIN "public".usuario u ON ( upm.dni = u.dni  )  
					INNER JOIN "public".contribuyente c1 ON ( u.dni = c1.dni  )  
				INNER JOIN "public".perfiles p ON ( upm.id_perfiles = p.id_perfiles  )  
			WHERE 	u.dni like p_user AND 
				u.password like MD5(p_password) AND 
				u.estado like 'A' AND
				upm.estado like 'A';
		end if;
			  
	end;
	
$function$
;

CREATE OR REPLACE FUNCTION public.f_usuario_perfiles(p_dni character varying)
 RETURNS TABLE(perfiles integer)
 LANGUAGE plpgsql
AS $function$
	declare v_usuario_estado varchar;
	
	begin
		return query
		SELECT p.id_perfiles
		FROM "public".usuario_menu_perfiles ump 
			INNER JOIN "public".usuario u ON ( ump.dni = u.dni  )  
			INNER JOIN "public".menu_perfiles mp ON ( ump.id_perfiles = mp.id_perfiles AND ump.id_menu = mp.id_menu  )  
				INNER JOIN "public".perfiles p ON ( mp.id_perfiles = p.id_perfiles  )  
		WHERE u.dni like p_dni
		GROUP BY u.dni, p.id_perfiles, p.nombre;
			  
	end;
	
$function$
;

CREATE VIEW public.v_deudas AS  SELECT c1.nombres,
    c1.apellido_paterno,
    c1.apellido_materno,
    d.id_deuda,
    d.dni,
    d.id_categoria,
    d.subtotal,
    d.mora,
    d.total,
    d.estado,
    c2.nombre AS categoria,
    d.descuento,
    d.cuotas,
    d.fecha,
    d.reclamo,
    d.reclamo_descripcion,
    d.reclamo_fecha,
    d.reclamo_ad
   FROM ((contribuyente c1
     JOIN deuda d ON ((c1.dni = d.dni)))
     JOIN categoria c2 ON ((d.id_categoria = c2.id_categoria)));;

CREATE VIEW public.v_deudas_reporte AS  SELECT dd.id_deuda,
    dd.cuotas,
    dd.mora,
    dd.fecha,
    dd.id_tipo_transaccion,
    dd.fecha_transaccion,
    dd.numero_transaccion,
    dd.estado,
    dd.subtotal,
    dd.descuento,
    dd.total,
    d.subtotal AS deuda_subtotal,
    d.mora AS deuda_mora,
    d.total AS deuda_total,
    d.descuento AS deuda_descuento,
    d.fecha AS deuda_fecha,
    d.cuotas AS deuda_cuotas,
    c1.dni,
    c1.nombres,
    c1.apellido_paterno,
    c1.apellido_materno,
    c1.celular,
    c1.direccion,
    c1.referencia,
    c1.correo,
    c2.nombre AS nombre_categoria
   FROM (((deuda_detalle dd
     JOIN deuda d ON ((dd.id_deuda = d.id_deuda)))
     JOIN contribuyente c1 ON ((d.dni = c1.dni)))
     JOIN categoria c2 ON ((d.id_categoria = c2.id_categoria)));;

INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'usuario de prueba', 'prueba', 'prueba', '9999', '978438896', 'las viñas', '', 'silviopd01@gmail.com', 'A', '46705014', 'NUEVO', 14, '2020-07-08 09:45:03.589161' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '        ', '', '', '', '', '', '', '', '', 'A', '46705014', 'NUEVO', 15, '2020-07-08 10:03:12.893298' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '        ', '', '', '', '', '', '', '', '', 'A', '46705014', 'ELIMINAR', 16, '2020-07-08 10:03:37.966163' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '978438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'A', '46705014', 'NUEVO', 17, '2020-07-08 10:28:15.620795' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '978438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'A', '46705014', 'ELIMINAR', 18, '2020-07-08 20:15:37.764786' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'NUEVO', 19, '2020-07-08 20:15:37.78227' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 20, '2020-07-08 20:15:37.78894' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '978438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 21, '2020-07-08 20:19:09.834892' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 22, '2020-07-08 20:19:09.857621' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 23, '2020-07-08 20:22:46.171305' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 24, '2020-07-08 20:22:46.188407' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 25, '2020-07-08 20:23:55.318391' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 26, '2020-07-08 20:23:55.338089' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 27, '2020-07-08 20:24:37.847032' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 28, '2020-07-08 20:24:37.872315' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 29, '2020-07-08 20:27:03.439258' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 30, '2020-07-08 20:27:03.458098' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 31, '2020-07-08 20:33:03.636714' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 32, '2020-07-08 20:33:03.658194' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 33, '2020-07-08 20:33:25.420101' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 34, '2020-07-08 20:33:25.444117' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 35, '2020-07-08 20:35:58.721376' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 36, '2020-07-08 20:35:58.747389' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 37, '2020-07-08 20:54:32.7644' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 38, '2020-07-08 20:54:32.78404' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I', '46705014', 'ELIMINAR', 39, '2020-07-08 20:55:27.475879' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 40, '2020-07-08 20:55:27.497284' ); 
INSERT INTO "audit".audit_contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado, dni_usuario, tipo, id_audit_contribuyente, fecha ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A', '46705014', 'EDITAR', 41, '2020-07-09 01:49:10.369445' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 96, 118, '46705014', 1, 20.00, 0.00, 'A', 20.00, 0.00, '46705014', 'NUEVO', '2020-07-10 12:30:12.909143' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 97, 119, '46705014', 1, 20.00, 0.00, 'A', 0.00, 0.00, '46705014', 'NUEVO', '2020-07-10 12:56:38.067644' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 94, 117, '46705014', 1, 0.00, 0.00, 'A', 10.00, 0.00, '46705014', 'NUEVO', '2020-07-06 05:16:11.406843' ); 
INSERT INTO "audit".audit_deuda( id_audit_contribuyente, id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, dni_usuario, tipo, fecha ) VALUES ( 95, 2, '46705014', 3, 7099.13, 0.00, 'A', 57961.94, 0.00, '46705014', 'EDITAR', '2020-07-06 05:20:38.898586' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 1, 'Impuesto Predial' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 2, 'Impuesto Vehicular' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 3, 'Impuesto Alcabala' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 4, 'Impuesto al espectáculo público no deportivo' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 5, 'Arbitrios Municipales - Liempieza Pública' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 6, 'Arbitrios  Municipales - Parques y jardines públic' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 7, 'Arbitrios Municipales - Serenazgo' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 8, 'Recaudación No Tributaria - Multas de tránsito' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 9, 'Recaudación No Tributaria - Multas administrativas' ); 
INSERT INTO "public".categoria( id_categoria, nombre ) VALUES ( 10, 'Merced conductiva' ); 
INSERT INTO "public".configuracion( tabla, numero ) VALUES ( 'perfiles', 0 ); 
INSERT INTO "public".configuracion( tabla, numero ) VALUES ( 'SSS', 1 ); 
INSERT INTO "public".configuracion( tabla, numero ) VALUES ( 'deuda', 119 ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '46705014', 'SILVIO REEYNALDO', 'PEÑA', 'DIAZ', '000000001', '978438896', 'aqui4', 'gfhjfghj', 'silviopd01@gmail.com', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '99999999', 'prueba nombre', 'prueba paterno', 'prueba materno', '99999999', '878438896', 'prueba dirección', 'prueba referencia', 'silviopd01@gmail.com', 'I' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '88888888', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '12341234', 'Luis Rafael', 'Diaz ', 'Tapia', '000000004', '836133242', 'ffff', 'gggg', 'andrequispea@gmail.c', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '43214321', 'Juana Elizabeth', 'Diaz', 'Calderon', '000000005', '885106112', 'asdf', 'asdf', 'srpdiaz@gmail.com', 'A' ); 
INSERT INTO "public".contribuyente( dni, nombres, apellido_paterno, apellido_materno, codigo, celular, direccion, referencia, correo, estado ) VALUES ( '11111111', 'Christian Jesus', 'Cordova', 'Diaz', '000000002', '858529597', null, null, 'silviopd02@gmail.com', 'A' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 117, '46705014', 1, 0.00, 0.00, 'A', 10.00, 0.00, '2020-07-06 05:16:11.406843', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 118, '46705014', 1, 20.00, 0.00, 'A', 20.00, 0.00, '2020-07-10 12:30:12.909143', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 5, '46705014', 5, 22733.24, 0.00, 'A', 33796.31, 0.00, '2018-07-02 05:09:07.712', 'N', 'A', 'verificar deuda', '2020-07-06 05:16:41.038925', '2020-07-06 05:17:10.009053', '46705014' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 6, '46705014', 9, 66031.30, 0.00, 'A', 67900.92, 0.00, '2018-04-26 09:28:02.816', 'N', 'D', 'verificar deuda', '2020-07-06 05:16:55.535597', '2020-07-06 05:17:19.320821', '46705014' ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 2, '46705014', 3, 0.00, 0.00, 'A', 57961.94, 0.00, '2020-06-16 03:11:50.784', 'S', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 119, '46705014', 1, 20.00, 0.00, 'A', 0.00, 0.00, '2020-07-10 12:56:38.067644', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 1, '43214321', 8, 73105.01, 0.00, 'A', 73078.13, 0.00, '2019-10-28 23:44:07.68', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 3, '11111111', 10, 6711.93, 0.00, 'A', 77800.45, 0.00, '2019-06-29 08:45:47.392', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 4, '11111111', 1, 76814.93, 0.00, 'A', 11499.59, 0.00, '2018-01-14 18:32:57.472', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 7, '11111111', 10, 80666.55, 0.00, 'A', 54221.50, 0.00, '2020-01-08 12:19:45.28', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 8, '43214321', 1, 2981.74, 0.00, 'A', 89488.03, 0.00, '2019-02-17 03:00:13.44', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 9, '12341234', 9, 81116.20, 0.00, 'A', 1520.72, 0.00, '2019-05-26 00:25:50.336', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 10, '43214321', 10, 94540.68, 0.00, 'A', 93290.58, 0.00, '2018-07-16 05:44:31.488', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 11, '46705014', 10, 15273.21, 0.00, 'A', 51938.95, 0.00, '2019-10-16 08:45:11.552', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 12, '46705014', 5, 91026.72, 0.00, 'A', 56613.33, 0.00, '2019-02-17 15:21:52.384', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 13, '12341234', 3, 99913.63, 0.00, 'A', 44150.80, 0.00, '2018-01-13 03:25:10.656', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 14, '46705014', 6, 53998.72, 0.00, 'A', 98971.92, 0.00, '2020-06-17 15:52:04.864', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 15, '12341234', 1, 97750.27, 0.00, 'A', 68518.76, 0.00, '2019-07-25 00:42:29.76', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 16, '43214321', 9, 15843.62, 0.00, 'A', 57657.91, 0.00, '2019-05-20 04:36:08.32', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 17, '12341234', 8, 54908.32, 0.00, 'A', 21529.26, 0.00, '2019-01-22 00:47:20.576', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 18, '12341234', 3, 9354.49, 0.00, 'A', 68834.05, 0.00, '2018-04-25 22:33:52.896', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 19, '12341234', 10, 44338.59, 0.00, 'A', 34560.44, 0.00, '2020-04-08 00:23:49.504', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 20, '11111111', 6, 41285.78, 0.00, 'A', 50320.81, 0.00, '2019-08-30 08:29:10.016', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 21, '11111111', 1, 28649.47, 0.00, 'A', 39916.92, 0.00, '2018-08-19 11:27:57.44', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 22, '12341234', 2, 16923.62, 0.00, 'A', 12322.15, 0.00, '2018-04-21 11:45:22.944', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 23, '11111111', 7, 48745.27, 0.00, 'A', 26380.63, 0.00, '2020-03-11 14:41:35.744', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 24, '46705014', 8, 94315.95, 0.00, 'A', 31311.51, 0.00, '2019-09-30 09:13:02.72', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 25, '11111111', 9, 89060.08, 0.00, 'A', 67066.52, 0.00, '2018-10-25 12:50:34.624', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 26, '11111111', 4, 53575.26, 0.00, 'A', 44515.06, 0.00, '2018-01-15 09:46:03.904', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 27, '43214321', 1, 57166.48, 0.00, 'A', 43675.28, 0.00, '2020-03-21 22:51:14.304', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 28, '12341234', 3, 25411.27, 0.00, 'A', 18977.27, 0.00, '2020-04-06 06:27:55.52', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 29, '46705014', 5, 69077.25, 0.00, 'A', 2026.05, 0.00, '2018-04-02 17:01:20.256', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".deuda( id_deuda, dni, id_categoria, subtotal, mora, estado, total, descuento, fecha, cuotas, reclamo, reclamo_descripcion, reclamo_fecha, reclamo_ad, reclamo_usuario_ad ) VALUES ( 30, '43214321', 10, 96120.16, 0.00, 'A', 32676.14, 0.00, '2018-08-16 13:07:38.624', 'N', 'N', null, null, null, null ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 3, 'descuento', null ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 7, 'reportes-3', 'fas fa-chart-pie' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 9, 'cerrar sesión', 'fas fa-lock' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 10, 'notificacion', 'fas fa-bell' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 4, 'pago', 'fas fa-money-bill-wave' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 2, 'deuda', 'fas fa-university' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 1, 'contribuyente', 'fas fa-users' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 8, 'datos personales', 'fas fa-user-cog' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 11, 'reclamos', 'fas fa-exclamation-triangle' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 12, 'usuarios', 'fas fa-user' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 6, 'reportes 2', 'fas fa-chart-pie' ); 
INSERT INTO "public".menu( id_menu, nombre, icono ) VALUES ( 5, 'reportes-deudas', 'fas fa-chart-pie' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '46705014', '{"endpoint":"https://fcm.googleapis.com/fcm/send/crjZ8p_Bn0w:APA91bHEGRBbBwCCY8jNNvzshD8Rei6sDvFXNcArungSLP6Q6yd_XAUutlxcuPH_Aaqiyrp5LKfS-wsBN0QTUtw4_qJ5KkG4-pTDav_JVvUSClC7zFzmEIEurf0Cs-sJ16jLbnUXghMz","expirationTime":null,"keys":{"p256dh":"BIRNdiAX6E85j5q6toBFnSIIDmTyVqK5uUxcp5P2N9KldnawBVefFFzchwcF0FFj79kQ9y1WzoXFdLo_1v87vFk","auth":"H5pJi0EynsHZZqJCjpMpWA"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '46705014', '{"endpoint":"https://fcm.googleapis.com/fcm/send/d8S6kJMg7KU:APA91bGOm5O1lHHuZFA-ojDg6r6E86nK4znDnJRdsrhlKrAfYzAKr25NdSU2lwa4wO-WLkKllyS49LYhXynXXH6WGeA6_K5LQLA5m4x74CI7oG4kD0sGRR7GzopAMkV7GDIGYp71yYV7","expirationTime":null,"keys":{"p256dh":"BDRy2pcm3HNshNYUnloLOkFxCH-5ltOwPYKVlvaNcRPPt9U7IhZmJgE_FHDSeltmpTA-K-opEtUKldDTnxvgA2c","auth":"ZhJqtu0L3psZRHgNGBjxOg"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '12341234', '{"endpoint":"https://fcm.googleapis.com/fcm/send/ckp5NRPQbUs:APA91bGDKpfoyU5ywWp0-ljeobcYQ5qCqPBpsnysVzvyq_1fKr-M8kBSq_KFnKacV0naKiOYsMRBgQs2GI1qiq9lZW7Ri3O2L6ZZwOyyGw5OwSUIZP7RfE6--ieqS5d3__QdTRT_upVa","expirationTime":null,"keys":{"p256dh":"BEYO5EcXOEcrm2qQNBxDOvs_KotnpZzKn_58T3VvkbBSJaXeFeGd9a1ze-rFYPD-8BlpwmqtLh4QKen5bJyUdXc","auth":"eOka5rFqE_u8hawV99zdyw"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '46705014', '{"endpoint":"https://fcm.googleapis.com/fcm/send/ckp5NRPQbUs:APA91bGDKpfoyU5ywWp0-ljeobcYQ5qCqPBpsnysVzvyq_1fKr-M8kBSq_KFnKacV0naKiOYsMRBgQs2GI1qiq9lZW7Ri3O2L6ZZwOyyGw5OwSUIZP7RfE6--ieqS5d3__QdTRT_upVa","expirationTime":null,"keys":{"p256dh":"BEYO5EcXOEcrm2qQNBxDOvs_KotnpZzKn_58T3VvkbBSJaXeFeGd9a1ze-rFYPD-8BlpwmqtLh4QKen5bJyUdXc","auth":"eOka5rFqE_u8hawV99zdyw"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '43214321', '{"endpoint":"https://fcm.googleapis.com/fcm/send/d8S6kJMg7KU:APA91bGOm5O1lHHuZFA-ojDg6r6E86nK4znDnJRdsrhlKrAfYzAKr25NdSU2lwa4wO-WLkKllyS49LYhXynXXH6WGeA6_K5LQLA5m4x74CI7oG4kD0sGRR7GzopAMkV7GDIGYp71yYV7","expirationTime":null,"keys":{"p256dh":"BDRy2pcm3HNshNYUnloLOkFxCH-5ltOwPYKVlvaNcRPPt9U7IhZmJgE_FHDSeltmpTA-K-opEtUKldDTnxvgA2c","auth":"ZhJqtu0L3psZRHgNGBjxOg"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '46705014', '{"endpoint":"https://fcm.googleapis.com/fcm/send/dlwoSEbMwMw:APA91bHN-2tqo2y1x38IikozspD_dN-_iSICY1zrzA5jYspUEezQc8sAbdkedlSMdrGrTMlT1RFI3m32rzug7QnOyn7uEYcQ_E1NXDco9vGhhvWN1pJ3jvOrwb9sNQ_DyeijLfotp6Lo","expirationTime":null,"keys":{"p256dh":"BC3djdQ4uhLgilqWvG0UnXuLS3oG1kK3DgBZ6dpaRPy_V1MBJe_pGIaVTUjuytm9ueK6MXK29Y_V1tHyingpzuY","auth":"oFyBVeENzZfaNMP_BRcbUA"}}', 'A' ); 
INSERT INTO "public".notifcacion_usuario( dni, data, estado ) VALUES ( '12341234', '{"endpoint":"https://fcm.googleapis.com/fcm/send/d8S6kJMg7KU:APA91bGOm5O1lHHuZFA-ojDg6r6E86nK4znDnJRdsrhlKrAfYzAKr25NdSU2lwa4wO-WLkKllyS49LYhXynXXH6WGeA6_K5LQLA5m4x74CI7oG4kD0sGRR7GzopAMkV7GDIGYp71yYV7","expirationTime":null,"keys":{"p256dh":"BDRy2pcm3HNshNYUnloLOkFxCH-5ltOwPYKVlvaNcRPPt9U7IhZmJgE_FHDSeltmpTA-K-opEtUKldDTnxvgA2c","auth":"ZhJqtu0L3psZRHgNGBjxOg"}}', 'A' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 58, 'S', 'S', 'S', 'Se ha pagado la deuda 2 y cuota 3...', '2020-07-06 05:14:28.00257', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 59, 'S', 'S', 'S', 'Tiene una deuda, por favor revisar...', '2020-07-06 05:16:11.438908', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 60, 'S', 'S', 'S', 'Tiene un reclamo pendiente, se le notificara la secuencia de su reclamo...', '2020-07-06 05:16:41.070656', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 61, 'S', 'S', 'S', 'Tiene un reclamo pendiente, se le notificara la secuencia de su reclamo...', '2020-07-06 05:16:55.56806', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 62, 'S', 'S', 'S', 'Su reclamo ha sido aprobado', '2020-07-06 05:17:10.011081', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 63, 'S', 'S', 'S', 'Su reclamo ha sido desaprobado', '2020-07-06 05:17:19.350825', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 64, 'S', 'S', 'S', 'Se ha modificado la deuda 2, por favor revisar...', '2020-07-06 05:20:38.900873', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 65, 'S', 'S', 'S', 'Tiene una deuda, por favor revisar...', '2020-07-10 12:30:12.913554', '46705014' ); 
INSERT INTO "public".notificacion( id_notificacion, app, correo, web, mensaje, fecha, dni ) VALUES ( 66, 'S', 'S', 'S', 'Tiene una deuda, por favor revisar...', '2020-07-10 12:56:38.13644', '46705014' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 2, 'contribuyente' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 1, 'administrador' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 4, 'departamento de reclamo' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 5, 'area de sistema' ); 
INSERT INTO "public".perfiles( id_perfiles, nombre ) VALUES ( 3, 'todos' ); 
INSERT INTO "public".tipo_transaccion( id_tipo_transaccion, nombre ) VALUES ( 0, 'cgtch' ); 
INSERT INTO "public".tipo_transaccion( id_tipo_transaccion, nombre ) VALUES ( 1, 'deposito' ); 
INSERT INTO "public".usuario( dni, password, estado ) VALUES ( '46705014', '202cb962ac59075b964b07152d234b70', 'A' ); 
INSERT INTO "public".usuario( dni, password, estado ) VALUES ( '12341234', '202cb962ac59075b964b07152d234b70', 'A' ); 
INSERT INTO "public".usuario( dni, password, estado ) VALUES ( '43214321', '202cb962ac59075b964b07152d234b70', 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 1, 1, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 1, 2, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 1, 10, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '12341234', 2, 10, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 2, 2, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 2, 10, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '12341234', 3, 9, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '12341234', 3, 8, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 3, 8, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 3, 9, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '12341234', 2, 3, 'I' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 1, 3, 'I' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 2, 3, 'I' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 1, 4, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 5, 11, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 1, 12, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '12341234', 2, 2, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 4, 11, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '43214321', 1, 1, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '43214321', 3, 8, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '43214321', 3, 9, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 2, 4, 'A' ); 
INSERT INTO "public".usuario_perfil_menu( dni, id_perfiles, id_menu, estado ) VALUES ( '46705014', 2, 5, 'A' ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 4, 0.00, '2020-11-06', null, null, null, 'A', 0.00, 0.00, 7245.24 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 5, 0.00, '2020-12-06', null, null, null, 'A', 0.00, 0.00, 7245.24 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 6, 0.00, '2021-01-06', null, null, null, 'A', 0.00, 0.00, 7245.24 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 7, 0.00, '2021-02-06', null, null, null, 'A', 0.00, 0.00, 7245.24 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 8, 0.00, '2021-03-06', null, null, null, 'A', 0.00, 0.00, 7245.24 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 1, 0.00, '2020-08-06', 0, '2020-07-06 05:10:55.570506', '3242423452345', 'P', 0.00, 0.00, 7245.24 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 2, 0.00, '2020-09-06', 0, '2020-07-06 05:12:52.403242', '234534534', 'P', 0.00, 0.00, 7245.24 ); 
INSERT INTO "public".deuda_detalle( id_deuda, cuotas, mora, fecha, id_tipo_transaccion, fecha_transaccion, numero_transaccion, estado, subtotal, descuento, total ) VALUES ( 2, 3, 0.00, '2020-10-06', 0, '2020-07-06 05:14:27.953428', '453453465', 'P', 0.00, 0.00, 7245.24 ); 
