const webpush = require("web-push");

const PUBLIC_KEY =
  process.env.PUBLIC_VAPID_KEY ||
  "BG1S5VBNxQtxe4_FpyVNUja-8Pw1OREcSxXqTIeRH9j8wVtSSDlcp9NcbDAhRZTBn_u5nTpRkyoVKPuERn5qrGY";
const PRIVATE_KEY =
  process.env.PRIVATE_VAPID_KEY ||
  "NTBu71p6b9BdAKOh6dzbQXX63RQHEzQYKAsaKbE7Cdo";

webpush.setVapidDetails("mailto:test@cgt.com", PUBLIC_KEY, PRIVATE_KEY);

module.exports = webpush;
