import express from "express";
let router = express.Router();
import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";

import {
  enviarNotificacionEspecifico,
  buscarCorreo,
  buscarCorreoDC,
  buscarDNI,
  sendMail,
  buscarCelular,
  sendSMS,
  sqlInsert,
} from "./util.js";

router.post("/deuda", (req, res) => {
  (async () => {
    try {
      poolWrite.query(
        `UPDATE public.deuda SET reclamo='S', reclamo_descripcion=$2, reclamo_fecha=now() WHERE id_deuda=$1;`,
        [req.body.id_deuda, req.body.descripcion],
        (error, results) => {
          if (error) {
            return res.status(500).send(error);  
          }
          res.status(200).json("ok");
        }
      );

      let mensaje = `Tiene un reclamo pendiente, se le notificara la secuencia de su reclamo...`;
      let titulo = `Notificación de reclamo`;

      let correo = await buscarCorreo(req.body.dni);

      sendMail(correo, titulo, mensaje);
      enviarNotificacionEspecifico(req.body.dni, titulo, mensaje);

      let numero = await buscarCelular(req.body.dni);
      sendSMS(numero, mensaje);

      await sqlInsert(
        `INSERT INTO public.notificacion(app, correo, web, mensaje,dni) VALUES ('S','S','S','${mensaje}','${req.body.dni}')`,
        []
      );
    } catch (error) {
      console.log(error);
    }
  })();
});

router.get("/reclamo", (req, res) => {
  if (req.query.opcion === "R") {
    poolRead.query(
      `SELECT * FROM public.v_deudas where reclamo like 'S'`,
      [],
      (error, results) => {
        if (error) {
          return res.status(500).send(error);  
        }
        res.status(200).json(results.rows);
      }
    );
  } else if (req.query.opcion === "S") {
    poolRead.query(
      `SELECT * FROM public.v_deudas where reclamo like 'A'`,
      [],
      (error, results) => {
        if (error) {
          return res.status(500).send(error);  
        }
        res.status(200).json(results.rows);
      }
    );
  }
});

router.post("/reclamo-ad", (req, res) => {
  (async () => {
    try {
      let mensaje = ``;
      let titulo = ``;

      if (req.body.opcion === "S") {
        poolWrite.query(
          `UPDATE public.deuda SET reclamo='A', reclamo_ad=now(), reclamo_usuario_ad=$2 WHERE id_deuda=$1;`,
          [req.body.id_deuda, req.body.dni_usuario],
          (error, results) => {
            if (error) {
              return res.status(500).send(error);  
            }
            res.status(200).json("ok");
          }
        );

        mensaje = `Su reclamo ha sido aprobado`;
        titulo = `Notificación de reclamo`;
      } else if (req.body.opcion === "N") {
        poolWrite.query(
          `UPDATE public.deuda SET reclamo='D',reclamo_ad=now(), reclamo_usuario_ad=$2 WHERE id_deuda=$1;`,
          [req.body.id_deuda, req.body.dni_usuario],
          (error, results) => {
            if (error) {
              return res.status(500).send(error);  
            }
            res.status(200).json("ok");
          }
        );

        mensaje = `Su reclamo ha sido desaprobado`;
        titulo = `Notificación de reclamo`;
      } else if (req.body.opcion === "Z") {
        poolWrite.query(
          `UPDATE public.deuda SET reclamo='Z' WHERE id_deuda=$1;`,
          [req.body.id_deuda],
          (error, results) => {
            if (error) {
              return res.status(500).send(error);  
            }
            res.status(200).json("ok");
          }
        );

        mensaje = `Su reclamo ha sido aprobada y ha sido actualizada su deuda`;
        titulo = `Notificación de reclamo`;
      }

      let correo = await buscarCorreo(req.body.dni);

      sendMail(correo, titulo, mensaje);
      enviarNotificacionEspecifico(req.body.dni, titulo, mensaje);

      let numero = await buscarCelular(req.body.dni);
      sendSMS(numero, mensaje);

      await sqlInsert(
        `INSERT INTO public.notificacion(app, correo, web, mensaje,dni) VALUES ('S','S','S','${mensaje}','${req.body.dni}')`,
        []
      );
    } catch (error) {
      console.log(error);
    }
  })();
});

module.exports = router;
