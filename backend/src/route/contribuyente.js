import express from "express";
let router = express.Router();
import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";

router.get("/", (req, res) => {
  poolRead.query("SELECT * FROM public.contribuyente where estado like 'A'", [], (error, results) => {
    if (error) {
      return res.status(500).send(error);  
    }
    res.status(200).json(results.rows);
  });
});

router.post("/eliminar", (req, res) => {
  poolWrite.query(
    "select * from f_contribuyente_delete($1,$2,'eliminar')",
    [req.body.dni, req.body.dni_usuario],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.get("/leerdatos", (req, res) => {
  console.log(req.query);

  poolRead.query(
    "SELECT * FROM public.contribuyente where dni like $1",
    [req.query.dni],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.post("/nuevo", (req, res) => {
  console.log(req.body);

  poolWrite.query(
    "select * from f_contribuyente_new($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,'nuevo')",
    [
      req.body.dni,
      req.body.nombre,
      req.body.paterno,
      req.body.materno,
      req.body.codigo,
      req.body.celular,
      req.body.direccion,
      req.body.referencia,
      req.body.correo,
      req.body.estado,
      req.body.dni_usuario,
    ],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.post("/editar", (req, res) => {
  poolWrite.query(
    "select * from f_contribuyente_edit($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,'editar')",
    [
      req.body.dni,
      req.body.nombre,
      req.body.paterno,
      req.body.materno,
      req.body.codigo,
      req.body.celular,
      req.body.direccion,
      req.body.referencia,
      req.body.correo,
      req.body.estado,
      req.body.dni_usuario,
    ],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.post("/editar-usuario", (req, res) => {
  poolWrite.query(
    `UPDATE public.contribuyente SET nombres=$2, apellido_paterno=$3, apellido_materno=$4, celular=$5, direccion=$6, referencia=$7, correo=$8 WHERE dni=$1;`,
    [
      req.body.dni,
      req.body.nombre,
      req.body.paterno,
      req.body.materno,
      req.body.celular,
      req.body.direccion,
      req.body.referencia,
      req.body.correo,
    ],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

module.exports = router;
