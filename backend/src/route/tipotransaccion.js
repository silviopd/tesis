import express from "express";
let router = express.Router();
import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";

router.get("/", (req, res) => {
  poolRead.query("SELECT * FROM tipo_transaccion", [], (error, results) => {
    if (error) {
      return res.status(500).send(error);  
    }
    res.status(200).json(results.rows);
  });
});

module.exports = router;
