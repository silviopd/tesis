import express from "express";
let router = express.Router();
import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";

router.get("/client", (req, res) => {
  poolRead.query(
    `SELECT * FROM public.v_deudas_reporte where dni like $1 and estado like 'A'`,
    [req.query.dni],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

module.exports = router;
