import express from "express";
let router = express.Router();
import poolRead from "../db/connectionRead";

router.post("/", (req, res) => {
  console.log(req.body.user, req.body.password);

  poolRead.query(
    "SELECT * FROM public.f_login($1,$2)",
    [req.body.user, req.body.password],
    (error, results) => {
      if (error) {
       return res.status(500).send(error);  
      }

      if (results.rowCount == 0) {
        return res.status(500).send("No se encontró menú");  
      }

      let mensaje = results.rows[0].r_mensaje;
      let dni = results.rows[0].r_dni;
      let nombres = results.rows[0].r_nombres;
      let apellido_paterno = results.rows[0].r_apellido_paterno;
      let apellido_materno = results.rows[0].r_apellido_materno;
      let menu = "";

      menu += `<li class="nav-item">
                    <a href="../home/index.php" class="nav-link active" id="menu-home">
                        <i class="nav-icon fa fa-home"></i>
                        <p>
                            Home
                        </p>
                    </a>
                </li>`;

      let admin = ``;
      let contri = ``;
      let todos = ``;
      let reclamo = ``;
      let sistema = ``;

      results.rows.map((dato) => {
        if (dato.r_id_perfiles === 1) {
          admin += `<li class="nav-item">
                    <a href="../${dato.r_menu}/admin.index.php" class="nav-link" id="menu${dato.r_id_menu}${dato.r_id_perfiles}">
                        <i class="nav-icon ${dato.r_icono}"></i>
                        <p>
                            ${dato.r_menu}
                        </p>
                    </a>
                </li>`;
        } else if (dato.r_id_perfiles === 2) {
          contri += `<li class="nav-item">`;

          dato.r_menu.split("-").length == 1
            ? (contri += `<a href="../${dato.r_menu}/index.php" class="nav-link" id="menu${dato.r_id_menu}${dato.r_id_perfiles}">
                        <i class="nav-icon ${dato.r_icono}"></i>
                        <p>
                            ${dato.r_menu}
                        </p>
                    </a>`)
            : (contri += `<a href="../${dato.r_menu.split("-")[0]}/${
                dato.r_menu.split("-")[1]
              }.php" class="nav-link" id="menu${dato.r_id_menu}${
                dato.r_id_perfiles
              }">
                        <i class="nav-icon ${dato.r_icono}"></i>
                        <p>
                            ${dato.r_menu.replace("-", " ")}
                        </p>
                    </a>`);
          contri += `</li>`;
        } else if (dato.r_id_perfiles === 3) {
          todos += `<li class="nav-item">
                    <a href="../${dato.r_menu}/index.php" class="nav-link" id="menu${dato.r_id_menu}${dato.r_id_perfiles}">
                        <i class="nav-icon ${dato.r_icono}"></i>
                        <p>
                            ${dato.r_menu}
                        </p>
                    </a>
                </li>`;
        } else if (dato.r_id_perfiles === 4) {
          reclamo += `<li class="nav-item">
                    <a href="../${dato.r_menu}/reclamo.index.php" class="nav-link" id="menu${dato.r_id_menu}${dato.r_id_perfiles}">
                        <i class="nav-icon ${dato.r_icono}"></i>
                        <p>
                            ${dato.r_menu}
                        </p>
                    </a>
                </li>`;
        } else if (dato.r_id_perfiles === 5) {
          sistema += `<li class="nav-item">
                    <a href="../${dato.r_menu}/sistema.index.php" class="nav-link" id="menu${dato.r_id_menu}${dato.r_id_perfiles}">
                        <i class="nav-icon ${dato.r_icono}"></i>
                        <p>
                            ${dato.r_menu}
                        </p>
                    </a>
                </li>`;
        }
      });

      if (admin.length > 0) {
        menu += ` <li class="nav-header">Administrador</li>`;
        menu += admin;
      }

      if (reclamo.length > 0) {
        menu += ` <li class="nav-header">Departamento de Reclamo</li>`;
        menu += reclamo;
      }

      if (sistema.length > 0) {
        menu += ` <li class="nav-header">Departamento de Sistema</li>`;
        menu += sistema;
      }

      if (contri.length > 0) {
        menu += ` <li class="nav-header">Usuario</li>`;
        menu += contri;
      }

      if (todos.length > 0) {
        menu += ` <li class="nav-header">Configuración</li>`;
        menu += todos;
      }

      let result = {
        mensaje,
        dni,
        nombres,
        apellido_paterno,
        apellido_materno,
        menu: JSON.stringify(menu),
      };

      // console.log(results.rows);

      res.status(200).json(result);
    }
  );
});

module.exports = router;
