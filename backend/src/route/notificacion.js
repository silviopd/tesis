import express from "express";

let router = express.Router();
import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";

import {
  enviarNotificacionTodos,
  sendMailAll,
  sendSMSAll,
  enviarNotificacionEspecifico,
  buscarCelular,
  sendSMS,
  buscarCorreo,
  sendMail,
} from "./util";

router.get("/", (req, res) => {
  poolRead.query(
    "SELECT dni FROM notifcacion_usuario group by dni",
    [],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.get("/usuario", (req, res) => {
  poolRead.query(
    "SELECT * FROM public.notificacion where dni like $1",
    [req.query.dni],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.post("/subscripcion", async (req, res) => {
  poolWrite.query(
    "INSERT INTO public.notifcacion_usuario(dni, data) VALUES ($1, $2);",
    [req.body.dni, req.body.subscription],
    (error, results) => {
      res.status(200).json("ok");
    }
  );
});

router.get("/todos", (req, res) => {
  (async () => {
    try {
      let mensaje = `Notificación de deudas, por favor revisar...`;
      let titulo = `Nodificación de Deudas`;

      enviarNotificacionTodos(titulo, mensaje);
      sendMailAll(titulo, mensaje);
      sendSMSAll(mensaje);

      sqlInsert(
        `INSERT INTO public.notificacion(app, correo, web, mensaje,dni) VALUES ('S','S','S','${mensaje}','${req.body.dni}')`,
        []
      );

      res.status(200).json("ok");
    } catch (error) {
      console.log(error);
    }
  })();
});

router.get("/especifico", async (req, res) => {
  (async () => {
    try {
      let dni = req.query.dni;
      let mensaje = `Notificación de deudas, por favor revisar...`;
      let titulo = `Nodificación de Deudas`;

      enviarNotificacionEspecifico(dni, titulo, mensaje);

      let correo = await buscarCorreo(dni);
      sendMail(correo, titulo, mensaje);

      let numero = await buscarCelular(dni);
      sendSMS(numero, mensaje);

      sqlInsert(
        `INSERT INTO public.notificacion(app, correo, web, mensaje,dni) VALUES ('S','S','S','${mensaje}','${req.body.dni}')`,
        []
      );

      res.status(200).json("ok");
    } catch (error) {
      console.log(error);
    }
  })();
});

module.exports = router;
