import express from "express";
let router = express.Router();
import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";

import {
  enviarNotificacionEspecifico,
  buscarCorreo,
  buscarCorreoDC,
  buscarDNI,
  sendMail,
  buscarCelular,
  sendSMS,
  sqlInsert,
} from "./util.js";

router.get("/", (req, res) => {
  console.log(req.query.dni);
  if (req.query.dni !== undefined) {
    poolRead.query(
      "SELECT * FROM public.v_deudas where dni like $1 and estado like 'A' and reclamo like 'N' or reclamo like 'S' and dni like $1 or reclamo like 'A' and dni like $1 or reclamo like 'Z' and dni like $1 or reclamo like 'D' and dni like $1",
      [req.query.dni],
      (error, results) => {
        if (error) {
          return res.status(500).send(error);
        }
        res.status(200).json(results.rows);
      }
    );
  } else {
    poolRead.query("SELECT * FROM public.v_deudas", [], (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    });
  }
});

router.post("/eliminar", (req, res) => {
  poolWrite.query(
    "select * from f_deuda_delete($1,$2,'eliminar')",
    [req.body.id_deuda, req.body.dni_usuario],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.get("/leerdatos", (req, res) => {
  poolRead.query(
    "SELECT * FROM public.deuda where id_deuda = $1",
    [req.query.id_deuda],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.post("/nuevo", (req, res) => {
  (async () => {
    try {
      poolWrite.query(
        "select * from f_deuda_new($1,$2,$3,$4,$5,$6,$7,$8,'nuevo')",
        [
          req.body.dni,
          req.body.id_categoria,
          req.body.subtotal,
          req.body.mora,
          req.body.total,
          req.body.descuento,
          req.body.estado,
          req.body.dni_usuario,
        ],
        (error, results) => {
          if (error) {
            return res.status(500).send(error);  
          }
          res.status(200).json(results.rows);
        }
      );

      let mensaje = `Tiene una deuda, por favor revisar...`;
      let titulo = `Notificación nueva deuda`;

      let correo = await buscarCorreo(req.body.dni);

      sendMail(correo, titulo, mensaje);
      enviarNotificacionEspecifico(req.body.dni, titulo, mensaje);

      let numero = await buscarCelular(req.body.dni);
      sendSMS(numero, mensaje);

      await sqlInsert(
        `INSERT INTO public.notificacion(app, correo, web, mensaje,dni) VALUES ('S','S','S','${mensaje}','${req.body.dni}')`,
        []
      );
    } catch (error) {
      console.log(error);
    }
  })();
});

router.post("/editar", (req, res) => {
  (async () => {
    try {
      poolWrite.query(
        "select * from f_deuda_edit($1,$2,$3,$4,$5,$6,$7,$8,$9,'editar')",
        [
          req.body.id_deuda,
          req.body.dni,
          req.body.id_categoria,
          req.body.subtotal,
          req.body.mora,
          req.body.total,
          req.body.descuento,
          req.body.estado,
          req.body.dni_usuario,
        ],
        (error, results) => {
          if (error) {
            return res.status(500).send(error);  
          }
          res.status(200).json(results.rows);
        }
      );

      let mensaje = `Se ha modificado la deuda ${req.body.id_deuda}, por favor revisar...`;
      let titulo = `[${req.body.id_deuda}] - Notificación`;

      let correo = await buscarCorreo(req.body.dni);

      sendMail(correo, titulo, mensaje);
      enviarNotificacionEspecifico(req.body.dni, titulo, mensaje);

      let numero = await buscarCelular(req.body.dni);
      sendSMS(numero, mensaje);

      await sqlInsert(
        `INSERT INTO public.notificacion(app, correo, web, mensaje,dni) VALUES ('S','S','S','${mensaje}','${req.body.dni}')`,
        []
      );
    } catch (error) {
      console.log(error);
    }
  })();
});

router.post("/cuotas", (req, res) => {
  poolWrite.query(
    "select * from f_deuda_cuotas($1,$2,$3)",
    [req.body.id_deuda, req.body.nro_cuotas, req.body.fecha],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.get("/detalle", (req, res) => {
  poolRead.query(
    `select dd.*,d.dni from public.deuda_detalle dd inner join deuda d on dd.id_deuda=d.id_deuda where dd.id_deuda = $1 and dd.estado like 'A'`,
    [req.query.id_deuda],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.post("/detalle", (req, res) => {
  (async () => {
    try {
      poolWrite.query(
        `select * from f_pagar_deuda($1,$2,$3,$4)`,
        [
          req.body.id_deuda,
          req.body.cuotas,
          req.body.id_tipo_transaccion,
          req.body.numero_transaccion,
        ],
        (error, results) => {
          if (error) {
            return res.status(500).send(error);  
          }
          res.status(200).json(results.rows);
        }
      );

      let mensaje = `Se ha pagado la cuota ${req.body.cuotas}  de la deuda ${req.body.id_deuda} ...`;
      let titulo = `[${req.body.id_deuda}] - Notificación`;

      let correo = await buscarCorreoDC(req.body.id_deuda, req.body.cuotas);
      let dni = await buscarDNI(req.body.id_deuda, req.body.cuotas);

      sendMail(correo, titulo, mensaje);
      enviarNotificacionEspecifico(dni, titulo, mensaje);

      let numero = await buscarCelular(dni);
      sendSMS(numero, mensaje);

      await sqlInsert(
        `INSERT INTO public.notificacion(app, correo, web, mensaje,dni) VALUES ('S','S','S','${mensaje}','${dni}')`,
        []
      );
    } catch (error) {
      console.log(error);
    }
  })();
});

router.get("/detalle-dni-pagado", (req, res) => {
  poolRead.query(
    `SELECT dd.id_deuda, dd.cuotas, dd.mora, dd.fecha, dd.id_tipo_transaccion, dd.fecha_transaccion, dd.numero_transaccion, dd.estado, dd.subtotal, dd.descuento, dd.total, d.dni FROM "public".deuda_detalle dd INNER JOIN "public".deuda d ON ( dd.id_deuda = d.id_deuda  ) WHERE d.dni like $1 and dd.estado like 'P'`,
    [req.query.dni],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

module.exports = router;
