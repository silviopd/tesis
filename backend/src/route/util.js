import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";
import webpush from "../webpush";
const nodemailer = require("nodemailer");
var AWS = require("aws-sdk");

//mail
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "appcgtch@gmail.com",
    pass: "jdiysbbmmnxgpmku",
  },
});

//SMS
AWS.config = new AWS.Config();
AWS.config.accessKeyId = process.env.AWS_ACCESS_KEY_ID;
AWS.config.secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
AWS.config.region = "us-east-1";

export const sendMail = (email, title, message) => {
  const mailOptions = {
    from: "appcgtch@gmail.com",
    to: `${email}`,
    subject: `${title}`,
    text: `${message}`,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};

export const sendMailAll = (titulo, mensaje) => {
  poolRead.query("SELECT * FROM public.contribuyente", [], (error, results) => {
    // console.log(results.rows);
    results.rows.map(async (usuario) => {
      try {
        let correo = await buscarCorreo(usuario.dni);
        sendMail(correo, titulo, mensaje);
      } catch (error) {
        console.log(error);
      }
    });
  });
};

export const sendSMS = async (numero, mensaje) => {
  let params = {
    Message: `${mensaje}` /* required */,
    PhoneNumber: `+51${numero}`,
  };

  let publishTextPromise = await new AWS.SNS({ apiVersion: "2010-03-31" })
    .publish(params)
    .promise();

  publishTextPromise
    .then(function (data) {
      console.log(JSON.stringify({ MessageID: data.MessageId }));
    })
    .catch(function (err) {
      console.log(JSON.stringify({ Error: err }));
    });
};

export const sendSMSAll = (mensaje) => {
  poolRead.query("SELECT * FROM public.contribuyente", [], (error, results) => {
    results.rows.map(async (usuario) => {
      try {
        let numero = await buscarCelular(usuario.dni);
        sendSMS(numero, mensaje);
      } catch (error) {
        console.log(error);
      }
    });
  });
};

export const buscarCelular = async (dni) => {
  let correo = await sqlRead(
    `select celular from public.contribuyente where dni like $1`,
    [dni]
  );
  return correo.rows[0].celular;
};

export const buscarCorreo = async (dni) => {
  let correo = await sqlRead(
    `select correo from public.contribuyente where dni like $1`,
    [dni]
  );
  return correo.rows[0].correo;
};

export const buscarCorreoDC = async (id_deuda, id_cuotas) => {
  let correo = await sqlRead(
    `SELECT c1.correo
FROM "public".deuda_detalle dd
	INNER JOIN "public".deuda d ON ( dd.id_deuda = d.id_deuda  )
    INNER JOIN "public".contribuyente c1 ON ( d.dni = c1.dni  )
    WHERE dd.id_deuda = $1 AND dd.cuotas = $2 `,
    [id_deuda, id_cuotas]
  );

  return correo.rows[0].correo;
};

export const buscarDNI = async (id_deuda, id_cuotas) => {
  let dni = await sqlRead(
    `SELECT c1.dni
FROM "public".deuda_detalle dd
	INNER JOIN "public".deuda d ON ( dd.id_deuda = d.id_deuda  )
    INNER JOIN "public".contribuyente c1 ON ( d.dni = c1.dni  )
    WHERE dd.id_deuda = $1 AND dd.cuotas = $2 `,
    [id_deuda, id_cuotas]
  );

  return dni.rows[0].dni;
};
export const enviarNotificacionEspecifico = (dni, titulo, mensaje) => {
  poolRead.query(
    "SELECT * FROM public.notifcacion_usuario where dni like $1",
    [dni],
    (error, results) => {
      if (error) {
        throw error;
      }

      results.rows.map(async (usuario) => {
        const payload = JSON.stringify({
          title: titulo,
          message: mensaje,
        });

        try {
          await webpush.sendNotification(JSON.parse(usuario.data), payload);
        } catch (error) {
          console.log(error);
        }
      });
    }
  );
};

export const enviarNotificacionTodos = (titulo, mensaje) => {
  poolRead.query(
    "SELECT * FROM public.notifcacion_usuario",
    [],
    (error, results) => {
      if (error) {
        throw error;
      }

      // console.log(results.rows);
      results.rows.map(async (usuario) => {
        const payload = JSON.stringify({
          title: titulo,
          message: mensaje,
        });

        try {
          await webpush.sendNotification(JSON.parse(usuario.data), payload);
        } catch (error) {
          console.log(error);
        }
      });
    }
  );
};

export const sqlRead = (sql, data) => {
  return new Promise((resolve, reject) => {
    poolRead.query(sql, data, (error, results) => {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
};
export const sqlWrite = (sql, data) => {
  return new Promise((resolve, reject) => {
    poolWrite.query(sql, data, (error, results) => {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
};

export const sqlInsert = (sql, data) => {
  return new Promise((resolve, reject) => {
    poolWrite.query(sql, data, (error, results) => {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
};
