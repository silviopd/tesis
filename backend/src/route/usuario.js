import express from "express";
let router = express.Router();
import poolRead from "../db/connectionRead";
import poolWrite from "../db/connectionWrite";

router.get("/", (req, res) => {
  poolRead.query(
    `SELECT 	upm.dni, 
	c1.nombres ||' '|| c1.apellido_paterno||' '||c1.apellido_materno as usuario,
	upm.id_perfiles, 
	p.nombre as perfiles, 
	upm.id_menu, 
	m1.nombre as menu,
	upm.estado
FROM "public".usuario_perfil_menu upm 
	INNER JOIN "public".perfiles p ON ( upm.id_perfiles = p.id_perfiles  )  
	INNER JOIN "public".menu m1 ON ( upm.id_menu = m1.id_menu  )  
	INNER JOIN "public".usuario u ON ( upm.dni = u.dni  )  
		INNER JOIN "public".contribuyente c1 ON ( u.dni = c1.dni  )`,
    [],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.get("/usuario", (req, res) => {
  console.log(req.query.dni, req.query.id_perfil, req.query.id_menu);

  poolRead.query(
    `SELECT 	upm.dni, 
	c1.nombres ||' '|| c1.apellido_paterno||' '||c1.apellido_materno as usuario,
	upm.id_perfiles, 
	p.nombre as perfiles, 
	upm.id_menu, 
	m1.nombre as menu,
	upm.estado
FROM "public".usuario_perfil_menu upm 
	INNER JOIN "public".perfiles p ON ( upm.id_perfiles = p.id_perfiles  )  
	INNER JOIN "public".menu m1 ON ( upm.id_menu = m1.id_menu  )  
	INNER JOIN "public".usuario u ON ( upm.dni = u.dni  )
    INNER JOIN "public".contribuyente c1 ON ( u.dni = c1.dni)
    WHERE upm.dni = $1 AND upm.id_perfiles = $2 AND upm.id_menu = $3`,
    [req.query.dni, req.query.id_perfil, req.query.id_menu],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json(results.rows);
    }
  );
});

router.post("/nuevo", (req, res) => {
  console.log(
    req.body.dni,
    req.body.id_perfiles,
    req.body.id_menu,
    req.body.estado
  );
  poolWrite.query(
    `INSERT INTO public.usuario_perfil_menu(dni, id_perfiles, id_menu, estado) VALUES ($1, $2, $3, $4);`,
    [
      req.body.dni,
      req.body.id_perfiles,
      req.body.id_menu,
      req.body.estado,
      // req.body.dni_usuario,
    ],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json("ok");
    }
  );
});

router.post("/editar", (req, res) => {
  poolWrite.query(
    `UPDATE public.usuario_perfil_menu SET estado=$4 WHERE dni=$1 AND id_perfiles=$2 AND id_menu=$3;`,
    [
      req.body.dni,
      req.body.id_perfiles,
      req.body.id_menu,
      req.body.estado,
      // req.body.dni_usuario,
    ],
    (error, results) => {
      if (error) {
        return res.status(500).send(error);  
      }
      res.status(200).json("ok");
    }
  );
});

module.exports = router;
