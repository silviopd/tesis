import { Pool } from "pg";

const HOST_DATABASE = process.env.HOST_DB;
const PORT_DATABASE = process.env.PORT_DB_READ;

const config = {
  user: "postgres",
  host: HOST_DATABASE,
  database: "tesis",
  password: "silviopd",
  port: PORT_DATABASE,
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000,
};

const pool = new Pool(config);

export default pool;
