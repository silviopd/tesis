"use strict";

import express from "express";
import cors from "cors";
import morgan from "morgan";

const app = express();

app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

let login = require("./route/login");
let deudas = require("./route/deudas");
let contribuyente = require("./route/contribuyente");
let categoria = require("./route/categoria");
let notificacion = require("./route/notificacion");
let tipotransaccion = require("./route/tipotransaccion");
let usuario = require("./route/usuario");
let perfil = require("./route/perfil");
let menu = require("./route/menu");
let reclamo = require("./route/reclamo");
let reporte = require("./route/reporte");

app.use("/api/login", login);
app.use("/api/deudas", deudas);
app.use("/api/contribuyente", contribuyente);
app.use("/api/categoria", categoria);
app.use("/api/notificacion", notificacion);
app.use("/api/tipotransaccion", tipotransaccion);
app.use("/api/usuario", usuario);
app.use("/api/perfil", perfil);
app.use("/api/menu", menu);
app.use("/api/reclamo", reclamo);
app.use("/api/reporte", reporte);

app.use("/", (req, res) => {
  res.status(200).send("La API funciona correctamente");
});

const PORT = process.env.PORT || 4000;

app.listen(PORT);
