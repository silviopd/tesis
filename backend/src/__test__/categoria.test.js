var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /categoria", function () {
  it("/", function (done) {
    server
      .get("/api/categoria")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
