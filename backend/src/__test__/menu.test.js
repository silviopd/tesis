var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /menu", function () {
  it("respond with json", function (done) {
    server
      .get("/api/menu")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
