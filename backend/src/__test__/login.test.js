var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /login", function () {
  it("login", function (done) {
    server
      .post("/api/login")
      .set("Accept", "application/json")
      .send({ user: "46705014", password: "123" })
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
