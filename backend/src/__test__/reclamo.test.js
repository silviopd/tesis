var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /reclamo", function () {
  it("/reclamo aprobado", function (done) {
    server
      .get("/api/reclamo/reclamo?opcion=R")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("/reclamo rechazado", function (done) {
    server
      .get("/api/reclamo/reclamo?opcion=S")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
