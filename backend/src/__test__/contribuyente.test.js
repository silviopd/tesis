var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /contribuyente", function () {
  it("/", function (done) {
    server
      .get("/api/contribuyente")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("eliminar", function (done) {
    server
      .post("/api/contribuyente/eliminar")
      .set("Accept", "application/json")
      .send({ dni: "99999999", dni_usuario: "46705014" })
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("/leerdatos", function (done) {
    server
      .get("/api/contribuyente/leerdatos?dni=46705014")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("nuevo", function (done) {
    server
      .post("/api/contribuyente/nuevo")
      .set("Accept", "application/json")
      .send({
        dni: "88888888",
        nombre: "a",
        paterno: "a",
        materno: "a",
        codigo: "a",
        celular: "a",
        direccion: "a",
        referencia: "a",
        correo: "a",
        estado: "A",
        dni_usuario: "46705014",
      })
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("editar", function (done) {
    server
      .post("/api/contribuyente/editar")
      .set("Accept", "application/json")
      .send({
        dni: "88888888",
        nombre: "a",
        paterno: "a",
        materno: "a",
        codigo: "a",
        celular: "a",
        direccion: "a",
        referencia: "a",
        correo: "a",
        estado: "A",
        dni_usuario: "46705014",
      })
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("editar-usuario", function (done) {
    server
      .post("/api/contribuyente/editar-usuario")
      .set("Accept", "application/json")
      .send({
        dni: "88888888",
        nombre: "a",
        paterno: "a",
        materno: "a",
        celular: "a",
        direccion: "a",
        referencia: "a",
        correo: "a",
      })
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
