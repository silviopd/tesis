var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /deudas", function () {
  it("/", function (done) {
    server
      .get("/api/deudas")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("/leerdatos", function (done) {
    server
      .get("/api/deudas/leerdatos?id_deuda=2")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("/detalle", function (done) {
    server
      .get("/api/deudas/detalle?id_deuda=2")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("/detalle-dni-pagado", function (done) {
    server
      .get("/api/deudas/detalle-dni-pagado?dni=46705014")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
