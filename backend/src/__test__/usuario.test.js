var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /usuario", function () {
  it("/usuario todos", function (done) {
    server
      .get("/api/usuario")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("/usuario especifico", function (done) {
    server
      .get("/api/usuario/usuario?dni=46705014&dni=1&id_menu=1")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
