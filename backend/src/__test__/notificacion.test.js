var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /notificacion", function () {
  it("/", function (done) {
    server
      .get("/api/notificacion")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("/usuario", function (done) {
    server
      .get("/api/notificacion/usuario?dni=46705014")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
