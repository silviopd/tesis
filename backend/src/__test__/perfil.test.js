var supertest = require("supertest");
var server = supertest.agent("http://localhost:4000");

describe("Endpoint /perfil", function () {
  it("/perfil", function (done) {
    server
      .get("/api/perfil")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
