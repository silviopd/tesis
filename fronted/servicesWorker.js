// var CACHE_NAME = 'SNE-CGTCH-v1';
// var urlsToCache = [
//   '/'
// ];

// self.addEventListener('install', function(event) {
//   // Perform install steps
//   event.waitUntil(
//       caches.open(CACHE_NAME)
//           .then(function(cache) {
//             console.log('Cache open!');
//             return cache.addAll(urlsToCache);
//           })
//   );
// });

// self.addEventListener('fetch', function(event) {
//   event.respondWith(
//       caches.match(event.request)
//           .then(function(response) {
//             // Cache hit - return response
//             if (response) {
//               return response;
//             }
//             return fetch(event.request);
//           })
//   );
// });

self.addEventListener("push", (e) => {
  const data = e.data.json();
  console.log(data);
  // console.log("Notificacion recibida");

  self.registration.showNotification(data.title, {
    body: data.message,
    icon: "../img/cgt-sinletras.jpg",
  });
});
