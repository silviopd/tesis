<aside class="main-sidebar elevation-4 sidebar-light-primary">
    <!-- Brand Logo -->
    <a href="../home/index.php" class="brand-link navbar-primary">
        <img src="../../img/cgt-sinletras.png" alt="cgt Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text" style="color: white">SNE-CGTCH</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="../../img/user.png" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <div id="menu-nombre"></div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-compact nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <div id="menu-principal"></div>
<!--                <li class="nav-item">-->
<!--                    <a href="../home/index.php" class="nav-link active">-->
<!--                        <i class="nav-icon fa fa-home"></i>-->
<!--                        <p>-->
<!--                            Home-->
<!--                        </p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-header">Administrador</li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="pages/calendar.html" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-users"></i>-->
<!--                        <p>-->
<!--                            Contribuyente-->
<!--                        </p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="../deuda/index.php" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-dollar-sign"></i>-->
<!--                        <p>-->
<!--                            Deuda-->
<!--                        </p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="pages/gallery.html" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-percent"></i>-->
<!--                        <p>-->
<!--                            Descuento-->
<!--                        </p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="pages/gallery.html" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-user"></i>-->
<!--                        <p>-->
<!--                            Usuario-->
<!--                        </p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item has-treeview">-->
<!--                    <a href="#" class="nav-link">-->
<!--                        <i class="nav-icon ion ion-pie-graph"></i>-->
<!--                        <p>-->
<!--                            Reportes-->
<!--                            <i class="fas fa-angle-left right"></i>-->
<!--                        </p>-->
<!--                    </a>-->
<!--                    <ul class="nav nav-treeview">-->
<!--                        <li class="nav-item">-->
<!--                            <a href="pages/examples/login.html" class="nav-link">-->
<!--                                <i class="far fa-circle nav-icon"></i>-->
<!--                                <p>Reporte 1</p>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a href="pages/examples/register.html" class="nav-link">-->
<!--                                <i class="far fa-circle nav-icon"></i>-->
<!--                                <p>Reporte 2</p>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a href="pages/examples/forgot-password.html" class="nav-link">-->
<!--                                <i class="far fa-circle nav-icon"></i>-->
<!--                                <p>Reporte 3</p>-->
<!--                            </a>-->
<!--                        </li>4-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li class="nav-header">Usuario</li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="https://adminlte.io/docs/3.0" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-comment-dollar"></i>-->
<!--                        <p>Deudas</p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="https://adminlte.io/docs/3.0" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-piggy-bank"></i>-->
<!--                        <p>Descuentos</p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item has-treeview">-->
<!--                    <a href="#" class="nav-link">-->
<!--                        <i class="nav-icon ion ion-pie-graph"></i>-->
<!--                        <p>-->
<!--                            Reportes-->
<!--                            <i class="fas fa-angle-left right"></i>-->
<!--                        </p>-->
<!--                    </a>-->
<!--                    <ul class="nav nav-treeview">-->
<!--                        <li class="nav-item">-->
<!--                            <a href="pages/examples/login.html" class="nav-link">-->
<!--                                <i class="far fa-circle nav-icon"></i>-->
<!--                                <p>Reporte 1</p>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a href="pages/examples/register.html" class="nav-link">-->
<!--                                <i class="far fa-circle nav-icon"></i>-->
<!--                                <p>Reporte 2</p>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a href="pages/examples/forgot-password.html" class="nav-link">-->
<!--                                <i class="far fa-circle nav-icon"></i>-->
<!--                                <p>Reporte 3</p>-->
<!--                            </a>-->
<!--                        </li>4-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li class="nav-header">Configuración</li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="https://adminlte.io/docs/3.0" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-user"></i>-->
<!--                        <p>Datos personales</p>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a href="https://adminlte.io/docs/3.0" class="nav-link">-->
<!--                        <i class="nav-icon fa fa-lock"></i>-->
<!--                        <p>Cerrar sesión</p>-->
<!--                    </a>-->
<!--                </li>-->
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>