$(function () {
  $(".select2").select2();
  $("#menu121").addClass("active");
  $("#menu-home").removeClass("active");

  cargarData();
  comboPerfiles();
  comboMenu();
});

let cargarData = async () => {
  try {
    let { data } = await axios.get(`${URL_API}/usuario`, {});
    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Dni</th>
                                        <th>Nombres</th>
                                        <th>Perfiles</th>
                                        <th>Menu</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.dni}</td>
                                        <td>${dato.usuario}</td>
                                        <td>${dato.perfiles}</td>
                                        <td>${dato.menu}</td>
                                        <td>${dato.estado}</td>
                                        <td>
                                            <div style="display: inline-flex;">
                                                <button type="button" class="btn btn-warning" style="margin-right: 10px" onclick="leerDatos('${dato.dni}','${dato.id_perfiles}','${dato.id_menu}')" data-toggle="modal" data-target="#modal-lg"><i class="fas fa-edit"></i></button>
                                            </div>
                                        </td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }  
};

let comboPerfiles = async () => {
  try {
    let { data } = await axios.get(`${URL_API}/perfil`, {});
    let html = "";
    html += `<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" id="modal-perfil">`;
    data.map((dato, i) => {
      html += `<option ${i === 0 ? 'selected="selected"' : null} value="${
        dato.id_perfiles
      }">${dato.nombre}</option>`;
    });

    html += `</select>`;

    $("#modal-combo-perfiles").html(html);

    $("#modal-perfil").select2();
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

let comboMenu = async () => {
  try {
    let { data } = await axios.get(`${URL_API}/menu`, {});
    let html = "";
    html += `<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" id="modal-menu">`;
    data.map((dato, i) => {
      html += `<option ${i === 0 ? 'selected="selected"' : null} value="${
        dato.id_menu
      }">${dato.nombre}</option>`;
    });

    html += `</select>`;

    $("#modal-combo-menu").html(html);

    $("#modal-menu").select2();
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }  
};

let nuevo = () => {
  $("#modal-title").text("NUEVO");
  $("#modal-button").text("GRABAR");

  $("#modal-dni").val("");
  $("#modal-perfil").val(1).trigger("change");
  $("#modal-menu").val(1).trigger("change");
  $("#modal-estado").val("A").trigger("change");

  menuHD(false);

  $("#txttipooperacion").val("agregar");
};

let leerDatos = async (dni, id_perfil, id_menu) => {
  try {
    $("#modal-title").text("EDITAR");
    $("#modal-button").text("GUARDAR");

    menuHD(true);

    let { data } = await axios.get(`${URL_API}/usuario/usuario`, {
      params: { dni, id_perfil, id_menu },
    });

    let datos = data[0];

    $("#modal-dni").val(datos.dni);
    $("#modal-perfil").val(datos.id_perfiles).trigger("change");
    $("#modal-menu").val(datos.id_menu).trigger("change");
    $("#modal-estado").val(datos.estado).trigger("change");

    $("#txttipooperacion").val("editar");
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

document.getElementById("modal-button").onclick = () => {
  let tipo = document.getElementById("txttipooperacion").value;

  let dni = $("#modal-dni").val();
  let id_perfiles = $("#modal-perfil").val();
  let id_menu = $("#modal-menu").val();
  let estado = $("#modal-estado").val();
  let dni_usuario = sessionStorage.getItem("Dni");

  if (dni === "") {
    Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
    return;
  }

  tipo === "agregar"
    ? agregar(dni, id_perfiles, id_menu, estado, dni_usuario)
    : editar(dni, id_perfiles, id_menu, estado, dni_usuario);
};

let agregar = (dni, id_perfiles, id_menu, estado, dni_usuario) => {
  console.log(dni, id_perfiles, id_menu, estado, dni_usuario);

  Swal.fire({
    title: "Deseas Agregar?",
    text: "Se agregará un nuevo usuario perfil!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, agregar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let { data } = await axios.post(`${URL_API}/usuario/nuevo`, {
          dni,
          id_perfiles,
          id_menu,
          estado,
          dni_usuario,
        });

        console.log(data);

        if (data === "ok") {
          Swal.fire("Agregado!", "Se ha agregado correctamente", "success");

          cargarData();
          nuevo();
          $("#modal-cerrar").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};

let editar = (dni, id_perfiles, id_menu, estado, dni_usuario) => {
  console.log(dni, id_perfiles, id_menu, estado, dni_usuario);

  Swal.fire({
    title: "Deseas Editar?",
    text: "Se editara este usuario perfil!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, editar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let { data } = await axios.post(`${URL_API}/usuario/editar`, {
          dni,
          id_perfiles,
          id_menu,
          estado,
          dni_usuario,
        });

        console.log(data);

        if (data === "ok") {
          Swal.fire(
            "Modificado!",
            "Se ha modificado correctamente.",
            "success"
          );

          cargarData();
          nuevo();
          $("#modal-cerrar").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};

const menuHD = (value) => {
  $("#modal-perfil").prop("disabled", value);
  $("#modal-menu").prop("disabled", value);
  $("#modal-dni").prop("disabled", value);
};
