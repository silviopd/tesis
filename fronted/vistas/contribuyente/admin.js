$(function () {
  $(".select2").select2();
  $("#menu11").addClass("active");
  $("#menu-home").removeClass("active");

  cargarData();
});

let cargarData = async () => {
  try {
    let { data } = await axios.get(`${URL_API}/contribuyente`, {});
    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Dni</th>
                                        <th>Nombres</th>
                                        <th>Ape. Paterno</th>
                                        <th>Ape. Materno</th>
                                        <th>Código</th>
                                        <th>Celular</th>
                                        <th>Correo</th>
                                        <th>Dirección</th>
                                        <th>Referencia</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.dni}</td>
                                        <td>${dato.nombres}</td>
                                        <td>${dato.apellido_paterno}</td>
                                        <td>${dato.apellido_materno}</td>
                                        <td>${dato.codigo}</td>
                                        <td>${dato.celular}</td>
                                        <td>${dato.correo}</td>
                                        <td>${dato.direccion}</td>
                                        <td>${dato.referencia}</td>
                                        <td>${dato.estado}</td>
                                        <td>
                                            <div style="display: inline-flex;">
                                                <button type="button" class="btn btn-warning" style="margin-right: 10px" onclick="leerDatos('${dato.dni}')" data-toggle="modal" data-target="#modal-lg"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger" onclick="eliminar('${dato.dni}')"><i class="fas fa-trash"></i></button>                     
                                            </div>
                                        </td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }

  
};

let nuevo = () => {
  $("#modal-title").text("NUEVO");
  $("#modal-button").text("GRABAR");

  $("#modal-dni").val("");
  $("#modal-codigo").val("");
  $("#modal-nombre").val("");
  $("#modal-materno").val("");
  $("#modal-paterno").val("");
  $("#modal-celular").val("");
  $("#modal-correo").val("");
  $("#modal-direccion").val("");
  $("#modal-estado").val("A").trigger("change");
  $("#modal-referencia").val("");

  $("#txttipooperacion").val("agregar");

  menuHD(false);
};

let eliminar = (dni) => {
  Swal.fire({
    title: "Eliminar",
    text: "¡Estás seguro de eliminar este registro!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, eliminar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let dni_usuario = sessionStorage.getItem("Dni");

        let { data } = await axios.post(`${URL_API}/contribuyente/eliminar`, {
          dni,
          dni_usuario,
        });

        if (data[0].v_result === 200) {
          Swal.fire("Eliminado!", "Se ha borrado correctamente.", "success");

          cargarData();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }
  });
};

let leerDatos = async (dni) => {
  try {
    $("#modal-title").text("EDITAR");
    $("#modal-button").text("GUARDAR");
    
    let { data } = await axios.get(`${URL_API}/contribuyente/leerdatos`, {
      params: { dni },
    });

    let datos = data[0];

    $("#modal-dni").val(datos.dni);
    $("#modal-codigo").val(datos.codigo);
    $("#modal-nombre").val(datos.nombres);
    $("#modal-materno").val(datos.apellido_materno);
    $("#modal-paterno").val(datos.apellido_paterno);
    $("#modal-celular").val(datos.celular);
    $("#modal-correo").val(datos.correo);
    $("#modal-direccion").val(datos.direccion);
    $("#modal-estado").val(datos.estado).trigger("change");
    $("#modal-referencia").val(datos.referencia);

    $("#txttipooperacion").val("editar");

    menuHD(true);
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

document.getElementById("modal-button").onclick = () => {
  let tipo = document.getElementById("txttipooperacion").value;

  let dni = $("#modal-dni").val();
  let codigo = $("#modal-codigo").val();
  let nombre = $("#modal-nombre").val();
  let materno = $("#modal-materno").val();
  let paterno = $("#modal-paterno").val();
  let celular = $("#modal-celular").val();
  let correo = $("#modal-correo").val();
  let direccion = $("#modal-direccion").val();
  let estado = $("#modal-estado").val();
  let referencia = $("#modal-referencia").val();
  let dni_usuario = sessionStorage.getItem("Dni");

  if (
    dni === "" ||
    nombre === "" ||
    materno === "" ||
    paterno === "" ||
    celular === "" ||
    correo === ""
  ) {
    Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
    return;
  }

  tipo === "agregar"
    ? agregar(
        dni,
        codigo,
        nombre,
        materno,
        paterno,
        celular,
        correo,
        direccion,
        estado,
        referencia,
        dni_usuario
      )
    : editar(
        dni,
        codigo,
        nombre,
        materno,
        paterno,
        celular,
        correo,
        direccion,
        estado,
        referencia,
        dni_usuario
      );
};

let agregar = (
  dni,
  codigo,
  nombre,
  materno,
  paterno,
  celular,
  correo,
  direccion,
  estado,
  referencia,
  dni_usuario
) => {
  Swal.fire({
    title: "Deseas Agregar?",
    text: "Se agregará un nuevo registro!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, agregar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let { data } = await axios.post(`${URL_API}/contribuyente/nuevo`, {
          dni,
          codigo,
          nombre,
          materno,
          paterno,
          celular,
          correo,
          direccion,
          estado,
          referencia,
          dni_usuario,
        });

        if (data[0].v_result === 200) {
          Swal.fire("Agregado!", "Se ha agregado exitosamente", "success");

          cargarData();
          nuevo();
          $("#modal-cerrar").click();
        } else {
          Swal.fire("Error!", "No se ha agregado exitosamente", "error");

          cargarData();
          nuevo();
          $("#modal-cerrar").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }
  });
};

let editar = (
  dni,
  codigo,
  nombre,
  materno,
  paterno,
  celular,
  correo,
  direccion,
  estado,
  referencia,
  dni_usuario
) => {
  Swal.fire({
    title: "Deseas Editar?",
    text: "Se editara este registro!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, editar!",
  }).then(async (result) => {
    try {
       if (result.value) {
         let { data } = await axios.post(`${URL_API}/contribuyente/editar`, {
           dni,
           codigo,
           nombre,
           materno,
           paterno,
           celular,
           correo,
           direccion,
           estado,
           referencia,
           dni_usuario,
         });

         if (data[0].v_result === 200) {
           Swal.fire("Agregado!", "Se ha agregado exitosamente", "success");

           cargarData();
           nuevo();
           $("#modal-cerrar").click();
         }
       }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }
  });
};

const menuHD = (value) => {
  $("#modal-dni").prop("disabled", value);
  $("#modal-codigo").prop("disabled", value);
};
