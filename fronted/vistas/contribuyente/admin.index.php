<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SNE-CGTCH</title>
    <link rel="icon" href="../../img/cgt.ico" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php require_once '../plugin-basic.php'?>

    <!-- DataTables -->
    <link rel="stylesheet" href="../../util/lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../util/lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    
    <!-- Select2 -->
    <link rel="stylesheet" href="../../util/lte/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="../../util/lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

</head>
<body class="hold-transition sidebar-mini layout-fixed text-sm">
<div class="wrapper">
	
	<?php require_once '../header.php'?>
	
	<?php require_once '../menu.php'?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Contribuyentes</h1>
                    </div><!-- /.col -->

                </div><!-- /.row -->

            </div><!-- /.container-fluid -->

        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tabla de todos los contribuyentes</h3>
                            </div>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-lg" onclick="nuevo()">
                                Nuevos Contribuyentes
                            </button>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div id="maintatable"></div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
	<?php require_once '../footer.php'?>
</div>

<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal-cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="form-control" id="txttipooperacion" >
                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Dni</label>
                            <input type="text" class="form-control" placeholder="" id="modal-dni" required />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Código</label>
                            <input type="text" class="form-control" placeholder="" id="modal-codigo" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre completo</label>
                    <input type="text" class="form-control" placeholder="" id="modal-nombre" required />
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <input type="text" class="form-control" placeholder="" id="modal-paterno" required />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Apellido Materno</label>
                            <input type="text" class="form-control" placeholder="" id="modal-materno" required />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Celular</label>
                            <input type="text" class="form-control" placeholder="" id="modal-celular" required />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="text" class="form-control" placeholder="" id="modal-correo" required />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" class="form-control" placeholder="" id="modal-direccion">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Referencia</label>
                            <input type="text" class="form-control" placeholder="" id="modal-referencia">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Estado</label>
                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" id="modal-estado">
                        <option selected="selected" value="A">Activo</option>
                        <option value="I">Inactivo</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="modal-button"></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- DataTable -->
<script src="../../util/lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../util/lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- Select2 -->
<script src="../../util/lte/plugins/select2/js/select2.full.min.js"></script>

<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="../util.js"></script>
<script src="admin.js"></script>
<!-- ./wrapper -->
</body>
</html>
