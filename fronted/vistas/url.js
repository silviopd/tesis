// PRODUCTION
const HOST = "172.17.0.3";
const PORT = "30769";

// DEV
// const HOST = "172.99.0.11";
// const PORT = "4000";

const URL_API = `http://${HOST}:${PORT}/api`;
