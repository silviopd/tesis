$(function () {
  $("#menu83").addClass("active");
  $("#menu-home").removeClass("active");

  cargarData();
});

let cargarData = async () => {
  try {
    let dni = sessionStorage.getItem("Dni");

    let { data } = await axios.get(`${URL_API}/contribuyente/leerdatos`, {
      params: { dni },
    });

    let datos = data[0];

    $("#modal-nombre").val(datos.nombres);
    $("#modal-paterno").val(datos.apellido_paterno);
    $("#modal-materno").val(datos.apellido_materno);
    $("#modal-celular").val(datos.celular);
    $("#modal-correo").val(datos.correo);
    $("#modal-direccion").val(datos.direccion);
    $("#modal-referencia").val(datos.referencia);
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

document.getElementById("modal-generales").onclick = async () => {
  let dni = sessionStorage.getItem("Dni");
  let nombre = $("#modal-nombre").val();
  let paterno = $("#modal-paterno").val();
  let materno = $("#modal-materno").val();
  let celular = $("#modal-celular").val();
  let correo = $("#modal-correo").val();
  let direccion = $("#modal-direccion").val();
  let referencia = $("#modal-referencia").val();

  if (
    nombre === "" ||
    materno === "" ||
    paterno === "" ||
    celular === "" ||
    correo === ""
  ) {
    Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
    return;
  }

  Swal.fire({
    title: "Deseas Editar?",
    text: "Se editara este registro!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, editar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let { data } = await axios.post(
          `${URL_API}/contribuyente/editar-usuario`,
          {
            dni,
            nombre,
            materno,
            paterno,
            celular,
            correo,
            direccion,
            referencia,
          }
        );

        if (data.length === 0) {
          Swal.fire("Modificado!", "Se ha modificado exitosamente", "success");

          sessionStorage.setItem("Nombre", nombre);
          sessionStorage.setItem("Paterno", paterno);
          sessionStorage.setItem("Materno", materno);

          location.reload();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }
  });
};

document.getElementById("modal-password").onclick = () => {
  console.log("chau");
};
