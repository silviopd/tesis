<!-- CSS -->
<link rel="manifest" href="../../manifest.json">

<!-- Font Awesome -->
<link rel="stylesheet" href="../../util/lte/plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="../../util/lte/dist/css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


<!-- JS -->

<!-- jQuery -->
<script src="../../util/lte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../util/lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../util/lte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../util/lte/dist/js/demo.js"></script>


<!--axios-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
<script src="../url.js"></script>
<script src="../main.js"></script>
