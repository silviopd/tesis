const data = async (user, password) => {
  try {
    let { data } = await axios.post(`${URL_API}/login`, { user, password });

    if (data.mensaje === "ok") {
      sessionStorage.setItem("Token", "");
      sessionStorage.setItem("Dni", data.dni);
      sessionStorage.setItem("Nombre", data.nombres);
      sessionStorage.setItem("Paterno", data.apellido_paterno);
      sessionStorage.setItem("Materno", data.apellido_materno);
      sessionStorage.setItem("Menu", data.menu);

      location.href = "../home/index.php";
    } else {
      Swal.fire("Error!", data.mensaje, "error");
    }
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
  
};

document.getElementById("btnLogin").onclick = () => {
  let user = document.getElementById("txtDni").value;
  let password = document.getElementById("txtPassword").value;

  data(user, password);
};

function isNumberKey(evt) {
  var charCode = evt.which ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
  return true;
}
