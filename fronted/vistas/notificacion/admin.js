$(function () {
  $("#menu101").addClass("active");
  $("#menu-home").removeClass("active");

  cargarData();
});

let cargarData = async () => {
  try {
    let { data } = await axios.get(`${URL_API}/notificacion`, {});
    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Dni</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.dni}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger" onclick="notificar('${dato.dni}')"><i class="fas fa-bullhorn"></i></button>
                                        </td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }  
};

const notificar = async (dni) => {
  try {
    await axios.get(`${URL_API}/notificacion/especifico`, { params: { dni } });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

const notificarTodos = async () => {
  try {
    await axios.get(`${URL_API}/notificacion/todos`, {});
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};
