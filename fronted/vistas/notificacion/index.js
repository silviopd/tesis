$(function () {
  $("#menu102").addClass("active");
  $("#menu-home").removeClass("active");

  cargarData();
});

let cargarData = async () => {
  try {
    let dni = await sessionStorage.getItem("Dni");
    let { data } = await axios.get(`${URL_API}/notificacion/usuario`, {
      params: { dni },
    });
    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>App</th>
                                        <th>Correo</th>
                                        <th>Web</th>
                                        <th>Mensaje</th>
                                        <th>Fecha</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${
                                          dato.app == "S" ? "SI" : "NO"
                                        }</td>
                                        <td>${
                                          dato.correo == "S" ? "SI" : "NO"
                                        }</td>
                                        <td>${
                                          dato.web == "S" ? "SI" : "NO"
                                        }</td>
                                        <td>${dato.mensaje}</td>
                                        <td>${dato.fecha.split("T")[0]} ${
        dato.fecha.split("T")[1].split(".")[0]
      }</td>                                        
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};
