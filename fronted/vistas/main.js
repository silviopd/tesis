const PUBLIc_VAPID_KEY =
  "BG1S5VBNxQtxe4_FpyVNUja-8Pw1OREcSxXqTIeRH9j8wVtSSDlcp9NcbDAhRZTBn_u5nTpRkyoVKPuERn5qrGY";

function urlBase64ToUint8Array(base64String) {
  const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding).replace(/-/g, "+").replace(/_/g, "/");

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

const subscription = () => {
  try {
    if ("serviceWorker" in navigator) {
      window.addEventListener("load", function () {
        navigator.serviceWorker.register("../../servicesWorker.js").then(
          async function (register) {
            // Registration was successful
            console.log(
              "ServiceWorker registration successful with scope: ",
              register.scope
            );

            const subscription = await register.pushManager.subscribe({
              userVisibleOnly: true,
              applicationServerKey: urlBase64ToUint8Array(PUBLIc_VAPID_KEY),
            });

            let data = await axios.post(
              `${URL_API}/notificacion/subscripcion`,
              {
                subscription: JSON.stringify(subscription),
                dni: sessionStorage.getItem("Dni"),
              }
            );
            console.log(data);
            console.log("suscrito");
          },
          function (err) {
            // registration failed :(
            console.log("ServiceWorker registration failed: ", err);
          }
        );
      });
    }
  } catch (error) {
    Swal.fire("Error!", "", error);
  }  
};

subscription();
