<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SNE-CGTCH</title>
  <link rel="icon" href="../../img/cgt.ico" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <?php require_once '../plugin-basic.php'?>
  <link rel="stylesheet" href="../../util/lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../util/lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <?php require_once '../header.php'?>

    <?php require_once '../menu.php'?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reportes</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Reporte Deuda</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body" id="reporte">
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php require_once '../footer.php'?>

</div>
<!-- ./wrapper -->


<!-- ChartJS -->
<script src="../../util/lte/plugins/chart.js/Chart.min.js"></script>

<!-- page script -->
<script src="../../util/lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../util/lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script src="../util.js"></script>
<script src="deudas.js"></script>

<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</body>
</html>
