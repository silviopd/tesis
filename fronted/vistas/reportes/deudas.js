$(function () {
  cargarData();

  $("#menu52").addClass("active");
  $("#menu-home").removeClass("active");
});

let cargarData = async () => {
  try {
     let dni = sessionStorage.getItem("Dni");
     let { data } = await axios.get(`${URL_API}/reporte/client`, {
       params: { dni },
     });
     console.log(data);

     let nombres = data[0].nombres;
     let apellido_paterno = data[0].apellido_paterno;
     let apellido_materno = data[0].apellido_materno;
     let celular = data[0].celular;
     let direccion = data[0].direccion;
     let referencia = data[0].referencia;
     let correo = data[0].correo;

     let html = "";

     html += `
  <div class="row">
    <div class="form-group row col-sm-6">
      <label for="inputEmail3" class="col-sm-3 col-form-label">Nombres</label>
      <div class="col-sm-9">
        <p class="form-control">${nombres} ${apellido_paterno} ${apellido_materno}</p>
      </div>
    </div>
    <div class="form-group row col-sm-6">
      <label for="inputEmail3" class="col-sm-3 col-form-label">Direccion</label>
      <div class="col-sm-9">
        <p class="form-control">${direccion}</p>
      </div>
    </div>
    <div class="form-group row col-sm-6">
      <label for="inputEmail3" class="col-sm-3 col-form-label">Referencia</label>
      <div class="col-sm-9">
        <p class="form-control">${referencia}</p>
      </div>
    </div>
    <div class="form-group row col-sm-6">
      <label for="inputEmail3" class="col-sm-3 col-form-label">Correo</label>
      <div class="col-sm-9">
        <p class="form-control">${correo}</p>
      </div>
    </div>
    <div class="form-group row col-sm-6">
      <label for="inputEmail3" class="col-sm-3 col-form-label">Celular</label>
      <div class="col-sm-9">
        <p class="form-control">${celular}</p>
      </div>
    </div>
  </div>
    <br/>

    <div class="row">
      <div class="col-sm-6" id="deuda-izquierda"></div>
      <div class="col-sm-6" id="deuda-derecha"></div>
    </div>
                  `;

     let filtro = [
       ...new Map(data.map((item) => [item["id_deuda"], item])).values(),
     ];

     filtro.map((dato, i) => {
       html += `
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="card card-primary collapsed-card">
              <div class="card-header">
                <h3 class="card-title">Deuda ${dato.id_deuda}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body" id="reporte">
                <div class="row" style="border: 3px solid #929ca7; border-radius: 10px">
                  <div class="col-sm-3" style="border-right: 3px solid #929ca7">
                    <h3 class="text-center" style="border-bottom: 3px solid #929ca7; color: #007BFF; font-weight: bold;">DEUDA</h3>
                    <div id="deuda-izquierda-${dato.id_deuda}"></div>
                  </div>
                  <div class="col-sm-9">
                    <h3 class="text-center" style="border-bottom: 3px solid #929ca7; color: #007BFF; font-weight: bold;">DEUDA DETALLE</h3>
                    <div class="row" id="deuda-derecha-${dato.id_deuda}"></div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <br/>`;
     });

     $("#reporte").html(html);

     filtro.map((dato) => {
       let izquierda = ``;
       izquierda += `
        <div style="padding:20px !important">
          <p><span style="font-weight: bold"; font-weight: bold">Deuda:</span> ${
            dato.id_deuda
          }</p>
          <p><span style="font-weight: bold"; font-weight: bold">Fecha:</span> ${
            dato.deuda_fecha.split("T")[0]
          }</p>
          <p><span style="font-weight: bold"; font-weight: bold">Subtotal:</span> ${
            dato.deuda_subtotal
          }</p>
          <p><span style="font-weight: bold"; font-weight: bold">Descuento:</span> ${
            dato.deuda_descuento
          }</p>
          <p><span style="font-weight: bold"; font-weight: bold">Mora:</span> ${
            dato.deuda_mora
          }</p>
          <p><span style="font-weight: bold"; font-weight: bold">Total:</span> ${
            dato.deuda_total
          }</p>
        </div>
        `;

       $(`#deuda-izquierda-${dato.id_deuda}`).html(izquierda);
     });

     data.map((dato, i) => {
       let derecha = ``;
       derecha += `
        <div class="col-sm-3" style="padding:10px !important;">
          <div style="padding:10px !important; border: 1.5px solid #007BFF; border-radius: 10px; background: #c9dff7">
            <p><span style="font-weight: bold"; font-weight: bold">Deuda:</span> ${
              dato.id_deuda
            }</p>
            <p><span style="font-weight: bold"; font-weight: bold">Cuota:</span> ${
              dato.cuotas
            }</p>
            <p><span style="font-weight: bold"; font-weight: bold">Fecha:</span> ${
              dato.fecha.split("T")[0]
            }</p>
            <p><span style="font-weight: bold"; font-weight: bold">Subtotal:</span> ${
              dato.subtotal
            }</p>
            <p><span style="font-weight: bold"; font-weight: bold">Descuento:</span> ${
              dato.descuento
            }</p>
            <p><span style="font-weight: bold"; font-weight: bold">Mora:</span> ${
              dato.mora
            }</p>
            <p><span style="font-weight: bold"; font-weight: bold">Total:</span> ${
              dato.total
            }</p>
          </div>
        </div>
                `;

       $(`#deuda-derecha-${dato.id_deuda}`).append(derecha);
     });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  } 
};
