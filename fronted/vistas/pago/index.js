$(function () {
  $("#menu42").addClass("active");
  $("#menu-home").removeClass("active");

  cargdarData();
});

let cargdarData = async () => {
  try {
    let dni = await sessionStorage.getItem("Dni");

    let { data } = await axios.get(`${URL_API}/deudas/detalle-dni-pagado`, {
      params: { dni },
    });

    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Deuda</th>
                                        <th>Cuota</th>
                                        <th>Subtotal</th>
                                        <th>Mora</th>
                                        <th>Descuento</th>
                                        <th>Total</th>
                                        <th>Fecha</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.id_deuda}</td>
                                        <td>${dato.cuotas}</td>
                                        <td>${dato.subtotal}</td>
                                        <td>${dato.mora}</td>
                                        <td>${dato.descuento}</td>
                                        <td>${dato.total}</td>
                                        <td>${dato.fecha.split("T")[0]}</td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};
