<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SNE-CGTCH</title>
    <link rel="icon" href="../../img/cgt.ico" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php require_once '../plugin-basic.php'?>

    <!-- DataTables -->
    <link rel="stylesheet" href="../../util/lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../util/lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="../../util/lte/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="../../util/lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed text-sm">
<div class="wrapper">

    <?php require_once '../header.php'?>

    <?php require_once '../menu.php'?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Pagos</h1>
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
<!--                --><?php //require_once '../menu-boddy.php' ?>
                <!-- /.row -->
                <!-- Main row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tabla de Pagos</h3>
                            </div>
                             <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Insertar Nro Deuda" id="modal-dni" required />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" id="btn-buscar">
                                            Buscar Deudas
                                        </button>
                                    </div>
                                </div>
                            </div>
                           
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div id="maintatable"></div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php require_once '../footer.php'?>
</div>

<div class="modal fade" id="modal-cuotas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title">Numero de cuotas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="modal-cerrar-cuotas">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="form-control" id="modal-id-deudas" >
                <input type="hidden" class="form-control" id="modal-id-deuda-cuotas" >
                <div class="row">
                    <div class="col-sm-6">
                        <!-- text input -->
                        <div class="form-group">
                            <label>N° Transaccion</label>
                            <input type="text" class="form-control" placeholder="" id="modal-nro-transaccion">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>tipo de transaccion</label>
                            <div id="modal-combo-tipo-transaccion"></div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="modal-button-cuotas">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- DataTable -->
<script src="../../util/lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../util/lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- Select2 -->
<script src="../../util/lte/plugins/select2/js/select2.full.min.js"></script>

<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="../util.js"></script>
<script src="admin.js"></script>
<!-- ./wrapper -->
</body>
</html>
