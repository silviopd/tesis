$(function () {
  $("#menu41").addClass("active");
  $("#menu-home").removeClass("active");

  comboDeuda();
});

document.getElementById("btn-buscar").onclick = async () => {
  try {
    let id_deuda = await document.getElementById("modal-dni").value;

    if (id_deuda === "") {
      Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
      return;
    }

    let { data } = await axios.get(`${URL_API}/deudas/detalle`, {
      params: { id_deuda },
    });

    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Deuda</th>
                                        <th>Cuota</th>
                                        <th>Dni</th>
                                        <th>Subtotal</th>
                                        <th>Mora</th>
                                        <th>Descuento</th>
                                        <th>Total</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.id_deuda}</td>
                                        <td>${dato.cuotas}</td>
                                        <td>${dato.dni}</td>
                                        <td>${dato.subtotal}</td>
                                        <td>${dato.mora}</td>
                                        <td>${dato.descuento}</td>
                                        <td>${dato.total}</td>
                                        <td>
                                            <div style="display: inline-flex;">
                                                <button type="button" class="btn btn-warning" onclick="setData('${dato.id_deuda}','${dato.cuotas}')" style="margin-right: 10px" data-toggle="modal" data-target="#modal-cuotas"><i class="fas fa-edit"></i></button>                    
                                            </div>
                                        </td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

let setData = (id_deuda, id_cuota) => {
  document.getElementById("modal-id-deudas").value = id_deuda;
  document.getElementById("modal-id-deuda-cuotas").value = id_cuota;
};

let comboDeuda = async () => {
  try {  
    let { data } = await axios.get(`${URL_API}/tipotransaccion`, {});
    let html = "";
    html += `<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" id="modal-transaccion">`;
    data.map((dato, i) => {
      html += `<option ${i === 0 ? 'selected="selected"' : null} value="${
        dato.id_tipo_transaccion
      }">${dato.nombre}</option>`;
    });

    html += `</select>`;

    $("#modal-combo-tipo-transaccion").html(html);

    $("#modal-transaccion").select2();
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

document.getElementById("modal-button-cuotas").onclick = () => {
  let numero_transaccion = document.getElementById("modal-nro-transaccion")
    .value;
  let id_tipo_transaccion = $("#modal-transaccion").val();
  let id_deuda = document.getElementById("modal-id-deudas").value;
  let cuotas = document.getElementById("modal-id-deuda-cuotas").value;

  if (numero_transaccion === "") {
    Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
    return;
  }

  Swal.fire({
    title: "Pagar",
    text: "¡Estás seguro de pagar esta deuda!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, pagar",
  }).then(async (result) => {
    try {
      if (result.value) {
        // let dni_usuario = sessionStorage.getItem("Dni");

        let { data } = await axios.post(`${URL_API}/deudas/detalle`, {
          id_deuda,
          cuotas,
          id_tipo_transaccion,
          numero_transaccion,
        });

        if (data[0].f_pagar_deuda) {
          Swal.fire("Pagado!", "Se ha pagado correctamente", "success");

          $("#modal-cerrar-cuotas").click();
          $("#btn-buscar").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }
  });
};
