$(function () {
  if (
    sessionStorage.getItem("Dni") === null ||
    sessionStorage.getItem("Dni") === "undefined" ||
    sessionStorage.getItem("Dni").length === 0
  ) {
    location.href = "../login/index.php";
  } else {
    $("#menu-principal").html(JSON.parse(sessionStorage.getItem("Menu")));
    $("#menu-nombre").html(
      `<a href="#" class="d-block">Sr(a). ${sessionStorage.getItem(
        "Paterno"
      )} ${sessionStorage.getItem("Materno")}</a>`
    );
  }

  $("#menu93").removeAttr("href").css("cursor", "pointer");

  document.getElementById("menu93").onclick = () => {
    location.href = "../login/index.php";
    sessionStorage.clear();
  };
});
