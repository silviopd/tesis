$(function () {
  $("#modal-estado").select2();
  $("#menu21").addClass("active");
  $("#menu-home").removeClass("active");

  cargarData();
  comboDeuda();
});

let cargarData = async () => {
  try {
    let { data } = await axios.get(`${URL_API}/deudas`, {});
    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Deuda</th>
                                        <th>Dni</th>
                                        <th>Contribuyente</th>
                                        <th>Categoria</th>
                                        <th>Subtotal</th>
                                        <th>Mora</th>
                                        <th>Descuento</th>
                                        <th>Total</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.id_deuda}</td>
                                        <td>${dato.dni}</td>
                                        <td>${dato.apellido_materno} ${dato.apellido_paterno} ${dato.nombres}</td>
                                        <td>${dato.categoria}</td>
                                        <td>${dato.subtotal}</td>
                                        <td>${dato.mora}</td>
                                        <td>${dato.descuento}</td>
                                        <td>${dato.total}</td>
                                        <td>${dato.estado}</td>

                                        <td>
                                             <div style="display: inline-flex;">
                                                <button type="button" class="btn btn-warning" style="margin-right: 10px" onclick="leerDatos('${dato.id_deuda}')" data-toggle="modal" data-target="#modal-lg"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-danger" onclick="eliminar('${dato.id_deuda}')"><i class="fas fa-trash"></i></button>                     
                                            </div>
                                        </td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

let nuevo = () => {
  $("#modal-title").text("NUEVO");
  $("#modal-button").text("GRABAR");

  $("#modal-id_deuda").val("");
  $("#modal-dni").val("");
  $("#modal-categoria").val("1").trigger("change");
  $("#modal-subtotal").val("");
  $("#modal-mora").val("");
  $("#modal-descuento").val("");
  $("#modal-total").val("");
  $("#modal-estado").val("A").trigger("change");

  $("#txttipooperacion").val("agregar");

  menuHD(false);
};

let eliminar = (id_deuda) => {
  Swal.fire({
    title: "Eliminar",
    text: "¡Estás seguro de eliminar este registro!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, eliminar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let dni_usuario = sessionStorage.getItem("Dni");

        let { data } = await axios.post(`${URL_API}/deudas/eliminar`, {
          id_deuda,
          dni_usuario,
        });

        if (data[0].v_result === 200) {
          Swal.fire("Eliminado!", "Se ha eliminado exitosamente.", "success");

          cargarData();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};

let leerDatos = async (id_deuda) => {
  try {
    $("#modal-title").text("EDITAR");
    $("#modal-button").text("GUARDAR");

    let { data } = await axios.get(`${URL_API}/deudas/leerdatos`, {
      params: { id_deuda },
    });

    let datos = data[0];

    $("#modal-id_deuda").val(datos.id_deuda);
    $("#modal-dni").val(datos.dni);
    $("#modal-categoria").val(datos.id_categoria).trigger("change");
    $("#modal-subtotal").val(datos.subtotal);
    $("#modal-mora").val(datos.mora);
    $("#modal-descuento").val(datos.descuento);
    $("#modal-total").val(datos.total);
    $("#modal-estado").val(datos.estado).trigger("change");

    $("#txttipooperacion").val("editar");

    menuHD(true);
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

document.getElementById("modal-button").onclick = () => {
  let tipo = document.getElementById("txttipooperacion").value;

  let id_deuda = $("#modal-id_deuda").val();
  let dni = $("#modal-dni").val();
  let id_categoria = $("#modal-categoria").val();
  let subtotal = $("#modal-subtotal").val();
  let mora = $("#modal-mora").val();
  let descuento = $("#modal-descuento").val();
  let total = $("#modal-total").val();
  let estado = $("#modal-estado").val();
  let dni_usuario = sessionStorage.getItem("Dni");

  if (dni === "" || total === "") {
    Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
    return;
  }

  tipo === "agregar"
    ? agregar(
        dni,
        id_categoria,
        subtotal,
        mora,
        descuento,
        total,
        estado,
        dni_usuario
      )
    : editar(
        id_deuda,
        dni,
        id_categoria,
        subtotal,
        mora,
        descuento,
        total,
        estado,
        dni_usuario
      );
};

let agregar = (
  dni,
  id_categoria,
  subtotal,
  mora,
  descuento,
  total,
  estado,
  dni_usuario
) => {
  Swal.fire({
    title: "Deseas Agregar?",
    text: "Se agregará un nuevo registro!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, agregar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let { data } = await axios.post(`${URL_API}/deudas/nuevo`, {
          dni,
          id_categoria,
          subtotal,
          mora,
          descuento,
          total,
          estado,
          dni_usuario,
        });

        if (data[0].v_result === 200) {
          Swal.fire("Agregado!", "Se ha agregado exitosamente", "success");

          cargarData();
          nuevo();
          $("#modal-cerrar").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};

let editar = (
  id_deuda,
  dni,
  id_categoria,
  subtotal,
  mora,
  descuento,
  total,
  estado,
  dni_usuario
) => {
  Swal.fire({
    title: "Deseas Editar?",
    text: "Se editara este registro!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, editar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let { data } = await axios.post(`${URL_API}/deudas/editar`, {
          id_deuda,
          dni,
          id_categoria,
          subtotal,
          mora,
          descuento,
          total,
          estado,
          dni_usuario,
        });

        if (data[0].v_result === 200) {
          Swal.fire("Editado!", "Se ha editado exitosamente", "success");

          cargarData();
          nuevo();
          $("#modal-cerrar").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};

let comboDeuda = async () => {
  try {
    let { data } = await axios.get(`${URL_API}/categoria`, {});
    let html = "";
    html += `<select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" id="modal-categoria">`;
    data.map((dato, i) => {
      html += `<option ${i === 0 ? 'selected="selected"' : null} value="${
        dato.id_categoria
      }">${dato.nombre}</option>`;
    });

    html += `</select>`;

    $("#moda-combo-categoria").html(html);

    $("#modal-categoria").select2();
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }
};

const menuHD = (value) => {
  $("#modal-dni").prop("disabled", value);
};
