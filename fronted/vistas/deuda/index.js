$(function () {
  $("#menu22").addClass("active");
  $("#menu-home").removeClass("active");

  //Date range picker
  $("#reservationdate").datetimepicker({
    format: "YYYY-MM-DD",
  });

  cargarData();
});

let cargarData = async () => {
  try {
    let dni = await sessionStorage.getItem("Dni");
    console.log(dni);
    let { data } = await axios.get(`${URL_API}/deudas`, { params: { dni } });
    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Deuda</th>
                                        <th>Dni</th>
                                        <th>Contribuyente</th>
                                        <th>Categoria</th>
                                        <th>Subtotal</th>
                                        <th>Mora</th>
                                        <th>Descuento</th>
                                        <th>Total</th>
                                        <th>Fecha</th>
                                        <th>estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.id_deuda}</td>
                                        <td>${dato.dni}</td>
                                        <td>${dato.apellido_materno} ${
        dato.apellido_paterno
      } ${dato.nombres}</td>
                                        <td>${dato.categoria}</td>
                                        <td>${dato.subtotal}</td>
                                        <td>${dato.mora}</td>
                                        <td>${dato.descuento}</td>
                                        <td>${dato.total}</td>
                                        <td>${dato.fecha.split("T")[0]}</td>
                                        <td>${
                                          dato.reclamo === "N" ||
                                          dato.reclamo === "D" ||
                                          dato.reclamo === "Z"
                                            ? "Activo"
                                            : "Revisión"
                                        }</td>
                                        <td>
                                            <div style="display: inline-flex;">`;

      if (
        dato.reclamo === "N" ||
        dato.reclamo === "D" ||
        dato.reclamo === "Z"
      ) {
        dato.cuotas === "S"
          ? ""
          : (html += `<button type="button" class="btn btn-warning" style="margin-right: 10px" onclick="cuotas('${dato.id_deuda}','${dato.total}')" data-toggle="modal" data-target="#modal-cuotas" title="Fraccionar cuotas"><i class="fas fa-edit"></i></button>
                  <button type="button" class="btn btn-danger" style="margin-right: 10px" onclick="reclamo('${dato.dni}','${dato.id_deuda}')" data-toggle="modal" data-target="#modal-reclamo" title="Reclamo"><i class="fas fa-exclamation-triangle"></i></button>`);
        dato.cuotas === "N"
          ? ""
          : (html += `<button type="button" class="btn btn-info" style="margin-right: 10px" onclick="detalle('${dato.id_deuda}')" title="Detalle"><i class="fas fa-align-center"></i></button>
                                            </div>
                                        </td>
                                    </tr>`);
      }
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }  
};

let cuotas = (id_deuda, total) => {
  $("#modal-deuda-total").val(total);
  $("#modal-id-deuda-cuotas").val(id_deuda);
};

document.getElementById("modal-button-cuotas").onclick = () => {
  let id_deuda = $("#modal-id-deuda-cuotas").val();
  let fecha = $("#fecha").val();
  let nro_cuotas = $("#modal-nro-cuotas").val();

  if (nro_cuotas > 10) {
    alert("no se puede ingresar numero de cuotas mayor a 12 meses (1 año)");
    return;
  }

  if (fecha === "" || nro_cuotas === "") {
    Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
    return;
  }

  Swal.fire({
    title: "Asignar cuotas",
    text: "¡Estás seguro de asignar estas cuotas!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, asignar cuotas!",
  }).then(async (result) => {
    try {
      if (result.value) {
        let dni_usuario = sessionStorage.getItem("Dni");

        let { data } = await axios.post(`${URL_API}/deudas/cuotas`, {
          id_deuda,
          nro_cuotas,
          fecha,
        });

        if (data[0].v_result === 200) {
          Swal.fire(
            "Agrgado!",
            "Se ha asignado cuotas correctament.",
            "success"
          );

          cargarData();

          $("#modal-cerrar-cuotas").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};

let reclamo = (dni, id_deuda) => {
  $("#modal-reclamo-dni").val(dni);
  $("#modal-reclamo-deuda").val(id_deuda);
};

let detalle = async (id_deuda) => {
  try {
    let { data } = await axios.get(`${URL_API}/deudas/detalle`, {
      params: { id_deuda },
    });
    let html = "";
    html += `<table id="tabla${id_deuda}" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Cuotas</th>
                                        <th>Subtotal</th>
                                        <th>Mora</th>
                                        <th>Descuento</th>
                                        <th>Total</th>                                        
                                        <th>Fecha</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.cuotas}</td>
                                        <td>${dato.subtotal}</td>
                                        <td>${dato.mora}</td>
                                        <td>${dato.descuento}</td>
                                        <td>${dato.total}</td>
                                        <td>${dato.fecha.split("T")[0]}</td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    await $("#maintatabledetalle").html(html);

    $("#modal-detalle")
      .on("shown.bs.modal", function () {
        $(`#tabla${id_deuda}`)
          .DataTable({ responsive: true, destroy: true })
          .columns.adjust()
          .responsive.recalc();
      })
      .modal("show");
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }  
};

document.getElementById("modal-button-reclamo").onclick = () => {
  let dni = $("#modal-reclamo-dni").val();
  let id_deuda = $("#modal-reclamo-deuda").val();
  let descripcion = $("#modal-reclamo-descripcion").val();

  if (descripcion === "") {
    Swal.fire("Campos Vacios!", "Debe llenar todos los campos", "info");
    return;
  }

  Swal.fire({
    title: "Reclamo",
    text: "¡Estás seguro de reclamar esta deuda!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, reclamar",
  }).then(async (result) => {
    try {
      if (result.value) {
        let { data } = await axios.post(`${URL_API}/reclamo/deuda`, {
          dni,
          id_deuda,
          descripcion,
        });

        console.log(data);
        if (data === "ok") {
          Swal.fire("Reclamo!", "reclamo satisfacotrio", "success");

          cargarData();

          $("#modal-cerrar-reclamo").click();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};
