<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SNE-CGTCH</title>
    <link rel="icon" href="../../img/cgt.ico" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php require_once '../plugin-basic.php'?>

    <!-- DataTables -->
    <link rel="stylesheet" href="../../util/lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../../util/lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed text-sm">
<div class="wrapper">

    <?php require_once '../header.php'?>

    <?php require_once '../menu.php'?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">DEPARTAMENTO DE RECLAMO</h1>
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
<!--                --><?php //require_once '../menu-boddy.php' ?>
                <!-- /.row -->
                <!-- Main row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tabla de reclamo</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div id="maintatable"></div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php require_once '../footer.php'?>
</div>

<script src="../../util/lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../util/lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../util/lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="../util.js"></script>
<script src="sistema.js"></script>
<!-- ./wrapper -->
</body>
</html>
