$(function () {
  $("#menu115").addClass("active");
  $("#menu-home").removeClass("active");

  cargarData();
});

let cargarData = async () => {
  try {
    let opcion = "S";
    let { data } = await axios.get(`${URL_API}/reclamo/reclamo`, {
      params: { opcion },
    });
    console.log(data);
    let html = "";
    html += `<table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Deuda</th>
                                        <th>Contribuyente</th>
                                        <th>Categoria</th>
                                        <th>Subtotal</th>
                                        <th>Mora</th>
                                        <th>Descuento</th>
                                        <th>Total</th>
                                        <th>Fecha</th>
                                        <th>Reclamo</th>
                                        <th>F. Reclamo</th>
                                        <th>F. R. Aprobado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>`;

    data.map((dato) => {
      html += `<tr>
                                        <td>${dato.id_deuda}</td>
                                        <td>${dato.apellido_materno} ${
        dato.apellido_paterno
      } ${dato.nombres}</td>
                                        <td>${dato.categoria}</td>
                                        <td>${dato.subtotal}</td>
                                        <td>${dato.mora}</td>
                                        <td>${dato.descuento}</td>
                                        <td>${dato.total}</td>
                                        <td>${dato.fecha.split("T")[0]}</td>
                                        <td>${dato.reclamo_descripcion}</td>
                                        <td>${
                                          dato.reclamo_fecha.split("T")[0]
                                        }</td>
                                        <td>${
                                          dato.reclamo_ad.split("T")[0]
                                        }</td>
                                        <td>
                                            <div style="display: inline-flex;">
                                              <button type="button" class="btn btn-success" style="margin-right: 10px" onclick="aprobar('${
                                                dato.dni
                                              }','${
        dato.id_deuda
      }')" data-toggle="modal" data-target="#modal-cuotas"><i class="fas fa-check"></i></button>
                                            </div>
                                        </td>
                                    </tr>`;
    });
    html += `</tbody>
                </table>`;

    $("#maintatable").html(html);

    $("#example1").DataTable({
      responsive: true,
      autoWidth: false,
    });
  } catch (error) {
    Swal.fire("Error!", "", "error");
  }  
};

const aprobar = (dni, id_deuda) => {
  console.log("aprobar", dni, id_deuda);
  // await axios.get(`${URL_API}/notificacion/especifico`, { params: { dni } });
  let opcion = "Z";
  Swal.fire({
    title: "Actualizado?",
    text: "Reclamo para actualizar!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, aprobar!",
  }).then(async (result) => {
    try {
      if (result.value) {
        // let dni_usuario = sessionStorage.getItem("Dni");

        let { data } = await axios.post(`${URL_API}/reclamo/reclamo-ad`, {
          dni,
          id_deuda,
          opcion,
        });

        if (data === "ok") {
          Swal.fire("Aprobado!", "Se ha aprobado correctamente.", "success");

          cargarData();
        }
      }
    } catch (error) {
      Swal.fire("Error!", "", "error");
    }    
  });
};

// const desaprobar = (dni, id_deuda) => {
//   console.log("aprobar", dni, id_deuda);
//   // await axios.get(`${URL_API}/notificacion/especifico`, { params: { dni } });
//   let opcion = "N";
//   Swal.fire({
//     title: "Desaṕrobar?",
//     text: "Deseas desaprobar este reclamo!",
//     icon: "warning",
//     showCancelButton: true,
//     confirmButtonColor: "#3085d6",
//     cancelButtonColor: "#d33",
//     confirmButtonText: "Yes, delete it!",
//   }).then(async (result) => {
//     if (result.value) {
//       // let dni_usuario = sessionStorage.getItem("Dni");

//       let { data } = await axios.post(`${URL_API}/reclamo/reclamo-ad`, {
//         dni,
//         id_deuda,
//         opcion,
//       });

//       if (data === "ok") {
//         Swal.fire("Deleted!", "Your file has been deleted.", "success");

//         cargarData();
//       }
//     }
//   });
// };
